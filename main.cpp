#include <QApplication>
#include "gui/Window.h"
#include "core/support/Settings.h"

using namespace std;

int main(int argc, char* argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling); // DPI support
    QApplication app(argc, argv);
	
	app.setWindowIcon(QIcon(":/icons/main.png"));
	
	// Initialization translator
	QTranslator translator;
		
	if(Settings::getInstance()->getLanguage() != "english")
	{
		QString path = ":/languages/" + Settings::getInstance()->getLanguage();
		translator.load(path);
		app.installTranslator(&translator);
	}

	// Initialization main window
	Window m_window;
	m_window.showMaximized();
	
	Settings::getInstance()->kill();
	
	return app.exec();
}
