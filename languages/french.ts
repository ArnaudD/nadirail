<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../gui/DialogAbout.ui" line="14"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="26"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>NadiRail v1.0</source>
        <translation type="vanished">NadiRail v1.0</translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="46"/>
        <source>NadiRail v1.1</source>
        <translation>NadiRail v1.1</translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="59"/>
        <source>License: GNU GPL version 3</source>
        <translation>License: GNU GPL version 3</translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="72"/>
        <source>&lt;a href=&quot;https://nadirail.frama.io/&quot;&gt;Website&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="104"/>
        <source>January 2020</source>
        <translation>Janvier 2020</translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="138"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:7.8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;NadiRail has been inspired by RailWorld, developed by Steve Kollmansberger.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;Maintained by Rick Rutt here: &lt;/span&gt;&lt;a href=&quot;https://github.com/rrutt/RailWorld&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/rrutt/RailWorld&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;The NadiRail icon, and the car thumbnails provided with maps are from MLGTraffic.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;Find more for your own maps at &lt;/span&gt;&lt;a href=&quot;http://www.mlgtraffic.net/index.html&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.mlgtraffic.net/index.html&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;NadiRail is developed using the following librairies:  &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;The Qt5 Library &lt;/span&gt;&lt;a href=&quot;http://www.qt.io/&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.qt.io/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;TinyXML-2 &lt;/span&gt;&lt;a href=&quot;http://www.grinninglizard.com/tinyxml2/&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.grinninglizard.com/tinyxml2/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;Some icons are taken or inspired from the Tangoified icons set for Inkscape and from the Tango Icon Theme:  &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://tango-project.org/&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://tango-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;NadiRail est inspiré de RailWorld, développé par Steve Kollmansberger.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Maintainu par Rick Rutt à cette adresse : &lt;a href=&quot;https://github.com/rrutt/RailWorld&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/rrutt/RailWorld&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;L&apos;icone de NadiRail, et les vignettes de wagons fournies avec les cartes viennent de MLGTraffic.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Vous pouvez les trouver pour vos cartes à cette adresse : &lt;a href=&quot;http://www.mlgtraffic.net/index.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.mlgtraffic.net/index.html&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;NadiRail utilise les librairies suivantes :  &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;La librairie Qt5 &lt;a href=&quot;http://www.qt.io/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.qt.io/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TinyXML-2 &lt;a href=&quot;http://www.grinninglizard.com/tinyxml2/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.grinninglizard.com/tinyxml2/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Certaines icones sont prises ou inspirées du set &quot;Tangoified icons set for Inkscape&quot; et du thème Tango :  &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://tango-project.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://tango-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>February 2020</source>
        <translation type="vanished">Février 2020</translation>
    </message>
    <message>
        <location filename="../gui/DialogAbout.ui" line="120"/>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;NadiRail has been inspired by RailWorld,  developed by Steve Kollmansberger.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Maintained by Rick Rutt here: &lt;a href=&quot;https://github.com/rrutt/RailWorld&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/rrutt/RailWorld&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The NadiRail icon, and the car thumbnails provided with maps are from MLGTraffic.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Find more for your own maps at &lt;a href=&quot;http://www.mlgtraffic.net/index.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.mlgtraffic.net/index.html&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;NadiRail is developed using the following librairies:  &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Qt5 Library &lt;a href=&quot;http://www.qt.io/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.qt.io/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TinyXML-2 &lt;a href=&quot;http://www.grinninglizard.com/tinyxml2/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.grinninglizard.com/tinyxml2/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Some icons are taken or inspired from the Tangoified icons set for Inkscape and from the Tango Icon Theme:  &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://tango-project.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://tango-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;NadiRail est inspiré de RailWorld,  développé par Steve Kollmansberger.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Maintainu par Rick Rutt à cette adresse : &lt;a href=&quot;https://github.com/rrutt/RailWorld&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/rrutt/RailWorld&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;L&apos;icone de NadiRail, et les vignettes de wagons fournies avec les cartes viennent de MLGTraffic.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Vous pouvez les trouver pour vos cartes à cette adresse : &lt;a href=&quot;http://www.mlgtraffic.net/index.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.mlgtraffic.net/index.html&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;NadiRail utilise les librairies suivantes :  &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;La librairie Qt5 &lt;a href=&quot;http://www.qt.io/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.qt.io/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TinyXML-2 &lt;a href=&quot;http://www.grinninglizard.com/tinyxml2/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.grinninglizard.com/tinyxml2/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Certaines icones sont prises ou inspirées du set &quot;Tangoified icons set for Inkscape&quot; et du thème Tango :  &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://tango-project.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://tango-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>DialogCompo</name>
    <message>
        <location filename="../gui/DialogCompo.ui" line="20"/>
        <source>Compositions</source>
        <translation>Compositions</translation>
    </message>
</context>
<context>
    <name>DialogCondition</name>
    <message>
        <location filename="../gui/DialogCondition.ui" line="14"/>
        <location filename="../gui/DialogCondition.ui" line="197"/>
        <source>Add condition</source>
        <translation>Ajouter condition</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="26"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="39"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="52"/>
        <source>Current condition(s) on this platform</source>
        <translation>Condition(s) actives sur ce quai</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="74"/>
        <source>Remove condition</source>
        <translation>Supprimer condition</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="88"/>
        <source>New condition</source>
        <translation>Nouvelle condition</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="113"/>
        <location filename="../gui/DialogCondition.ui" line="155"/>
        <source>Passenger car</source>
        <translation>Wagon passagers</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="120"/>
        <source>Box car</source>
        <translation>Wagon couvert</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="125"/>
        <source>Gondola car</source>
        <translation>Tombereau</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="130"/>
        <source>Flat car</source>
        <translation>Wagon plat</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="135"/>
        <source>Hopper car</source>
        <translation>Wagon trémie</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="140"/>
        <source>Special type car</source>
        <translation>Wagon spécial</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="145"/>
        <source>Maintenance of way car</source>
        <translation>Wagon de maintenance</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="150"/>
        <source>Caboose car</source>
        <translation>Fourgon</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="160"/>
        <source>Refrigerator car</source>
        <translation>Wagon frigorifique</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="165"/>
        <source>Stock car</source>
        <translation>Wagon bétailler</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="170"/>
        <source>Tank car</source>
        <translation>Wagon citerne</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="175"/>
        <source>Containers car</source>
        <translation>Wagon porte-containeurs</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="184"/>
        <source>Unloaded</source>
        <translation>Non chargé</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="189"/>
        <source>Loaded</source>
        <translation>Chargé</translation>
    </message>
    <message>
        <location filename="../gui/DialogCondition.ui" line="222"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogDrivingPanel</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Poste de conduite</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="32"/>
        <source>Driving Panel</source>
        <translation>Poste de conduite</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="66"/>
        <source>Throttle</source>
        <translation>Commande</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="109"/>
        <source>Power</source>
        <translation>Puissance</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="79"/>
        <location filename="../gui/DialogDrivingPanel.ui" line="122"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="49"/>
        <location filename="../gui/DialogDrivingPanel.cpp" line="80"/>
        <location filename="../gui/DialogDrivingPanel.cpp" line="108"/>
        <source>no thumbnail</source>
        <translation>Pas d&apos;imagette</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="155"/>
        <source>Speed limiter (max 5)</source>
        <translation>Limiteur de vitesse (5)</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="165"/>
        <source>Break</source>
        <translation>Frein</translation>
    </message>
    <message>
        <source>Horn</source>
        <translation type="vanished">Klaxon</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="175"/>
        <source>Reverse</source>
        <translation>Changer de sens</translation>
    </message>
    <message>
        <location filename="../gui/DialogDrivingPanel.ui" line="185"/>
        <source>Follow</source>
        <translation>Suivre</translation>
    </message>
</context>
<context>
    <name>DialogNewCar</name>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="14"/>
        <source>Add Car</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="311"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="26"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="39"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="61"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>A - Equipped box car</source>
        <translation type="obsolete">Wagon couvert</translation>
    </message>
    <message>
        <source>F - Flat car</source>
        <translation type="obsolete">Wagon plat</translation>
    </message>
    <message>
        <source>G - Unequipped gondola car</source>
        <translation type="obsolete">Tombereau</translation>
    </message>
    <message>
        <source>K - Equipped hopper car</source>
        <translation type="obsolete">Trémie</translation>
    </message>
    <message>
        <source>L - Special type car</source>
        <translation type="obsolete">Spécial</translation>
    </message>
    <message>
        <source>M - Maintenance of way car</source>
        <translation type="obsolete">Maintenance</translation>
    </message>
    <message>
        <source>N - Caboose car</source>
        <translation type="obsolete">Fourgon</translation>
    </message>
    <message>
        <source>P - Passenger car</source>
        <translation type="obsolete">Passagers</translation>
    </message>
    <message>
        <source>R - Refrigerator car</source>
        <translation type="obsolete">Frigorifique</translation>
    </message>
    <message>
        <source>S - Stock car</source>
        <translation type="obsolete">Bétailler</translation>
    </message>
    <message>
        <source>T - Tank car</source>
        <translation type="obsolete">Citerne</translation>
    </message>
    <message>
        <source>U - Containers</source>
        <translation type="obsolete">Containeurs</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="75"/>
        <location filename="../gui/DialogNewCar.cpp" line="78"/>
        <source>Box car</source>
        <translation>Wagon couvert</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="80"/>
        <location filename="../gui/DialogNewCar.cpp" line="80"/>
        <source>Gondola car</source>
        <translation>Tombereau</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="85"/>
        <location filename="../gui/DialogNewCar.cpp" line="82"/>
        <source>Flat car</source>
        <translation>Wagon plat</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="90"/>
        <location filename="../gui/DialogNewCar.cpp" line="84"/>
        <source>Hopper car</source>
        <translation>Wagon trémie</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="95"/>
        <location filename="../gui/DialogNewCar.cpp" line="86"/>
        <source>Special type car</source>
        <translation>Wagon spécial</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="100"/>
        <location filename="../gui/DialogNewCar.cpp" line="88"/>
        <source>Maintenance of way car</source>
        <translation>Wagon de maintenance</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="105"/>
        <location filename="../gui/DialogNewCar.cpp" line="90"/>
        <source>Caboose car</source>
        <translation>Fourgon</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="346"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="68"/>
        <location filename="../gui/DialogNewCar.ui" line="110"/>
        <location filename="../gui/DialogNewCar.cpp" line="92"/>
        <source>Passenger car</source>
        <translation>Wagon passagers</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="115"/>
        <location filename="../gui/DialogNewCar.cpp" line="94"/>
        <source>Refrigerator car</source>
        <translation>Wagon frigorifique</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="120"/>
        <location filename="../gui/DialogNewCar.cpp" line="96"/>
        <source>Stock car</source>
        <translation>Wagon bétailler</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="125"/>
        <location filename="../gui/DialogNewCar.cpp" line="98"/>
        <source>Tank car</source>
        <translation>Wagon citerne</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="130"/>
        <location filename="../gui/DialogNewCar.cpp" line="100"/>
        <source>Containers car</source>
        <translation>Wagon porte-containeurs</translation>
    </message>
    <message>
        <source>Containers</source>
        <translation type="vanished">Containeurs</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="161"/>
        <source>Thumbnail (optional)</source>
        <translation>Imagette (optionnelle)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="196"/>
        <source>no thumbnail</source>
        <translation>Pas d&apos;imagette</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="215"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="248"/>
        <source>Unloaded</source>
        <translation>Non chargé</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="253"/>
        <source>Loaded</source>
        <translation>Chargé</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.ui" line="289"/>
        <source>Departure platform</source>
        <translation>Quai de départ</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.cpp" line="142"/>
        <source>Open Image</source>
        <translation>Ouvrir image</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewCar.cpp" line="142"/>
        <source>Image Files (*.png *.jpg *.bmp *.tif *.gif)</source>
        <translation>Fichiers image (*.png *.jpg *.bmp *.tif *.gif)</translation>
    </message>
</context>
<context>
    <name>DialogNewLoco</name>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="14"/>
        <source>Add Locomotive</source>
        <translation>Ajouter une locomotive</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="40"/>
        <source>Thumbnail (optional)</source>
        <translation>Imagette (optionnelle)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="66"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="79"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="101"/>
        <source>no thumbnail</source>
        <translation>Pas d&apos;imagette</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="123"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="155"/>
        <source>Coupled cars (optional)</source>
        <translation>Wagons attelés (optionnel)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="177"/>
        <source>Add car</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="190"/>
        <source>Remove car</source>
        <translation>Retirer</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="203"/>
        <source>Copy last car</source>
        <translation>Copier dernier</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="220"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="242"/>
        <source>Maximum speed</source>
        <translation>Vitesse maximale</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="271"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="306"/>
        <source>Departure platform</source>
        <translation>Quai de départ</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.ui" line="328"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.cpp" line="84"/>
        <source>Open Image</source>
        <translation>Ouvrir image</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.cpp" line="84"/>
        <source>Image Files (*.png *.jpg *.bmp *.tif *.gif)</source>
        <translation>Fichiers image (*.png *.jpg *.bmp *.tif *.gif)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.cpp" line="194"/>
        <source>Name missing</source>
        <translation>Nom manquant</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewLoco.cpp" line="194"/>
        <source>You must specify a name to identify this locomotive.</source>
        <translation>Vous devez spécifier un nom pour identifier cette locomotive.</translation>
    </message>
</context>
<context>
    <name>DialogNewMap</name>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="14"/>
        <source>Map size and background image</source>
        <translation>Taille de la carte et image de fond</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="26"/>
        <source>Map size</source>
        <translation>Taille de la carte</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="44"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="73"/>
        <source>Y (m)</source>
        <translation>Y (m)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="100"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="116"/>
        <source>Spatial resolution (meters per pixel)</source>
        <translation>Résolution spatiale (metres par pixel)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.ui" line="159"/>
        <source>Background image</source>
        <translation>Image de fond</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.cpp" line="26"/>
        <source>Open Image</source>
        <translation>Ouvrir image</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.cpp" line="26"/>
        <source>Image Files (*.png *.jpg *.bmp *.tif)</source>
        <translation>Fichiers image (*.png *.jpg *.bmp *.tif)</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.cpp" line="82"/>
        <source>Background image not selected</source>
        <translation>Image de fond non sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/DialogNewMap.cpp" line="82"/>
        <source>You must specify a background image.</source>
        <translation>Vous devez sélectionner une image de fond.</translation>
    </message>
</context>
<context>
    <name>DialogOptions</name>
    <message>
        <location filename="../gui/DialogOptions.ui" line="20"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="58"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="77"/>
        <location filename="../gui/DialogOptions.cpp" line="70"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="82"/>
        <location filename="../gui/DialogOptions.cpp" line="74"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="97"/>
        <source>Track geometry</source>
        <translation>Géométrie de la voie</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="109"/>
        <source>Gauge</source>
        <translation>Ecartement des rails</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="141"/>
        <source>Crosstie width</source>
        <translation>Largeur de la voie</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="174"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="186"/>
        <source>Tracks</source>
        <translation>Rails</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="199"/>
        <source>Railway</source>
        <translation>Voie</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="238"/>
        <source>Platform</source>
        <translation>Quai</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="32"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.ui" line="45"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.cpp" line="85"/>
        <source>Language change</source>
        <translation>Changement de langue</translation>
    </message>
    <message>
        <location filename="../gui/DialogOptions.cpp" line="85"/>
        <source>The application needs to be restarted to apply the language change.</source>
        <translation>Le programme doit être redémarré pour appliquer le changement de langue.</translation>
    </message>
</context>
<context>
    <name>DialogScenario</name>
    <message>
        <location filename="../gui/DialogScenario.ui" line="14"/>
        <source>Edit scenario</source>
        <translation>Editer scénario</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="26"/>
        <source>Description of the scenario</source>
        <translation>Description du scénario</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="49"/>
        <source>Condition on platforms</source>
        <translation>Conditions sur quais</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="64"/>
        <source>Edit condition</source>
        <translation>Editer condition</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="88"/>
        <source>Permissions</source>
        <translation>Permissions</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="100"/>
        <source>Add locomotives</source>
        <translation>Ajouter locomotives</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="113"/>
        <source>Add cars</source>
        <translation>Ajouter wagons</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="127"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gui/DialogScenario.ui" line="140"/>
        <source>Victory message</source>
        <translation>Message de victoire</translation>
    </message>
</context>
<context>
    <name>Scene</name>
    <message>
        <location filename="../core/support/Scene.cpp" line="1269"/>
        <source>Platform%1</source>
        <translation>Quai%1</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1275"/>
        <location filename="../core/support/Scene.cpp" line="1291"/>
        <location filename="../core/support/Scene.cpp" line="1297"/>
        <source>Platform name</source>
        <translation>Nom du quai</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1276"/>
        <source>Platform name:</source>
        <translation>Nom du quai:</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1291"/>
        <source>The name cannot be empty. Please enter a valid name.</source>
        <translation>Le nom ne doit pas être vide. Veuillez entrer un nom valide.</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1297"/>
        <source>This name already exists. Please choose another name.</source>
        <translation>Ce nom existe déjà. Veuillez choisir un autre nom.</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1329"/>
        <source>An accident happened, involving:</source>
        <translation>Un accident s&apos;est produit, concernant:</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1343"/>
        <source>- a box car</source>
        <translation>- un wagon couvert</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1347"/>
        <source>- a gondola car</source>
        <translation>- un tombereau</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1351"/>
        <source>- a flat car</source>
        <translation>- un wagon plat</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1355"/>
        <source>- a hopper car</source>
        <translation>- un wagon trémie</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1359"/>
        <source>- a special type car</source>
        <translation>- un wagon spécial</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1363"/>
        <source>- a maintenance of way car</source>
        <translation>- un wagon de maintenance</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1367"/>
        <source>- a caboose car</source>
        <translation>- un fourgon</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1371"/>
        <source>- a passenger car</source>
        <translation>- un wagon passagers</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1375"/>
        <source>- a refrigerator car</source>
        <translation>- un wagon frigorifique</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1379"/>
        <source>- a stock car</source>
        <translation>- un wagon bétailler</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1383"/>
        <source>- a tank car</source>
        <translation>- un wagon citerne</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1387"/>
        <source>- a containers car</source>
        <translation>- un wagon porte-containeurs</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1393"/>
        <source>The damaged material will be removed from the map.</source>
        <translation>Le matériel endommagé sera retiré de la carte.</translation>
    </message>
    <message>
        <source>An accident happened, involving: 
</source>
        <translation type="vanished">Un accident s&apos;est produit, impliquant:\n</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1336"/>
        <source>- a locomotive, </source>
        <translation>- une locomotive, </translation>
    </message>
    <message>
        <source>- a </source>
        <translation type="vanished">- un </translation>
    </message>
    <message>
        <source>
Those will be removed from the map.</source>
        <translation type="vanished">\nCeux-ci seront retirés de la carte.</translation>
    </message>
    <message>
        <location filename="../core/support/Scene.cpp" line="1395"/>
        <source>Crash!</source>
        <translation>Accident !</translation>
    </message>
</context>
<context>
    <name>Window</name>
    <message>
        <source>File</source>
        <translation type="vanished">Fichier</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">Nouveau...</translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="vanished">Ouvrir...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="59"/>
        <source>Save...</source>
        <translation>Enregistrer...</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="51"/>
        <source>Open a map or a saved game...</source>
        <translation>Ouvrir une carte ou une partie sauvegardée...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="68"/>
        <source>Options...</source>
        <translation>Options...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="76"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="82"/>
        <source>Trains</source>
        <translation>Trains</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="85"/>
        <source>Train</source>
        <translation>Train</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="88"/>
        <source>Add locomotive...</source>
        <translation>Ajouter une locomotive...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="93"/>
        <source>Add car...</source>
        <translation>Ajouter un wagon...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="99"/>
        <source>Editor</source>
        <translation>Editeur</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="102"/>
        <source>Edit map</source>
        <translation>Editer la carte</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="116"/>
        <location filename="../gui/Window.cpp" line="117"/>
        <source>Drawing</source>
        <translation>Dessin</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="122"/>
        <source>Railway (straight)</source>
        <translation>Voie ferrée (droite)</translation>
    </message>
    <message>
        <source>Railway (platform)</source>
        <translation type="vanished">Voie ferrée (quai)</translation>
    </message>
    <message>
        <source>Railway (points)</source>
        <translation type="vanished">Aiguillage</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="46"/>
        <source>Create new map from scratch...</source>
        <translation>Créer une nouvelle carte...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="109"/>
        <source>Edit scenario...</source>
        <translation>Editer scénario...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="128"/>
        <source>Platform (straight)</source>
        <translation>Quai (droit)</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="134"/>
        <source>Tunnel (straigth)</source>
        <translation>Tunnel (droit)</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="143"/>
        <source>Railway (arc)</source>
        <translation>Voie ferrée (arc)</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="149"/>
        <source>Platform (arc)</source>
        <translation>Quai (arc)</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="155"/>
        <source>Tunnel (arc)</source>
        <translation>Tunnel (arc)</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="164"/>
        <source>Points</source>
        <translation>Aiguillage</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="173"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="179"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="186"/>
        <location filename="../gui/Window.cpp" line="187"/>
        <source>Modify</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="192"/>
        <source>Selection</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="201"/>
        <source>Change platform name</source>
        <translation>Changer le nom du quai</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="210"/>
        <source>Translate</source>
        <translation>Déplacer</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="216"/>
        <source>Rotate</source>
        <translation>Orienter</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="222"/>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="228"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="237"/>
        <source>Change font...</source>
        <translation>Changer la police...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="243"/>
        <source>Change text color...</source>
        <translation>Changer la couleur du texte...</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="250"/>
        <location filename="../gui/Window.cpp" line="251"/>
        <source>Constraints</source>
        <translation>Contraintes</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="256"/>
        <source>Ortho</source>
        <translation>Orthogonale</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="262"/>
        <source>Snapping</source>
        <translation>Accrochage</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="284"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="286"/>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="290"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="455"/>
        <source>Would you like to save before leaving?</source>
        <translation>Souhaitez vous enregistrer avant de quitter ?</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="24"/>
        <source>Driving panel</source>
        <translation>Poste de conduite</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="32"/>
        <location filename="../gui/Window.cpp" line="278"/>
        <source>Compositions</source>
        <translation>Compositions</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="43"/>
        <location filename="../gui/Window.cpp" line="44"/>
        <source>Game</source>
        <translation>Jeu</translation>
    </message>
    <message>
        <source>New map from scratch...</source>
        <translation type="vanished">Créer une nouvelle carte...</translation>
    </message>
    <message>
        <source>Open map or saved game...</source>
        <translation type="vanished">Ouvrir une carte ou un jeu sauvegardé</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="270"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="273"/>
        <source>Driving Panel</source>
        <translation>Poste de conduite</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="386"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="386"/>
        <location filename="../gui/Window.cpp" line="424"/>
        <source>NadiRail maps (*.tml)</source>
        <translation>Cartes NadiRail (*.tml)</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="398"/>
        <location filename="../gui/Window.cpp" line="433"/>
        <source>Critical Error</source>
        <translation>Erreur critique</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="398"/>
        <source>Failed to read file</source>
        <translation>La lecture du fichier a échouée</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="433"/>
        <source>Failed to save file</source>
        <translation>La sauvegarde du fichier a échouée</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="424"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <source>Would you like to save your work?</source>
        <translation type="vanished">Voulez vous enregistrer les modifications ?</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="501"/>
        <location filename="../gui/Window.cpp" line="531"/>
        <source>No platform</source>
        <translation>Pas de quai</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="501"/>
        <location filename="../gui/Window.cpp" line="531"/>
        <source>There is no platform in your map. Please create at least one platform using the editor.</source>
        <translation>Il n&apos;y a pas de quai dans votre carte. Veuillez créer au moins un quai en utilisant l&apos;éditeur.</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="989"/>
        <source>Open Image</source>
        <translation>Ouvrir une image</translation>
    </message>
    <message>
        <location filename="../gui/Window.cpp" line="989"/>
        <source>Image Files (*.png *.jpg *.bmp *.tif)</source>
        <translation>Fichiers image (*.png *.jpg *.bmp *.tif)</translation>
    </message>
</context>
</TS>
