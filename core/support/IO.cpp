#include "core/support/IO.h"

bool IO::readTML(Scene* scene_, View* view_, QString filename_)
{
	
	// Remove all the layers in the scene
	foreach(Layer* layer, *(scene_->getListLayers()))
	{
		delete layer;
		scene_->getListLayers()->clear();
	}
	
	tinyxml2::XMLDocument* xmlDoc = new tinyxml2::XMLDocument();
	scene_->setFilename(filename_);
	
	if(xmlDoc->LoadFile(QDir::toNativeSeparators(filename_).toStdString().c_str()) == tinyxml2::XML_SUCCESS)
	{
		// Switch the locale
		char * oldLocale;
		oldLocale = setlocale( LC_ALL, 0 );
		setlocale( LC_ALL, "C" );
	
		// Get root
		tinyxml2::XMLNode* root = xmlDoc->FirstChild();

		if (root == NULL)
			return false;

		// Retrieve scene extent
		tinyxml2::XMLElement *elementScene = root->FirstChildElement("scene");
		if (elementScene == NULL)
			return false;
		else
		{
			float x, y, width, height;

			if(elementScene->QueryFloatAttribute("x", &x) != tinyxml2::XML_SUCCESS)
				return false;
			if(elementScene->QueryFloatAttribute("y", &y) != tinyxml2::XML_SUCCESS)
				return false;
			if(elementScene->QueryFloatAttribute("width", &width) != tinyxml2::XML_SUCCESS)
				return false;
			if(elementScene->QueryFloatAttribute("height", &height) != tinyxml2::XML_SUCCESS)
				return false;
			
			scene_->setSceneRect(x, y, width, height);
		}

		// Retrieve view extent
		tinyxml2::XMLElement *elementView = root->FirstChildElement("view");
		if (elementView == NULL)
			return false;
		else
		{
			double m11, m12, m13, m21, m22, m23, m31, m32, m33;
			int scrollH, scrollV;

			if(elementView->QueryDoubleAttribute("m11", &m11) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementView->QueryDoubleAttribute("m12", &m12) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryDoubleAttribute("m13", &m13) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryDoubleAttribute("m21", &m21) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryDoubleAttribute("m22", &m22) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryDoubleAttribute("m23", &m23) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementView->QueryDoubleAttribute("m31", &m31) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryDoubleAttribute("m32", &m32) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryDoubleAttribute("m33", &m33) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryIntAttribute("scrollH", &scrollH) != tinyxml2::XML_SUCCESS)
				return false;
				
			if(elementView->QueryIntAttribute("scrollV", &scrollV) != tinyxml2::XML_SUCCESS)
				return false;
			
			view_->setTransform(QTransform(m11, m12, m13, m21, m22, m23, m31, m32, m33));
			view_->horizontalScrollBar()->setValue(scrollH);
			view_->verticalScrollBar()->setValue(scrollV);
		}

		// Extract layers
		tinyxml2::XMLElement *elementLayers = root->FirstChildElement("layers");
		if (elementLayers == NULL)
			return false;

		const char* name;
		int colorR, colorG, colorB, level;
		bool visible;

		tinyxml2::XMLElement *elementLayer = elementLayers->FirstChildElement("layer");

		while (elementLayer != NULL)	
		{
			if(elementLayer->QueryStringAttribute("name", &name) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementLayer->QueryIntAttribute("colorR", &colorR) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementLayer->QueryIntAttribute("colorG", &colorG) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementLayer->QueryIntAttribute("colorB", &colorB) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementLayer->QueryBoolAttribute("visible", &visible) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementLayer->QueryIntAttribute("level", &level) != tinyxml2::XML_SUCCESS)
				return false;
			
			scene_->getListLayers()->append(new Layer(name, QColor(colorR, colorG, colorB), visible, level));
			
			elementLayer = elementLayer->NextSiblingElement("layer");
		}
		
		// Retrieve Scenario
		tinyxml2::XMLElement *elementScenario = root->FirstChildElement("scenario");
		if (elementScenario == NULL)
			return false;
		else
		{
			const char* messageScenario;
			const char* messageVictory;
			bool locsAllowed;
			bool carsAllowed;
			
			tinyxml2::XMLElement *elementMessageScenario = elementScenario->FirstChildElement("messageScenario");
			if (elementMessageScenario == NULL)
				return false;

			if(elementMessageScenario->QueryStringAttribute("text", &messageScenario) != tinyxml2::XML_SUCCESS)
				return false;
				
			tinyxml2::XMLElement *elementMessageVictory = elementScenario->FirstChildElement("messageVictory");
			if (elementMessageVictory == NULL)
				return false;

			if(elementMessageVictory->QueryStringAttribute("text", &messageVictory) != tinyxml2::XML_SUCCESS)
				return false;

			tinyxml2::XMLElement *elementAddLocs = elementScenario->FirstChildElement("addLocs");
			if (elementAddLocs == NULL)
				return false;

			if(elementAddLocs->QueryBoolAttribute("isAllowed", &locsAllowed) != tinyxml2::XML_SUCCESS)
				return false;

			tinyxml2::XMLElement *elementAddCars = elementScenario->FirstChildElement("addCars");
			if (elementAddCars == NULL)
				return false;

			if(elementAddCars->QueryBoolAttribute("isAllowed", &carsAllowed) != tinyxml2::XML_SUCCESS)
				return false;
				
			scene_->setMessageScenario(messageScenario);
			scene_->setMessageVictory(messageVictory);
			scene_->setLocsAllowed(locsAllowed);
			scene_->setCarsAllowed(carsAllowed);
		}

		// Extract shapes
		QFileInfo fileInfo(filename_);
		QString dir = fileInfo.absolutePath();
		
		tinyxml2::XMLElement* elementShapes = root->FirstChildElement("shapes");
		if (elementShapes == NULL)
			return false;

		tinyxml2::XMLElement* elementShape = elementShapes->FirstChildElement("shape");

		// Iterate each parent <shape> and call readTMLFeature()
		while (elementShape != NULL)	
		{	
			if(readTMLFeature(scene_, dir, elementShape) == NULL)
				return false;

			elementShape = elementShape->NextSiblingElement();
		}
		
		// Extract cars
		tinyxml2::XMLElement* elementCars = root->FirstChildElement("cars");
		if (elementCars == NULL)
			return false;

		tinyxml2::XMLElement* elementCar = elementCars->FirstChildElement("car");
		
		// Iterate each parent <car> and call readTMLCar()
		while (elementCar != NULL)	
		{	
			if(readTMLCar(scene_, dir, elementCar) == NULL)
				return false;

			elementCar = elementCar->NextSiblingElement();
		}
		
		// Hook the cars
		scene_->checkHookCollisions();
		
		//restore locale
		setlocale( LC_ALL, oldLocale );
	}
	else	
	{
		delete xmlDoc;
		return false;
	}

	return true;
}

ShapeItem* IO::readTMLFeature(Scene* scene_, QString dir_, tinyxml2::XMLElement* elementShape_)
{
	// Extract layer
	const char* layer;
	if(elementShape_->QueryStringAttribute("layer", &layer) != tinyxml2::XML_SUCCESS)
				return NULL;
			
	// Extract nodes
	QList<QPointF> listNodes = readTMLNodes(elementShape_);
	if(listNodes.isEmpty())
		return NULL;
	
	int type;
	
	if(elementShape_->QueryIntAttribute("type", &type) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	// Extract address
	int address;
	
	if(elementShape_->QueryIntAttribute("address", &address) != tinyxml2::XML_SUCCESS)
		return NULL;
			
	// Check the type of each shape and take create the associated ShapeItem. If block (type 1), call this function recursively
	switch(type)
	{
		// BlockItem
		case 1:
		{
			tinyxml2::XMLElement *elementBlock = elementShape_->FirstChildElement();
			QList<QGraphicsItem*> listItems;

			while (elementBlock != NULL)	
			{
				// Recursive call to add child items
				ShapeItem* shape = IO::readTMLFeature(scene_, dir_, elementBlock);

				if(shape==NULL)
					return NULL;
				
				// We have to cast a ShapeItem which is a block explicitely in QGraphicsItemGroup otherwise the adress is different (multiple inheritance)
				if(shape->getType() == 1)
					listItems.append(dynamic_cast<QGraphicsItemGroup*>(shape));
				else
					listItems.append(shape);
			
				elementBlock = elementBlock->NextSiblingElement("shape");
			}
			
			BlockItem* block = new BlockItem(scene_, scene_->searchLayer(QString(layer)), listNodes.at(0), listItems, false);
		
			return dynamic_cast<ShapeItem*>(block);
		}
		break;
		
		// ArcItem
		case 2:
		case 21:
		case 22:
		{
			tinyxml2::XMLElement *elementWidth = elementShape_->FirstChildElement("width");
			double gauge;
			double crosstie;
			if(elementWidth->QueryDoubleAttribute("gauge", &gauge) != tinyxml2::XML_SUCCESS)
				return NULL;
			if(elementWidth->QueryDoubleAttribute("crosstie", &crosstie) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			ArcItem* arc = new ArcItem(scene_, scene_->searchLayer(QString(layer)), listNodes.at(1), listNodes.at(2), listNodes.at(3), type, gauge, crosstie, false);
			arc->setAddress(address);
			
			// If this is a platform, fetch the name
			if(type == 21)
			{
				tinyxml2::XMLElement *elementPlatform = elementShape_->FirstChildElement("platform");
				const char* string;
				if(elementPlatform->QueryStringAttribute("name", &string) != tinyxml2::XML_SUCCESS)
					return NULL;
			
				arc->setName(string);
				
				// Read conditions if any
				if(readTMLConditions(arc, elementShape_) == false)
					return NULL;
			}
			
			return dynamic_cast<ShapeItem*>(arc); 
		}
		break;
		
		// ImageItem
		case 3:
		{
			tinyxml2::XMLElement *elementImage = elementShape_->FirstChildElement("image");
			
			const char* filename;
			if(elementImage->QueryStringAttribute("filename", &filename) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			double scale, rotation;
			if(elementImage->QueryDoubleAttribute("scale", &scale) != tinyxml2::XML_SUCCESS)
				return NULL;

			if(elementImage->QueryDoubleAttribute("rotation", &rotation) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			QString imagePath = dir_ + QString(filename).remove(0,2); // Remove the ../ of the relative path
			
			QPixmap pixmap = QPixmap(imagePath).transformed(QTransform::fromScale(1, -1));
			ImageItem* image = new ImageItem(scene_, scene_->searchLayer(QString(layer)), listNodes.at(0), pixmap, imagePath);
			
			ShapeItem* shapeImage = dynamic_cast<ShapeItem*>(image);
			shapeImage->setRotation(rotation);
			shapeImage->setScale(scale);
		
			// Make the image not selectable if this is the background image
			if(QString(layer) == "BACKGROUND")
			{
				image->ShapeItem::setFlag(QGraphicsItem::ItemIsSelectable, false);
				image->ShapeItem::setFlag(QGraphicsItem::ItemIsFocusable, false);
				image->ShapeItem::setZValue(0);
			}
			
			return shapeImage;
		}
		break;
		
		// PathItem
		case 4:
		case 41:
		case 42:
		{
			tinyxml2::XMLElement *elementWidth = elementShape_->FirstChildElement("width");
			double gauge;
			double crosstie;
			if(elementWidth->QueryDoubleAttribute("gauge", &gauge) != tinyxml2::XML_SUCCESS)
				return NULL;
			if(elementWidth->QueryDoubleAttribute("crosstie", &crosstie) != tinyxml2::XML_SUCCESS)
				return NULL;

					
			PathItem* path = new PathItem(scene_, scene_->searchLayer(QString(layer)), listNodes.at(0), false, type, gauge, crosstie);
			
			for(int i=1; i<listNodes.size(); i++)
				path->lineTo(listNodes.at(i));
			
			path->reconstructNodesfromPath();
			
			// If this is a platform, fetch the name
			if(type == 41)
			{
				tinyxml2::XMLElement *elementPlatform = elementShape_->FirstChildElement("platform");
				const char* string;
				if(elementPlatform->QueryStringAttribute("name", &string) != tinyxml2::XML_SUCCESS)
					return NULL;
			
				path->setName(string);
				
				// Read conditions if any
				if(readTMLConditions(path, elementShape_) == false)
					return NULL;
			}
			
			path->setAddress(address);
			return dynamic_cast<ShapeItem*>(path);
		}
		break;
		
		// PointsItem
		case 6:
		{
			tinyxml2::XMLElement *elementWidth = elementShape_->FirstChildElement("width");
			double gauge;
			double crosstie;
			if(elementWidth->QueryDoubleAttribute("gauge", &gauge) != tinyxml2::XML_SUCCESS)
				return NULL;
			if(elementWidth->QueryDoubleAttribute("crosstie", &crosstie) != tinyxml2::XML_SUCCESS)
				return NULL;
				
			PointsItem* pointsItem_ = new PointsItem(scene_, scene_->searchLayer(QString(layer)), listNodes.at(0), listNodes.at(1), listNodes.at(2), gauge, crosstie);
			
			pointsItem_->setAddress(address);
			return dynamic_cast<ShapeItem*>(pointsItem_);
		}
		break;
		
		// TextItem
		case 7:
		{
			const char* string;
			const char* fontString;
			bool customColor;
			double rotation;
			
			tinyxml2::XMLElement *elementText = elementShape_->FirstChildElement("text");
			
			if(elementText->QueryStringAttribute("string", &string) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			if(elementText->QueryBoolAttribute("customColor", &customColor) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			TextItem* text = new TextItem(scene_, scene_->searchLayer(QString(layer)), listNodes.at(0), string);
			
			if(customColor == true)
			{
				int colorR, colorG, colorB;
				
				if(elementText->QueryIntAttribute("colorR", &colorR) != tinyxml2::XML_SUCCESS)
				return NULL;
			
				if(elementText->QueryIntAttribute("colorG", &colorG) != tinyxml2::XML_SUCCESS)
					return NULL;
				
				if(elementText->QueryIntAttribute("colorB", &colorB) != tinyxml2::XML_SUCCESS)
					return NULL;
				
				text->setCustomColor(QColor(colorR, colorG, colorB));
			}
			
			if(elementText->QueryStringAttribute("font", &fontString) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			if(elementText->QueryDoubleAttribute("rotation", &rotation) != tinyxml2::XML_SUCCESS)
				return NULL;
			
			QFont font;
			font.fromString(fontString);
			text->setFont(font);
			dynamic_cast<ShapeItem*>(text)->setRotation(rotation);
			
			return dynamic_cast<ShapeItem*>(text);
		}
		break;
	}
	
	return NULL;
}

QList<QPointF> IO::readTMLNodes(tinyxml2::XMLElement* elementShape_)
{
	tinyxml2::XMLElement *elementNode = elementShape_->FirstChildElement("node");
	
	QList<QPointF> listNodes;
	
	while (elementNode != NULL)	
	{
		double x, y;
		QPointF nodePoint;
		
		if(elementNode->QueryDoubleAttribute("x", &x) != tinyxml2::XML_SUCCESS)
			return listNodes;
	
		if(elementNode->QueryDoubleAttribute("y", &y) != tinyxml2::XML_SUCCESS)
			return listNodes;
		
		nodePoint.setX(x);
		nodePoint.setY(y);
		
		listNodes.append(nodePoint);
		
		elementNode = elementNode->NextSiblingElement("node");
	}
	
	return listNodes;
}

CarItem* IO::readTMLCar(Scene* scene_, QString dir_, tinyxml2::XMLElement* elementCar_)
{
	int type, colorR, colorG, colorB, maxSpeed, address1, address2;
	const char* name;
	const char* filename;
	bool direction, isLoaded;
	double x, y, x2, y2;
	double length, width;
	
	if(elementCar_->QueryIntAttribute("type", &type) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryStringAttribute("name", &name) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryDoubleAttribute("length", &length) != tinyxml2::XML_SUCCESS)
		return NULL;
		
	if(elementCar_->QueryDoubleAttribute("width", &width) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryIntAttribute("colorR", &colorR) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryIntAttribute("colorG", &colorG) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryIntAttribute("colorB", &colorB) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryBoolAttribute("direction", &direction) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryIntAttribute("maxspeed", &maxSpeed) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryBoolAttribute("isloaded", &isLoaded) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	if(elementCar_->QueryStringAttribute("filename", &filename) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	if(elementCar_->QueryDoubleAttribute("x", &x) != tinyxml2::XML_SUCCESS)
		return NULL;

	if(elementCar_->QueryDoubleAttribute("y", &y) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	if(elementCar_->QueryDoubleAttribute("x2", &x2) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	if(elementCar_->QueryDoubleAttribute("y2", &y2) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	if(elementCar_->QueryIntAttribute("address1", &address1) != tinyxml2::XML_SUCCESS)
		return NULL;
	
	if(elementCar_->QueryIntAttribute("address2", &address2) != tinyxml2::XML_SUCCESS)
		return NULL;

	// Create the CarItem
	
	QString imagePath = "";
	
	if(QString(filename) != "")
		imagePath = dir_ + QString(filename).remove(0,2);
	
	CarItem* car = new CarItem(scene_, QString(name), maxSpeed, length, width, type, isLoaded, imagePath, QColor(colorR, colorG, colorB));
	
	bool status = car->insert(QPointF(x, y), QPointF(x2, y2), address1, address2);
	
	if(status == true)
		scene_->setSceneSelectItem(car);
	else
		delete car;
	
	return dynamic_cast<CarItem*>(car); 
}

bool IO::readTMLConditions(ShapeItem* shape_, tinyxml2::XMLElement* elementShape_)
{
	tinyxml2::XMLElement* elementConditions = elementShape_->FirstChildElement("conditions");
				
	if (elementConditions != NULL)
	{
		QList<Condition> listCond; 
						
		int nbCars, type;
		bool loaded;
		Condition cond;

		tinyxml2::XMLElement *elementCondition = elementConditions->FirstChildElement("condition");

		while (elementCondition != NULL)	
		{
			if(elementCondition->QueryIntAttribute("type", &type) != tinyxml2::XML_SUCCESS)
				return false;
			
			if(elementCondition->QueryIntAttribute("nbCars", &nbCars) != tinyxml2::XML_SUCCESS)
				return false;
							
			if(elementCondition->QueryBoolAttribute("loaded", &loaded) != tinyxml2::XML_SUCCESS)
				return false;
				
			cond.nbCars = nbCars;
			cond.type = type;
			cond.loaded = loaded;
									
			listCond.append(cond);
			
			elementCondition = elementCondition->NextSiblingElement("condition");
		}

		shape_->setListConditions(listCond);
	}
	
	return true;
}

bool IO::writeTML(Scene* scene_, View* view_, QString filename_)
{
	// Switch the locale
	char * oldLocale;
	oldLocale = setlocale( LC_ALL, 0 );
	setlocale( LC_ALL, "C" );

	QDir dir = QDir(filename_);
	
	QList<QGraphicsItem*> listItems = scene_->items();
	
	tinyxml2::XMLDocument* xmlDoc = new tinyxml2::XMLDocument();
	tinyxml2::XMLNode* root = xmlDoc->NewElement("testa");
	xmlDoc->InsertFirstChild(root);
	
	// Scene extent
	
	tinyxml2::XMLElement *elementScene = xmlDoc->NewElement("scene");
	root->InsertEndChild(elementScene);
	
	elementScene->SetAttribute("x", scene_->sceneRect().x());
	elementScene->SetAttribute("y", scene_->sceneRect().y());
	elementScene->SetAttribute("width", scene_->sceneRect().width());
	elementScene->SetAttribute("height", scene_->sceneRect().height());

	// View extent
	
	tinyxml2::XMLElement *elementView = xmlDoc->NewElement("view");
	root->InsertEndChild(elementView);
	
	QTransform transform = view_->transform();
	
	elementView->SetAttribute("m11", transform.m11());
	elementView->SetAttribute("m12", transform.m12());
	elementView->SetAttribute("m13", transform.m13());
	elementView->SetAttribute("m21", transform.m21());
	elementView->SetAttribute("m22", transform.m22());
	elementView->SetAttribute("m23", transform.m23());
	elementView->SetAttribute("m31", transform.m31());
	elementView->SetAttribute("m32", transform.m32());
	elementView->SetAttribute("m33", transform.m33());
	
	elementView->SetAttribute("scrollH", view_->horizontalScrollBar()->value());
	elementView->SetAttribute("scrollV", view_->verticalScrollBar()->value());

	// Save layers
	
	tinyxml2::XMLElement *elementLayers = xmlDoc->NewElement("layers");
	root->InsertEndChild(elementLayers);
	
	for(int i=0; i<scene_->getListLayers()->size(); i++)
	{
		Layer* layer = scene_->getListLayers()->at(i);
		tinyxml2::XMLElement *elementLayer = xmlDoc->NewElement("layer");
		elementLayer->SetAttribute("name", layer->getName().toStdString().c_str());
		elementLayer->SetAttribute("colorR", layer->getColor().red());
		elementLayer->SetAttribute("colorG", layer->getColor().green());
		elementLayer->SetAttribute("colorB", layer->getColor().blue());
		elementLayer->SetAttribute("visible", layer->isVisible());
		elementLayer->SetAttribute("level", layer->getLevel());
		elementLayers->InsertEndChild(elementLayer);
	}
	
	// Save Scenario
		
	tinyxml2::XMLElement *elementScenario = xmlDoc->NewElement("scenario");
	root->InsertEndChild(elementScenario);
	
	tinyxml2::XMLElement *elementMessageScenario = xmlDoc->NewElement("messageScenario");
	elementMessageScenario->SetAttribute("text", scene_->getMessageScenario().toStdString().c_str());
	elementScenario->InsertEndChild(elementMessageScenario);
	
	tinyxml2::XMLElement *elementMessageVictory = xmlDoc->NewElement("messageVictory");
	elementMessageVictory->SetAttribute("text", scene_->getMessageVictory().toStdString().c_str());
	elementScenario->InsertEndChild(elementMessageVictory);
	
	tinyxml2::XMLElement *elementAddLocs = xmlDoc->NewElement("addLocs");
	elementAddLocs->SetAttribute("isAllowed", scene_->getLocsAllowed());
	elementScenario->InsertEndChild(elementAddLocs);
	
	tinyxml2::XMLElement *elementAddCars = xmlDoc->NewElement("addCars");
	elementAddCars->SetAttribute("isAllowed", scene_->getCarsAllowed());
	elementScenario->InsertEndChild(elementAddCars);
	
	// Save shapes
	
	tinyxml2::XMLElement *elementShapes = xmlDoc->NewElement("shapes");
	root->InsertEndChild(elementShapes);
	
	foreach(QGraphicsItem* item, listItems)
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
		
		if(shape)
		{
			// If the shape is not part of a block, burn it. All those which are part of a block are burned through the writeTMLBlock method
			if(shape->getType()==1 and (dynamic_cast<BlockItem*>(item))->QGraphicsItemGroup::parentItem()==0)
				IO::writeTMLBlock(dir, listItems, item, xmlDoc, elementShapes);
			// If standard feature
			else if(shape->getType()!=1 and shape->parentItem() == 0) 
				IO::writeTMLFeature(dir, item, xmlDoc, elementShapes);
		}
	}
	
	// Save trains
	
	tinyxml2::XMLElement *elementCars = xmlDoc->NewElement("cars");
	root->InsertEndChild(elementCars);
	
	foreach(QGraphicsItem* item, listItems)
	{
		CarItem* car = dynamic_cast<CarItem*>(item);
		
		if(car)
		{
			IO::writeTMLCar(dir, car, xmlDoc, elementCars);
		}
	}
	
	// Save file
		
	tinyxml2::XMLError result = xmlDoc->SaveFile(QDir::toNativeSeparators(filename_).toStdString().c_str());
	delete xmlDoc;
	
	// Restore locale
	setlocale( LC_ALL, oldLocale );

	
	if(result == tinyxml2::XML_SUCCESS)
		return true;

	return false;
}

void IO::writeTMLBlock(QDir dir_, QList<QGraphicsItem*>& listItems_, QGraphicsItem* item_, tinyxml2::XMLDocument* xmlDoc_, tinyxml2::XMLElement* elementShapes_)
{
	// Block header
	tinyxml2::XMLElement *elementBlock;
	elementBlock = xmlDoc_->NewElement("shape");
	
	elementShapes_->InsertEndChild(elementBlock);
	
	BlockItem* block_ = dynamic_cast<BlockItem*>(item_);
	elementBlock->SetAttribute("layer", block_->getLayer()->getName().toStdString().c_str());
	elementBlock->SetAttribute("type", 1);
	
	// Remove this block from the list
	listItems_.removeAll(item_);

	// Iterate child block
	QList<QGraphicsItem*> listChilds = block_->QGraphicsItemGroup::childItems();
	
	foreach(QGraphicsItem* childItem, listChilds)
	{
		// If a block is contained in the block
		if((dynamic_cast<ShapeItem*>(childItem))->getType()==1)
		{
			listItems_.removeAll(childItem);
			IO::writeTMLBlock(dir_, listItems_, childItem, xmlDoc_, elementBlock);
		}
		// If normal feature
		else
		{
			listItems_.removeAll(childItem);
			IO::writeTMLFeature(dir_, childItem, xmlDoc_, elementBlock);
		}
	}
	
	// Save block node
	tinyxml2::XMLElement *elementNode = xmlDoc_->NewElement("node");
	elementNode->SetAttribute("x", block_->getListNodes().at(0)->pos().x());
	elementNode->SetAttribute("y", block_->getListNodes().at(0)->pos().y());
	elementBlock->InsertEndChild(elementNode);
}

void IO::writeTMLFeature(QDir dir_, QGraphicsItem* item_, tinyxml2::XMLDocument* xmlDoc_, tinyxml2::XMLElement* elementShapes_)
{
	ShapeItem* shape = dynamic_cast<ShapeItem*>(item_);

	// save basic information
	tinyxml2::XMLElement *elementShape;
	elementShape = xmlDoc_->NewElement("shape");
	elementShapes_->InsertEndChild(elementShape);
	
	elementShape->SetAttribute("layer", shape->getLayer()->getName().toStdString().c_str());
	elementShape->SetAttribute("type", shape->getType());
	
	elementShape->SetAttribute("address", QString::number((qlonglong)shape).toStdString().c_str());
	
	// save specific information
	switch(shape->getType())
	{
		// Case Arc
		case 2:
		case 21:
		case 22:
		{
			ArcItem* arc = dynamic_cast<ArcItem*>(shape);
			
			// if platform
			if(shape->getType() == 21)
			{
				tinyxml2::XMLElement *elementPlatform = xmlDoc_->NewElement("platform");
				elementPlatform->SetAttribute("name", arc->getName().toStdString().c_str());
				elementShape->InsertEndChild(elementPlatform);
				
				// Write conditions if any
				writeTMLCondition(arc, xmlDoc_, elementShape);
			}
			
			tinyxml2::XMLElement *elementWidth = xmlDoc_->NewElement("width");
			elementWidth->SetAttribute("gauge", arc->getGauge());
			elementWidth->SetAttribute("crosstie", arc->getCrosstie());				
			elementShape->InsertEndChild(elementWidth);
			break;
		}
		
		// Case ImageItem
		 case 3:
		 {
			ImageItem* image = dynamic_cast<ImageItem*>(shape);

			tinyxml2::XMLElement *elementImage = xmlDoc_->NewElement("image");
			elementImage->SetAttribute("filename", dir_.relativeFilePath(image->getFilename()).toStdString().c_str());
			elementImage->SetAttribute("scale", image->ShapeItem::scale());
			elementImage->SetAttribute("rotation", image->ShapeItem::rotation());
			elementShape->InsertEndChild(elementImage);
			break;
		}
		
		// Case Straight
		case 4:
		case 41:
		case 42:
		{
			PathItem* path = dynamic_cast<PathItem*>(shape);
			
			// If platform
			if(path->getType() == 41)
			{
				tinyxml2::XMLElement *elementPlatform = xmlDoc_->NewElement("platform");
				elementPlatform->SetAttribute("name", path->getName().toStdString().c_str());
				elementShape->InsertEndChild(elementPlatform);
				
				// Write conditions if any
				writeTMLCondition(path, xmlDoc_, elementShape);
			}

			tinyxml2::XMLElement *elementWidth = xmlDoc_->NewElement("width");
			elementWidth->SetAttribute("gauge", path->getGauge());
			elementWidth->SetAttribute("crosstie", path->getCrosstie());
		
			elementShape->InsertEndChild(elementWidth);
			break;
		}
		
		// Case TextItem
		 case 7:
		 {
			TextItem* text = dynamic_cast<TextItem*>(shape);

			tinyxml2::XMLElement *elementText = xmlDoc_->NewElement("text");
			elementText->SetAttribute("string", text->getText().toStdString().c_str());
			elementText->SetAttribute("customColor", text->hasCustomColor());
			elementText->SetAttribute("colorR", text->getCustomColor().red());
			elementText->SetAttribute("colorG", text->getCustomColor().green());
			elementText->SetAttribute("colorB", text->getCustomColor().blue());
			elementText->SetAttribute("font", text->getFont().toString().toStdString().c_str());
			
			elementText->SetAttribute("rotation", text->ShapeItem::rotation());
			elementShape->InsertEndChild(elementText);
							
			break;
		}
		
		// Case Points
		case 6:
		{
			PointsItem* points = dynamic_cast<PointsItem*>(shape);
			tinyxml2::XMLElement *elementWidth = xmlDoc_->NewElement("width");
			elementWidth->SetAttribute("gauge", points->getGauge());
			elementWidth->SetAttribute("crosstie", points->getCrosstie());
			elementShape->InsertEndChild(elementWidth);
			break;
		}
	}
	
	// Save nodes
	QList<Node*> listNodes = shape->getListNodes();
	foreach(Node* node, listNodes)
	{
		tinyxml2::XMLElement *elementNode = xmlDoc_->NewElement("node");
		elementNode->SetAttribute("x", node->pos().x());
		elementNode->SetAttribute("y", node->pos().y());
		elementShape->InsertEndChild(elementNode);
	}	
}
		
void IO::writeTMLCar(QDir dir_, CarItem* car_, tinyxml2::XMLDocument* xmlDoc_, tinyxml2::XMLElement* elementCars_)
{
	// save basic information
	tinyxml2::XMLElement *elementCar;
	elementCar = xmlDoc_->NewElement("car");
	elementCars_->InsertEndChild(elementCar);
	
	elementCar->SetAttribute("type", car_->getType());
	elementCar->SetAttribute("name", car_->getName().toStdString().c_str());
	elementCar->SetAttribute("length", car_->getLength());
	elementCar->SetAttribute("width", car_->getWidth());
	elementCar->SetAttribute("colorR", car_->getColor().red());
	elementCar->SetAttribute("colorG", car_->getColor().green());
	elementCar->SetAttribute("colorB", car_->getColor().blue());
	elementCar->SetAttribute("direction", car_->getDirection());
	elementCar->SetAttribute("maxspeed", car_->getMaxSpeed());
	elementCar->SetAttribute("isloaded", car_->getIsLoaded());
	
	QString filename = car_->getFilename();
	if(filename != "")
	filename = dir_.relativeFilePath(car_->getFilename());
	elementCar->SetAttribute("filename", filename.toStdString().c_str());
	
	elementCar->SetAttribute("x", car_->pos().x());
	elementCar->SetAttribute("y", car_->pos().y());
	elementCar->SetAttribute("x2", car_->getPos(1).x());
	elementCar->SetAttribute("y2", car_->getPos(1).y());
	
	elementCar->SetAttribute("address1", QString::number((qlonglong)car_->getShapeItem(0)).toStdString().c_str());
	elementCar->SetAttribute("address2", QString::number((qlonglong)car_->getShapeItem(1)).toStdString().c_str());
}

void IO::writeTMLCondition(ShapeItem* shape_, tinyxml2::XMLDocument* xmlDoc_, tinyxml2::XMLElement* elementShape_)
{
	// Check and write conditions if any
	QList<Condition> listCond = shape_->getListConditions();
	
	if(listCond.size() > 0)
	{
		// Create a tree <conditions>
		tinyxml2::XMLElement *elementConditions = xmlDoc_->NewElement("conditions");
		elementShape_->InsertEndChild(elementConditions);

		// Add a <condition> foreach condition
		foreach(Condition cond, listCond)
		{
			tinyxml2::XMLElement *elementCondition = xmlDoc_->NewElement("condition");
			elementCondition->SetAttribute("nbCars", cond.nbCars);
			elementCondition->SetAttribute("loaded", cond.loaded);
			elementCondition->SetAttribute("type", cond.type);
			elementConditions->InsertEndChild(elementCondition);
		}			
	}
}
