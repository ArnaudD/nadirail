#include "core/support/Scene.h"
#include "gui/View.h"
#include "gui/Window.h"
#include <iostream>

Scene::Scene(Window *window_) : QGraphicsScene(window_)
{
	m_parentWindow = window_;
	connect(this, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

	// Initialization layer BACKGROUND
	Layer *layerBackground = new Layer("BACKGROUND", Qt::white, true, 0);
	m_layerList.append(layerBackground);
	
	// Initialization layer IMAGES
	Layer *layerImages = new Layer("IMAGES", Qt::white, true, 1);
	m_layerList.append(layerImages);
	
	// Initialization layer RAILWAYS
	Layer *layerRailways = new Layer("RAILWAYS", Qt::black, true, 2);
	m_layerList.append(layerRailways);

	// Initialization layer TRAINS
	Layer *layerTrains = new Layer("TRAINS", Qt::white, true, 2);
	m_layerList.append(layerTrains);
	
	// Initialization layer TEXT
	Layer *layerText = new Layer("TEXT", Qt::white, true, 3);
	m_layerList.append(layerText);

	// Status drawing
	m_statusEditor = false;
	m_drawingState = 0;
	m_activeFunction = drawingFunction::inactive;

	// Initialisation status constraints
	m_constraintOrtho_isActive = false;
	m_constraintSnapping_isActive = true;
	
	m_snappingDrawOn = false;
	m_snappingNode = new SnappingNode(this);
	m_snappingNode->snapToVertices(true);
	
	penSelection.setCosmetic(true);
	penSelection.setStyle(Qt::DotLine);
	
	// Filename
	m_filename = "";
	
	// m_car
	m_car = NULL;
	
	// Scenario;
	m_locsAllowed = true;
	m_carsAllowed = true;
	m_isScenarioValidated = false;
}

void Scene::clear()
{
	// Remove all items
	QGraphicsScene::clear();
	
	// Re-add the snapping node
	m_snappingNode = new SnappingNode(this);
	m_snappingNode->snapToVertices(true);
	
	// Filename
	m_filename = "";
}


void Scene::advance()
{
	QGraphicsScene::advance();
	
	if(m_listPlatformsScenario.size() > 0 and m_isScenarioValidated == false)
	{
	
		int nbTotalConditions = 0;
		int nbConditionsValidated = 0;
		
		// For each platform
		foreach(ShapeItem* shape, m_listPlatformsScenario)
		{
			// Get the colliding items
			QList<QGraphicsItem*> listItems = shape->collidingItems();
			
			// For each condition of the platform
			foreach(Condition cond, shape->getListConditions())
			{
				int nbCarsValidCond = 0;
				
				// For each car in list
				foreach(QGraphicsItem* item, listItems)
				{
					CarItem* car = dynamic_cast<CarItem*>(item);
					
					if(car)
					{
						// Check if the car is actually on the platform	
						if(car->getShapeItem(0) == shape or car->getShapeItem(1) == shape)
						{						
							// Check if the car validates the condition
							if(car->getIsLoaded() == cond.loaded and car->getType() == cond.type and car->getSpeed() == 0)
								nbCarsValidCond++;
						}
					}
				}
				
				if(nbCarsValidCond >= cond.nbCars)
					nbConditionsValidated++;
				
				nbTotalConditions++;
			}
		}

		if(nbConditionsValidated == nbTotalConditions)
		{
			QMessageBox::information(m_parentWindow, m_filename, m_messageVictory);
			
			// Otherwise the message keep appeating
			m_isScenarioValidated = true;
		}
	}
}


QString Scene::getFilename() const
{
	return m_filename;
}

void Scene::setFilename(QString filename_)
{
	m_filename = filename_;
}

void Scene::keyReleaseEvent(QKeyEvent *e)
{
	// Test if the scene is loaded
	if(m_statusEditor == false)
	{
		// Keys driving
		m_parentWindow->keyRelease(e->key());
		return;
	}
	
	QGraphicsScene::keyPressEvent(e);
}
void Scene::keyPressEvent(QKeyEvent *e)
{
	// Test if the scene is loaded
	if(m_statusEditor == false)
	{
		// Keys driving
		m_parentWindow->keyPress(e->key());
		return;
	}
	else
	{
		// Keys constraints
		switch (e->key())
		{
			
			case Qt::Key_F8:
				m_parentWindow->slot_ortho();
				break;
			case Qt::Key_F3:
				m_parentWindow->slot_snapping();
				break;
		}
	
		// Keys for edition
		if(m_drawingState == drawingFunction::inactive)
		{
			switch (e->key())
			{
				case Qt::Key_Escape:
				{
					if(selectedItems().size()==0)
						disableDrawingAll();
					else
						clearSelection();
					break;
				}
				case Qt::Key_Delete:
				{
					foreach (QGraphicsItem *item, selectedItems())
					{
						// If item exists
						if(item)
						{
							ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
							shape->removeFromScene();
						}
					}
					break;
				}
			}
		}
	}
	
	QGraphicsScene::keyPressEvent(e);
}

void Scene::mouseMoveEvent (QGraphicsSceneMouseEvent* e)
{
	m_parentWindow->setCoordStatusBar(e->scenePos().x(), e->scenePos().y());

	// update positions
	if(m_activeFunction != drawingFunction::inactive and m_activeFunction != drawingFunction::drawSelection and m_activeFunction != drawingFunction::changePlatformName)
	{
		// If drawing start, update m_pos1
		if(m_drawingState == 0)
			m_pos1 = snapping(e->scenePos());

		// If drawing an intermediary point or an end point, update m_pos2
		else if(m_drawingState > 0)
			m_pos2 = snapping(e->scenePos());
	}
	// don't take snapping into account while drawing selection
	else if(m_activeFunction == drawingFunction::drawSelection)
	{
		// If drawing start, update m_pos1
		if(m_drawingState == 0)
			m_pos1 = e->scenePos();

		// If drawing an intermediary point or an end point, update m_pos2
		else if(m_drawingState > 0)
			m_pos2 = e->scenePos();
	}

	// If Ortho engaged, update m_pos2
	if (m_constraintOrtho_isActive==true and m_activeFunction != drawingFunction::drawSelection and m_activeFunction != drawingFunction::drawImage)
		m_pos2 = computePosOrtho(m_pos1, m_pos2);

	// Update moving line
	if((m_activeFunction == drawingFunction::drawLine or m_activeFunction == drawingFunction::drawTunnelLine) and m_drawingState == 1)
		m_pathConstruction->updateLastPos(m_pos2);
	
	// Update moving platform
	if(m_activeFunction == drawingFunction::drawPlatformLine and m_drawingState == 1)
	{
		if(MathCoord::distance(m_pos1, m_pos2) < 40)
		{
			qreal az = MathCoord::azimuth(m_pos1, m_pos2);
			m_pos2 = MathCoord::projectPoint(m_pos1, az, 40);
		}

		m_pathConstruction->updateLastPos(m_pos2);
	}
	
	// Update moving points
	if(m_activeFunction == drawingFunction::drawPoints and m_drawingState == 1 )
		m_pathConstruction->updateLastPos(m_pos2);
	
	if(m_activeFunction == drawingFunction::drawPoints and m_drawingState == 2 )
		m_pathConstruction2->updateLastPos(m_pos2);
	
	// Update moving arc
	else if((m_activeFunction == drawingFunction::drawArc or m_activeFunction == drawingFunction::drawPlatformArc or m_activeFunction == drawingFunction::drawTunnelArc) and m_drawingState==1)
		m_pathConstruction->updateLastPos(m_pos2);

	else if((m_activeFunction == drawingFunction::drawArc or m_activeFunction == drawingFunction::drawPlatformArc or m_activeFunction == drawingFunction::drawTunnelArc) and m_drawingState==2)
		m_arcConstruction->setArc(m_pos1,m_pos3,m_pos2);

	// Update moving image
	else if(m_activeFunction == drawingFunction::drawImage and m_drawingState == 1)
	{
		double height_ = fabs(m_pos2.x()-m_pos1.x())*(double)m_pixmap.height()/(double)m_pixmap.width();
		m_pos2.setY(m_pos1.y() + height_);
		
		m_pathConstruction->updateRectangle(m_pos1, m_pos2);
	}
	
	// Update moving line for translation or copy
	else if((m_activeFunction == drawingFunction::translate or m_activeFunction == drawingFunction::copy) and m_drawingState==1)
	{
		m_pathConstruction->updateLastPos(m_pos2);
		
		// Move the block under the mouse
		QPointF shift = m_pos2-m_initPos;
		m_blockMoving->QGraphicsItemGroup::moveBy(shift.x(), shift.y());
		m_blockMoving->translateItem(shift, false);
	}
	
	// Update moving line when rotating
	else if(m_activeFunction == drawingFunction::rotate and m_drawingState > 0)
	{
		m_pathConstruction->updateLastPos(m_pos2);
	
		// Rotate block
			if(m_drawingState == 2)
			{
				qreal angle = MathCoord::angle(m_pos2, m_pos1, m_initPos);
				m_blockMoving->rotateItem(m_pos1, angle);
			}
	}

	// Update moving line when scaling
	else if(m_activeFunction == drawingFunction::scale and m_drawingState > 0)
	{
		m_pathConstruction->updateLastPos(m_pos2);
	
		// Scale block
			if(m_drawingState == 2)
			{
				qreal scaleFactor = MathCoord::distance(m_pos1, m_pos2) / MathCoord::distance(m_pos1,  m_initPos);
				m_blockMoving->scaleItem(m_pos1, scaleFactor);
			}
	}

	// Update selection rectangle
	else if(m_activeFunction == drawingFunction::drawSelection and m_drawingState == 1)
	{
		if (m_pos2.y()<m_pos1.y())
			penSelection.setColor(Qt::blue);
		else
			penSelection.setColor(Qt::green);

		m_rectSelection->setPen(penSelection);
		m_rectSelection->setRect(QRectF(m_pos1,QSizeF(m_pos2.x()-m_pos1.x(),m_pos2.y()-m_pos1.y())));
	}
	
	if(m_activeFunction != drawingFunction::inactive and m_activeFunction != drawingFunction::drawSelection and m_activeFunction != drawingFunction::changePlatformName)
		m_initPos = snapping(e->scenePos());
	else
		m_initPos = e->scenePos();
		
	update();
	QGraphicsScene::mouseMoveEvent(e);
}

void Scene::mousePressEvent (QGraphicsSceneMouseEvent* e)
{
	if(m_activeFunction != drawingFunction::inactive and m_activeFunction != drawingFunction::drawSelection and m_activeFunction != drawingFunction::changePlatformName)
		m_initPos = snapping(e->scenePos());
	
	if(e->button() == Qt::RightButton)
    {
		// Terminate copy on right clic
		if(m_activeFunction == drawingFunction::copy)			
			disableDrawingAll();	
	}
	else if(e->button() == Qt::LeftButton)
	{
		// Add text on clic
		if(m_activeFunction == drawingFunction::drawText)
		{
			new TextItem(this, searchLayer("TEXT"), m_pos1, "text");
			disableDrawingAll();
		}
		// Add line on clic
		else if(m_activeFunction == drawingFunction::drawLine or m_activeFunction == drawingFunction::drawPlatformLine or m_activeFunction == drawingFunction::drawTunnelLine or m_activeFunction == drawingFunction::drawPoints)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
					if(m_activeFunction == drawingFunction::drawLine or m_activeFunction == drawingFunction::drawPoints)
						initLineConstruction(4);
					if(m_activeFunction == drawingFunction::drawPlatformLine)
						initLineConstruction(41);
					if(m_activeFunction == drawingFunction::drawTunnelLine)
						initLineConstruction(42);
				break;
				// On second clic, create line on scene
				case 1:
					// If it's a platform, get its name
					if(m_activeFunction == drawingFunction::drawPlatformLine)
					{
						m_pathConstruction->setName(getPlatformName(""));
						m_pathConstruction->clone();
						
						disableDrawingAll();
					}
					// Second clic points: create a second construction line
					else if(m_activeFunction == drawingFunction::drawPoints)
					{
						m_pathConstruction2 = new PathItem(this, searchLayer("RAILWAYS"), m_pos1, true, 4,  Settings::getInstance()->getGauge(), Settings::getInstance()->getCrosstie());
						QPointF posInit_(0.005,0.005);
						posInit_+=m_pos1;
						m_pathConstruction2->lineTo(posInit_);
						m_pos3 = m_pos2;
						m_drawingState = 2;
					}
					else
						// If it's a line or a tunnel
					{
						m_pathConstruction->clone();
						disableDrawingAll();
					}
				break;
				// On third clic, create points
				case 2:
					new PointsItem(this, searchLayer("RAILWAYS"), m_pos1, m_pos3, m_pos2, Settings::getInstance()->getGauge(), Settings::getInstance()->getCrosstie());
					disableDrawingAll();
				break;
			}
		}
		// Add arc or platformArc or tunnelArc on clic
		else if(m_activeFunction == drawingFunction::drawArc or m_activeFunction == drawingFunction::drawPlatformArc or m_activeFunction == drawingFunction::drawTunnelArc)
		{
			switch(m_drawingState)
			{
				// On first clic, create a moving line
				case 0:
				{
					if(m_activeFunction == drawingFunction::drawArc)
						initLineConstruction(4);
					else if(m_activeFunction == drawingFunction::drawPlatformArc)
						initLineConstruction(41);
					else if(m_activeFunction == drawingFunction::drawTunnelArc)
						initLineConstruction(42);
				break;
				}
				// On second clic, remove moving line and add moving circle
				case 1:
				{
					m_pathConstruction->removeFromScene();
					m_pos3 = m_pos2;
					int type = 2;
					if(m_activeFunction == drawingFunction::drawPlatformArc)
						type = 21;
					else if(m_activeFunction == drawingFunction::drawTunnelArc)
						type = 22;
					m_arcConstruction = new ArcItem(this, searchLayer("RAILWAYS"), m_pos1, m_pos3, m_pos2, type, Settings::getInstance()->getGauge(), Settings::getInstance()->getCrosstie(), true);					
					m_drawingState = 2;
				break;
				}
				// On third clic, add arc to scene
				case 2:
				{
					// set name if platform
					if(m_activeFunction == drawingFunction::drawPlatformArc)
						m_arcConstruction->setName(getPlatformName(""));
					
					// add arc to scene
					m_arcConstruction->clone();
					
					disableDrawingAll();
				break;
				}
			}
		}
		// Add image on clic
		else if(m_activeFunction == drawingFunction::drawImage)
		{
			switch(m_drawingState)
			{
				// On first clic, add moving rectangle
				case 0:
					initRectConstruction();
				break;
				// On second clic, remove moving rectangle and add image to scene
				case 1:
					ImageItem* imageItem = new ImageItem(this, searchLayer("IMAGES"), m_pos1, m_pixmap.transformed(QTransform::fromScale(1, -1),Qt::SmoothTransformation), m_filenamePixmap);
					
					qreal factor = fabs(m_pos2.x()-m_pos1.x())/(qreal)m_pixmap.width();
					imageItem->scaleItem(m_pos1, factor);
					
					disableDrawingAll();
				break;
			}
		}
		// change platform name on clic
		if(m_activeFunction == drawingFunction::changePlatformName)
		{
			editPlatformName(e->scenePos());
			disableDrawingAll();
		}
		// Add line on clic to represent translation
		else if(m_activeFunction == drawingFunction::translate)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
					// Create construction line to represent the vector moving
					initLineConstruction(42);

					// Create block representing the objects moving
					m_blockMoving = new BlockItem(this, searchLayer("RAILWAYS"), m_pos1, m_blockSelection, true);
					m_blockMoving->QGraphicsItemGroup::setSelected(true);
				break;
				// On second clic, remove line from scene and execute the transformation on selection
				case 1:
					m_blockMoving->explode();					
					disableDrawingAll();
				break;
			}
		}
		// Rotation and scale
		else if(m_activeFunction == drawingFunction::rotate or m_activeFunction == drawingFunction::scale)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
					initLineConstruction(42);
				break;
				case 1:
						// second clic: save the position
						m_pos3 = m_pos2;
						m_drawingState = 2;
						
						// Create block representing the objects rotating
						m_blockMoving = new BlockItem(this, searchLayer("RAILWAYS"), m_pos1, m_blockSelection, true);
						m_blockMoving->QGraphicsItemGroup::setSelected(true);
						break;
				case 2:
						m_blockMoving->explode();
						disableDrawingAll();
				break;
			}
		}
		// Add line on clic to represent copy
		else if(m_activeFunction == drawingFunction::copy)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
				{
					// Create construction line to represent the vector moving
					initLineConstruction(42);

					// Create block of selected objects
					BlockItem* blockCopy = new BlockItem(this, searchLayer("RAILWAYS"), m_pos1, m_blockSelection, true);
					
					// Copy this block, it will be the one moving
					m_blockMoving = dynamic_cast<BlockItem*>(blockCopy->clone());
					m_blockMoving->QGraphicsItemGroup::setSelected(true);
					
					// Delete the initial block
					blockCopy->explode();
					blockCopy->removeFromScene();
				break;
				}
				// On second clic, add a copy of the moving block in the Scene
				case 1:
				{
					BlockItem* blockCopy = dynamic_cast<BlockItem*>(m_blockMoving->clone());
					blockCopy->explode();
					blockCopy->removeFromScene();
				}
				break;
			}
		}
		// On clic, add a selection rectangle
		else if(m_activeFunction == drawingFunction::drawSelection)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving rectangle
				case 0:
					{
						m_pos1 = e->scenePos();
						clearSelection();
						m_rectSelection = new QGraphicsRectItem(m_pos1.x(), m_pos1.y(), 0, 0);
						addItem(m_rectSelection);
						m_drawingState = 1;
						break;
					}
				// On second clic, execute the selection
				case 1:
					QPainterPath path;
					path.addRect(m_rectSelection->rect());

					// If second point is below first point, select only the items enclosed in the rectangle
					if (e->scenePos().y()<m_pos1.y())
						setSelectionArea(path,Qt::ContainsItemBoundingRect);
					// If second point is above first point, select intersecting rectangle
					else
						setSelectionArea(path,Qt::IntersectsItemBoundingRect);

					disableDrawingAll();
				break;
			}
		}
		else
			QGraphicsScene::mousePressEvent(e);
	}
}

void Scene::selectionChanged()
{
	// If unselect, unactivate font actions
	if(selectedItems().size()==0)
	{
		m_parentWindow->setFontActions(false);
	}
	// If select
	else
	{
		foreach (QGraphicsItem *item, selectedItems())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, activate font actions
				if (textItem_)
				{
					m_parentWindow->setFontActions(true);
					return;
				}
			}
	}
}

Layer* Scene::searchLayer(QString getName_Layer)
{
	foreach(Layer* Layer_, m_layerList)
	{
		if( Layer_->getName() == getName_Layer)
			return Layer_;
	}

	return NULL;
}

QPointF Scene::computePosOrtho(QPointF pos1_, QPointF posMouse_)
{
	if(fabs(posMouse_.y()-pos1_.y())<fabs(posMouse_.x()-pos1_.x()))
		posMouse_.setY(pos1_.y());
	else if(fabs(posMouse_.y()-pos1_.y())>fabs(posMouse_.x()-pos1_.x()))
		posMouse_.setX(pos1_.x());

	return posMouse_;
}

void Scene::setSceneSelection(bool status_)
{
	disableDrawingAll();
	
	if(status_==false)
	{
		m_parentWindow->setSelectionButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setSelectionButton(true);
		m_activeFunction = drawingFunction::drawSelection;
		clearSelection();
	}
}

void Scene::setSceneLine(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setLineButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setLineButton(true);
		m_activeFunction = drawingFunction::drawLine;
	}
}

void Scene::setScenePlatformLine(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setPlatformLineButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setPlatformLineButton(true);
		m_activeFunction = drawingFunction::drawPlatformLine;
	}
}

void Scene::setSceneTunnelLine(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setTunnelLineButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setTunnelLineButton(true);
		m_activeFunction = drawingFunction::drawTunnelLine;
	}
}

void Scene::setScenePoints(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setPointsButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setPointsButton(true);
		m_activeFunction = drawingFunction::drawPoints;
	}
}

void Scene::setSceneArc(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setArcButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setArcButton(true);
		m_activeFunction = drawingFunction::drawArc;
	}
}

void Scene::setScenePlatformArc(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setPlatformArcButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setPlatformArcButton(true);
		m_activeFunction = drawingFunction::drawPlatformArc;
	}
}

void Scene::setSceneTunnelArc(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setTunnelArcButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setTunnelArcButton(true);
		m_activeFunction = drawingFunction::drawTunnelArc;
	}
}

void Scene::setSceneText(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setTextButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setTextButton(true);
		m_activeFunction = drawingFunction::drawText;
	}
}

void Scene::setSceneImage(bool status_)
{
	if(status_==false)
	{
		m_parentWindow->setImageButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setImageButton(true);
		m_activeFunction = drawingFunction::drawImage;
		
		m_filenamePixmap = m_parentWindow->openImageDialog();

		if(m_filenamePixmap=="")
		{
			m_parentWindow->setImageButton(false);
			return;
		}
		else
			m_pixmap = QPixmap(m_filenamePixmap);
	}
}

void Scene::setChangePlatformName(bool status_)
{
	clearSelection();
	disableDrawingAll();
	
	if(status_==false)
	{
		m_parentWindow->setTranslateButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setPlatformNameButton(true);
		m_activeFunction = drawingFunction::changePlatformName;
	}
}

void Scene::setSceneTranslate(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setTranslateButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
	{
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setTranslateButton(true);
			m_activeFunction = drawingFunction::translate;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setTranslateButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
	}
}

void Scene::setSceneRotate(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setRotateButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setRotateButton(true);
			m_activeFunction = drawingFunction::rotate;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setRotateButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
}

void Scene::setSceneScale(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setScaleButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
	{
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setScaleButton(true);
			m_activeFunction = drawingFunction::scale;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setScaleButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
	}
}


void Scene::setSceneCopy(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setCopyButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setCopyButton(true);
			m_activeFunction = drawingFunction::copy;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setCopyButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
}

void Scene::setSceneSelectItem(CarItem* car_)
{
	clearSelection();
	disableDrawingAll();
	
	if(car_ != NULL)
	{
		m_car = car_;
		addItem(m_car);
		
		if(m_car->getType() == -1)
		{
			m_locList.append(m_car);
			m_parentWindow->updateCompo(false);
		}
	}
}

void Scene::disableDrawingAll()
{
	if (m_activeFunction == drawingFunction::drawLine or m_activeFunction == drawingFunction::drawPlatformLine or m_activeFunction == drawingFunction::drawTunnelLine)
	{
		m_parentWindow->setLineButton(false);
		m_parentWindow->setPlatformLineButton(false);
		m_parentWindow->setTunnelLineButton(false);

		if(m_drawingState>0)
			m_pathConstruction->removeFromScene();
	}
	if (m_activeFunction == drawingFunction::drawPoints)
	{
		m_parentWindow->setPointsButton(false);

		if(m_drawingState==1)
			m_pathConstruction->removeFromScene();
		else if(m_drawingState==2)
		{
			m_pathConstruction->removeFromScene();
			m_pathConstruction2->removeFromScene();
		}
	}
	else if (m_activeFunction == drawingFunction::drawArc or m_activeFunction == drawingFunction::drawPlatformArc or m_activeFunction == drawingFunction::drawTunnelArc)
	{
		m_parentWindow->setArcButton(false);
		m_parentWindow->setPlatformArcButton(false);
		m_parentWindow->setTunnelArcButton(false);

		if(m_drawingState==1)
			m_pathConstruction->removeFromScene();
		else if(m_drawingState==2)
			m_arcConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::drawText)
	{
		m_parentWindow->setTextButton(false);
	}
	else if (m_activeFunction == drawingFunction::drawImage)
	{
		m_parentWindow->setImageButton(false);
		
		if(m_drawingState>0)
			m_pathConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::changePlatformName)
	{
		m_parentWindow->setPlatformNameButton(false);
	}	
	else if (m_activeFunction == drawingFunction::translate or m_activeFunction == drawingFunction::rotate or m_activeFunction == drawingFunction::scale or m_activeFunction == drawingFunction::copy)
	{
		m_parentWindow->setTranslateButton(false);
		m_parentWindow->setRotateButton(false);
		m_parentWindow->setScaleButton(false);
		m_parentWindow->setCopyButton(false);
		
		if(m_drawingState>0)
		{
			m_pathConstruction->removeFromScene();
			m_blockMoving->removeFromScene();
		}
	}
	else if(m_activeFunction == drawingFunction::drawSelection)
	{
		m_parentWindow->setSelectionButton(false);

		if(m_drawingState>0)
			delete m_rectSelection;
	}

	// If TextItem in edition, unactivate all
	foreach (QGraphicsItem *item, items())
		{
			TextItem* textItem_;
			textItem_ = dynamic_cast<TextItem*>(item);

			if (textItem_)
				textItem_->outFocus();
		}

	m_activeFunction = drawingFunction::inactive;
	m_snappingDrawOn=false;
	m_snappingNode->hide();
	m_drawingState = 0;
}

void Scene::toggleOrtho()
{
	if (m_constraintOrtho_isActive == true)
	{
		m_constraintOrtho_isActive = false;
	}
	else
	{
		m_constraintOrtho_isActive = true;
	}
}

void Scene::toggleSnapping()
{
	if (m_constraintSnapping_isActive == true)
	{
		m_constraintSnapping_isActive = false;
		m_snappingDrawOn=false;
		m_snappingNode->hide();
	}
	else
	{
		m_constraintSnapping_isActive = true;
	}
}

void Scene::changeLayerSelectedItems(QString newLayerName_)
{
	for(int a=0;a<selectedItems().size();a++)
	{
		ShapeItem *item = dynamic_cast<ShapeItem*>(selectedItems()[a]);
		item->setLayer(searchLayer(newLayerName_));
	}
	update();
}

QPointF Scene::snapping(QPointF e_)
{
	QPointF pos2_ = e_;
		
		// If snapping is activated, calculate the snapping position
		if(m_constraintSnapping_isActive==true)
			pos2_ = m_snappingNode->computePosSnapping(e_);

	return pos2_;
}

QList<Layer*> *Scene::getListLayers()
{
		return &m_layerList;
}

void Scene::deleteItemsLayer(int LayerIndex_)
{
	foreach(QGraphicsItem* item, items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			if(shape->getLayer()->getName() == m_layerList[LayerIndex_]->getName())
				shape->removeFromScene();
		}
	}

	delete m_layerList[LayerIndex_];
	m_layerList.erase(m_layerList.begin()+LayerIndex_);
}

void Scene::hideSnappingNode()
{
	m_snappingDrawOn=false;
	m_snappingNode->hide();
}

bool Scene::isOrthoEngaged()
{
	return m_constraintOrtho_isActive;
}

bool Scene::isSnappingEngaged()
{
	return m_constraintSnapping_isActive;
}

bool Scene::isDrawingActive()
{
	if(m_activeFunction == drawingFunction::inactive)
		return false;

	return true;
}

void Scene::applyFontTexts(QFont font_)
{
	foreach (QGraphicsItem *item, selectedItems())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, apply the font
				if (textItem_)
					textItem_->setFont(font_);
			}
}

void Scene::applyColorTexts(QColor color_)
{
	foreach (QGraphicsItem *item, selectedItems())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, apply the color
				if (textItem_)
					textItem_->setCustomColor(color_);
			}
}

void Scene::initLineConstruction(int type_)
{
	m_pathConstruction = new PathItem(this, searchLayer("RAILWAYS"), m_pos1, true, type_, Settings::getInstance()->getGauge(), Settings::getInstance()->getCrosstie());
						
	// Small first move on the moving line in order to initialize the QPainterPath
	QPointF posInit_(0.005,0.005);
	posInit_+=m_pos1;
	m_pathConstruction->lineTo(posInit_);
	m_drawingState = 1;
}

void Scene::initRectConstruction()
{
	m_pathConstruction = new PathItem(this, searchLayer("IMAGES"), m_pos1, true, 5,  Settings::getInstance()->getGauge(), Settings::getInstance()->getCrosstie());
						
	// Small first move on the moving line in order to initialize the QPainterPath
	QPointF posInit(0.005,0.005);
	posInit+=m_pos1;
	m_pathConstruction->initRectangle(posInit);

	m_drawingState = 1;
}

enum drawingFunction::state Scene::getActiveFunction() const
{
	return m_activeFunction;
}

void Scene::reconstructTopology()
{
	// For each item in the scene
	foreach(QGraphicsItem* item, items())
	{
		// If it's a ShapeItem
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
		{
			if(shape)
			{
				// If it's an arc or a line or points, i.e., a railway
				if(shape->getType() == 2 or shape->getType() == 21 or shape->getType() == 22 or shape->getType() == 4 or shape->getType() == 41 or shape->getType() == 42 or shape->getType() == 6)
				{
					shape->connect();
				}
			}
		}
	}
}

QList<CarItem*> Scene::getLocList() const
{
	return m_locList;
}

void Scene::editPlatformName(QPointF pos_)
{
	QGraphicsItem* item = itemAt(pos_, QTransform());
	
	// If there is an item under the clic
	if (item != NULL)
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		// If this is a ShapeItem
		if(shape)
		{
			// if this is a platform
			if(shape->getType() == 41 or shape->getType() == 21)
			{
				shape->setName(getPlatformName(shape->getName()));
			}
		}
	}
}

QString Scene::getPlatformName(QString prefill_)
{
	// Construct a list with all the platform names
	QList<QString> listNamePlatforms;
	
	// Iterate all items and get the platforms
	foreach (QGraphicsItem *item, items())
	{	
		PathItem* path = dynamic_cast<PathItem*>(item);
		ArcItem* arc = dynamic_cast<ArcItem*>(item);

		if (path)
		{
			if(path->getType() == 41)
				listNamePlatforms.append(path->getName());
		}
		else if(arc)
		{
			if(arc->getType() == 21)
				listNamePlatforms.append(arc->getName());
		}
	}
	
	QString text, prefill;
	bool ok = false;
	
	// If getPlatformName is called with "" as argument, it means it's a new platform. Get a standard name.
	if(prefill_ == "")
		prefill = QString(tr("Platform%1")).arg(listNamePlatforms.size());
	// Else it's a modification, prefill with the name in argument
	else
		prefill = prefill_;
	
	do{
		text = QInputDialog::getText(m_parentWindow, tr("Platform name"),
						 tr("Platform name:"), QLineEdit::Normal,
						 prefill, &ok);
						 
		if(!ok and prefill_ == "")
		{
			// If cancel and there is no prefill (new name) don't accept the cancel
		}
		else if(!ok and prefill_ != "")
		{
			// If cancel and there is a prefill, it means it's a modification. Accept the cancel
			return prefill_;
		}
		// If text is empty. Keep that first, the current platforms exists in the scene with ""
		else if(text == "")
		{
				QMessageBox::information(m_parentWindow, tr("Platform name"), tr("The name cannot be empty. Please enter a valid name."));
				ok = false;
		}
		// If the platform name exists, refuse it
		else if (listNamePlatforms.indexOf(text) != -1)
			{
				QMessageBox::information(m_parentWindow, tr("Platform name"), tr("This name already exists. Please choose another name."));
				ok = false;
			}

		
	} while (!ok);
	
	return text;
}

void Scene::crash(HookItem* hook1_, HookItem* hook2_ = NULL)
{
	// Add all the cars connected to the hooks in a list
	QList<CarItem*> listCars;
	
	HookItem* hookTemp = hook1_;
	do{
		hookTemp = hookTemp->getOtherHook();
		listCars.append(hookTemp->getParent());
		hookTemp = hookTemp->getCoupledHook();
	}while(hookTemp != NULL);
	
	if(hook2_ != NULL)
	{
		hookTemp = hook2_;
		do{
			hookTemp = hookTemp->getOtherHook();
			listCars.append(hookTemp->getParent());
			hookTemp = hookTemp->getCoupledHook();
		}while(hookTemp != NULL);
	}
	
	QString message = tr("An accident happened, involving:") + "\n";
	
	foreach(CarItem* car, listCars)
	{
		switch(car->getType())
		{
			case -1:
			message = message + tr("- a locomotive, ") + car->getName() + "\n";
			
			// Remove it from the driving panel
			m_locList.removeOne(car);
			break;
			
			case 0:
			message = message + tr("- a box car") + "\n";
			break;
			
			case 1:
			message = message + tr("- a gondola car") + "\n";
			break;
			
			case 2:
			message = message + tr("- a flat car") + "\n";
			break;
			
			case 3:
			message = message + tr("- a hopper car") + "\n";
			break;
			
			case 4:
			message = message + tr("- a special type car") + "\n";
			break;
			
			case 5:
			message = message + tr("- a maintenance of way car") + "\n";
			break;
			
			case 6:
			message = message + tr("- a caboose car") + "\n";
			break;
			
			case 7:
			message = message + tr("- a passenger car") + "\n";
			break;
			
			case 8:
			message = message + tr("- a refrigerator car") + "\n";
			break;
			
			case 9:
			message = message + tr("- a stock car") + "\n";
			break;
			
			case 10:
			message = message + tr("- a tank car") + "\n";
			break;
			
			case 11:
			message = message + tr("- a containers car") + "\n";
			break;
		}
			
	}
	
	message = message + "\n" + tr("The damaged material will be removed from the map.");
	
	QMessageBox::critical(m_parentWindow, tr("Crash!"), message);
	
	// Remove all objects
	foreach(CarItem* carItem_, listCars)
		removeItem(carItem_);
	
	// Update compo
	m_parentWindow->updateCompo(false);
}

void Scene::bump(CarItem* loc)
{
	// Update combo driving
	m_parentWindow->stopLoc(loc);
}

// Usefull at the end of reading a file to hook all the cars
void Scene::checkHookCollisions()
{
	foreach (QGraphicsItem *item, items())
	{
		// If item exists
		if(item)
		{
			HookItem* hook = dynamic_cast<HookItem*>(item);
			
			//If this is a hook
			if(hook)
				hook->checkCollision();
		}
	}
}

void Scene::updateCompo()
{
	// When there is a hooking
	m_parentWindow->updateCompo(true);
}

void Scene::setActiveLoc(CarItem* loc_)
{
	m_parentWindow->setActiveLoc(loc_);
}

void Scene::setMessageScenario(QString messageScenario_)
{
	m_messageScenario = messageScenario_;
}

void Scene::setMessageVictory(QString messageVictory_)
{
	m_messageVictory = messageVictory_;
}

QString Scene::getMessageScenario() const
{
	return m_messageScenario;
}

QString Scene::getMessageVictory() const
{
	return m_messageVictory;
}

// Permissions on insert locs and cars

void Scene::setLocsAllowed(bool locsAllowed_)
{
	m_locsAllowed = locsAllowed_;
}

void Scene::setCarsAllowed(bool carsAllowed_)
{
	m_carsAllowed = carsAllowed_;
}

bool Scene::getLocsAllowed() const
{
	return m_locsAllowed;
}

bool Scene::getCarsAllowed() const
{
	return m_carsAllowed;
}

void Scene::updateListPlatformsScenario()
{
	// Construct a list with all the platform containing conditions

	m_listPlatformsScenario.clear();
	
	foreach (QGraphicsItem* item, items())
	{	
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			if (shape->getListConditions().size() > 0)
			{
				m_listPlatformsScenario.append(shape);
				
			}
		}
	}
}

void Scene::setStatusEditor(bool statusEditor_)
{
	m_statusEditor = statusEditor_;
}

Scene::~Scene()
{
	qDeleteAll(m_layerList);
}
