#ifndef SNAPPINGNODE_H_INCLUDED
#define SNAPPINGNODE_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"

namespace drawingNode
{
	enum state{
			inactive,
			vertices, 
			nearest
			};
};

class Scene;

class SnappingNode : public QGraphicsItem
{
	public:	
		SnappingNode(Scene*);
		
		QPointF computePosSnapping(QPointF);
		ShapeItem* getSnappedItem() const;
		
		void snapToVertices(bool);
		void snapToNearest(bool);

	protected:
		QRectF boundingRect() const;
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	private:
		Scene* m_scene;
		bool m_snapToVertices;
		bool m_snapToNearest;
		
		ShapeItem* m_snappedItem;
		
		drawingNode::state m_statusDrawing;
};

#endif
