#ifndef SCENE_H_INCLUDED
#define SCENE_H_INCLUDED

#include <QtWidgets>

#include "core/support/Node.h"
#include "core/shapes/ShapeItem.h"
#include "core/shapes/PathItem.h"
#include "core/shapes/PointsItem.h"
#include "core/shapes/ArcItem.h"
#include "core/shapes/TextItem.h"
#include "core/shapes/ImageItem.h"
#include "core/shapes/BlockItem.h"
#include "core/shapes/CarItem.h"
#include "core/support/Layer.h"
#include "core/support/SnappingNode.h"
#include "core/support/MathCoord.h"

class Window;
class View;

namespace drawingFunction
{
	enum state{	inactive,
			drawSelection,
			drawLine,
			drawPlatformLine,
			drawTunnelLine,
			drawPoints,
			drawArc,
			drawPlatformArc,
			drawTunnelArc,
			drawText,
			drawImage,
			changePlatformName,
			translate,
			rotate,
			scale,
			copy,
			createBlock};
};

class Scene : public QGraphicsScene
{
	Q_OBJECT

	public:
		Scene(Window*);
		~Scene();
		
		QList<Layer *> *getListLayers();
		void changeLayerSelectedItems(QString);
		void deleteItemsLayer(int);
		
		void setActiveLoc(CarItem*);

		enum drawingFunction::state getActiveFunction() const;
		bool isDrawingActive();
		void disableDrawingAll();
		
		void applyFontTexts(QFont);
		void applyColorTexts(QColor);
		
		Layer* searchLayer(QString);
		
		void setStatusEditor(bool);
		bool isOrthoEngaged();
		bool isSnappingEngaged();
		QPointF snapping(QPointF);
		void hideSnappingNode();
		
		void saveFile(QString);
		QString getFilename() const;
		void setFilename(QString);
		
		void reconstructTopology();
		
		QList<CarItem*> getLocList() const;
		
		void crash(HookItem*, HookItem*);
		void bump(CarItem*);
		void updateCompo();
		
		void checkHookCollisions();
		
		// Scenario
		QString getMessageScenario() const;
		QString getMessageVictory() const;
		bool getLocsAllowed() const;
		bool getCarsAllowed() const;
						
		void setMessageScenario(QString);
		void setMessageVictory(QString);
		void setLocsAllowed(bool);
		void setCarsAllowed(bool);
		
		void updateListPlatformsScenario();

	public slots:
		void advance();
		void setSceneSelection(bool);
		void setSceneLine(bool);
		void setScenePlatformLine(bool);
		void setSceneTunnelLine(bool);
		void setScenePoints(bool);
		void setSceneArc(bool);
		void setScenePlatformArc(bool);
		void setSceneTunnelArc(bool);
		void setSceneText(bool);
		void setSceneImage(bool);
		void setChangePlatformName(bool);
		void setSceneTranslate(bool);
		void setSceneRotate(bool);
		void setSceneScale(bool);
		void setSceneCopy(bool);
		void setSceneSelectItem(CarItem*);
	
		void toggleOrtho();
		void toggleSnapping();
		
		void clear();
		
	protected:
		virtual void mouseMoveEvent (QGraphicsSceneMouseEvent *e);
		virtual void mousePressEvent (QGraphicsSceneMouseEvent *e);
		virtual void keyPressEvent (QKeyEvent *e);
		virtual void keyReleaseEvent (QKeyEvent *e);

	private slots:
		void selectionChanged();

	private:
		void initLineConstruction(int);
		void initRectConstruction();
		void editPlatformName(QPointF);
		QString getPlatformName(QString);
		
		bool m_modifyTranslate_isActive;
		bool m_modifyRotate_isActive;

		bool m_constraintOrtho_isActive;
		bool m_constraintSnapping_isActive;
		bool m_snappingDrawOn;
		
		QPointF computePosOrtho(QPointF, QPointF);
		
		bool m_statusEditor;
		int m_drawingState;
		drawingFunction::state m_activeFunction;
		
		QPointF m_pos1, m_pos2, m_pos3, m_posSnapping, m_initPos;
		SnappingNode* m_snappingNode;

		Window *m_parentWindow;

		// Lists
		QList<Layer*> m_layerList;
		QList<CarItem*> m_locList;

		// Construction tools
		ArcItem *m_arcConstruction;
		QGraphicsRectItem *m_rectSelection;
		PathItem *m_pathConstruction;
		PathItem *m_pathConstruction2;
		CarItem *m_car;
		
		// QPen drawing 
		QPen penSelection;

		// Filename for pixmap
		QPixmap m_pixmap;
		QString m_filenamePixmap;
		QString m_filename;

		// Block handling
		QList<QGraphicsItem*> m_blockSelection;
		BlockItem* m_blockMoving;
		
		// Scenario
		QString m_messageScenario;
		QString m_messageVictory;
		bool m_locsAllowed;
		bool m_carsAllowed;
		QList<ShapeItem*> m_listPlatformsScenario;
		bool m_isScenarioValidated;
		
};

#endif
