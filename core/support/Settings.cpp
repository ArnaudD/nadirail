#include "core/support/Settings.h"

Settings* Settings::singleton = NULL;

Settings::Settings()
{
	QSettings settings("NadiRail", "NadiRail");
	
	m_language = settings.value("language").toString();
	m_gauge = settings.value("gauge").toReal();
	m_crosstie = settings.value("crosstie").toReal();
	m_colorTracks = settings.value("colorTracks").value<QColor>();
	m_colorRailway = settings.value("colorRailway").value<QColor>();
	m_colorPlatform = settings.value("colorPlatform").value<QColor>();
	
	if(m_language == "")
	m_language = "english";
	
	if(m_gauge == 0)
	m_gauge = 1.435;
	
	if(m_crosstie == 0)
	m_crosstie = 2.60;
	
	if(m_colorTracks.isValid() == false)
	m_colorTracks = QColor(0, 0, 0, 255);	
	
	if(m_colorRailway.isValid() == false)
	m_colorRailway = QColor(50, 50, 50, 140);	
	
	if(m_colorPlatform.isValid() == false)
	m_colorPlatform = QColor(255, 255, 127, 140);	
}

Settings* Settings::getInstance()
{
	if (singleton == NULL)
	{
		singleton = new Settings;
	}

	return singleton;
}

void Settings::kill()
{
	if (singleton != NULL)
    {
		delete singleton;
        singleton = NULL;
    }
}

QString Settings::getLanguage() const
{
	return m_language;
}

qreal Settings::getGauge() const
{
	return m_gauge;
}

qreal Settings::getCrosstie() const
{
	return m_crosstie;
}

QColor Settings::getColorTracks() const
{
	return m_colorTracks;
}

QColor Settings::getColorRailway() const
{
	return m_colorRailway;
}

QColor Settings::getColorPlatform() const
{
	return m_colorPlatform;
}

void Settings::setLanguage(QString language_)
{
	m_language = language_;
}

void Settings::setGauge(qreal gauge_)
{
	m_gauge = gauge_;
}

void Settings::setCrosstie(qreal crosstie_)
{
	m_crosstie = crosstie_;
}

void Settings::setColorTracks(QColor colorTracks_)
{
	m_colorTracks = colorTracks_;
}

void Settings::setColorRailway(QColor colorRailway_)
{
	m_colorRailway = colorRailway_;
}

void Settings::setColorPlatform(QColor colorPlatform_)
{
	m_colorPlatform = colorPlatform_;
}

void Settings::save()
{
	QSettings settings("NadiRail", "NadiRail");
	
	settings.setValue("language", m_language);
	settings.setValue("gauge", m_gauge);
	settings.setValue("crosstie", m_crosstie);
	settings.setValue("colorTracks", m_colorTracks);
	settings.setValue("colorRailway", m_colorRailway);
	settings.setValue("colorPlatform", m_colorPlatform);
}
