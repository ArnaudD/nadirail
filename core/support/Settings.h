#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

#include <QtGui>

class Settings
{

	public:	
		static Settings* getInstance();
		static void kill();
		
		QString getLanguage() const;
		qreal getGauge() const;
		qreal getCrosstie() const;
		QColor getColorTracks() const;
		QColor getColorRailway() const;
		QColor getColorPlatform() const;
		
		void setLanguage(QString);
		void setGauge(qreal);
		void setCrosstie(qreal);
		void setColorTracks(QColor);
		void setColorRailway(QColor);
		void setColorPlatform(QColor);
		
		void save();

	private:
		Settings();
		static Settings* singleton;
		
		QString m_language;
		qreal m_gauge;
		qreal m_crosstie;
		QColor m_colorTracks;
		QColor m_colorRailway;
		QColor m_colorPlatform;
};

#endif
