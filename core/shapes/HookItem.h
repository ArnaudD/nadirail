#ifndef HOOKITEM_H_INCLUDED
#define HOOKITEM_H_INCLUDED

#include <QtWidgets>

class Scene;
class CarItem;

class HookItem : public QGraphicsRectItem
{

	public:	
		HookItem(Scene*, CarItem*, qreal, qreal, qreal, qreal);
		bool getIsHooked() const;
		CarItem* getParent() const;
		HookItem* getCoupledHook() const;
		HookItem* getOtherHook() const;
		QPointF getPos() const;
		
		void hook(HookItem*);
		void deHook();
		void hookCoupled(bool, HookItem*);
		
		void setOtherHook(HookItem*);		
		void checkCollision();

	protected:
		void mousePressEvent (QGraphicsSceneMouseEvent*);

	private:	
		CarItem *m_carItem;
		Scene *m_scene;
		bool isHooked;
		HookItem* m_otherHook;
		HookItem* m_coupledHook;
		bool m_forceOpen;
		QPointF m_initPos;
};

#endif
