#ifndef POINTSITEM_H_INCLUDED
#define POINTSITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"
#include "core/support/Settings.h"

class Scene;

class PointsItem : public ShapeItem
{
	public:
		PointsItem(Scene*, Layer*, QPointF, QPointF, QPointF, qreal, qreal);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual bool isOnItem(QPointF);
		
		void lineTo(QPointF);
		void updateLastPos(QPointF);
		void updatePos(QPointF, int);
		QPainterPath getPath();
		void reconstructPathfromNodes();
		void reconstructNodesfromPath();
		void setPath(QPainterPath);
		
		bool getDirection() const;
		bool getRogueDirection() const;
		void setRogueDirection(bool);
		
		qreal getGauge() const;
		qreal getCrosstie() const;

	protected:
		QRectF boundingRect() const;
		QPainterPath shape() const;
		
		virtual void mousePressEvent (QGraphicsSceneMouseEvent *e);
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);

	private:
		void reconstructPathDraw();
	
		QPainterPath m_path;
		QPainterPath m_pathShape;
		
		QPainterPath m_pathWay1_1;
		QPainterPath m_pathWay1_2;
		QPainterPath m_pathWay2_1;
		QPainterPath m_pathWay2_2;
			
		bool m_direction;
		bool m_rogueDirection;
		qreal m_gauge;
		qreal m_crosstie;
};


#endif
