#ifndef CARITEM_H_INCLUDED
#define CARITEM_H_INCLUDED

#include <QtWidgets>
#include "core/support/Layer.h"
#include "core/shapes/ShapeItem.h"
#include "core/shapes/HookItem.h"

class Scene;

class CarItem : public QGraphicsItem
{
	public:
		CarItem(Scene*, QString, int, qreal, qreal, int, bool, QString, QColor);
	
		virtual void advance(int);
		
		QString getName() const;
		int getType() const;
		QPixmap getPixmap() const;
		int getMaxSpeed() const;
		int getTargetSpeed() const;
		qreal getLength() const;
		qreal getWidth() const;
		qreal getSpeed() const;
		QString getFilename() const;
		ShapeItem* getShapeItem(int) const;
		QColor getColor() const;
		bool getDirection() const;
		QGraphicsRectItem* getCarShape() const;
		QPointF getPos(int) const;
		bool getIsLoaded() const;
		HookItem* getOnlyHook() const;
		bool getSpeedLimiter() const;
		
		void setTargetSpeed(int);
		void setPosInit(ShapeItem*);
		bool insert(QPointF, QPointF, int, int);
		void setSpeed(qreal, HookItem*);
		void bump(HookItem*);
		void setBreaking(bool);
		void setLoaded(bool);
		void reverse(HookItem*);
		void strongReverse();
		void setSpeedLimiter(bool);
		

	protected:
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		void mousePressEvent (QGraphicsSceneMouseEvent*);
	
	private:
		QPointF projectPoint(ShapeItem*, QPointF, qreal, int);
		QPointF attributeItem(ShapeItem*, QPointF, qreal, int);
		void adjustPoints(int index_, QPointF);
		
		Scene* m_scene;
		Layer* m_layer;
		QString m_name;
		
		bool m_isLoaded;
		qreal m_speed;
		int m_targetSpeed;
		int m_maxSpeed;
		qreal m_length;
		qreal m_width;
		bool m_breaking;
		int m_type;
		QPixmap m_pixmap;
		QString m_filename;
		QPointF m_secondPoint;
		
		QList<ShapeItem*> m_shapeItem;
		QList<QPointF> m_points;
		QList<HookItem*> m_hookItem;

		bool m_direction;
		QColor m_color;
		QGraphicsRectItem* m_carShape;
		bool m_speedLimiter;
};

#endif
