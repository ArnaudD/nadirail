 #include "core/shapes/ShapeItem.h"
#include "core/support/Scene.h"

ShapeItem::ShapeItem(Scene* scene_, Layer* layer_, QPointF p1_, bool construction_) : QGraphicsItem()
{
	setAcceptHoverEvents(true);
	setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);

	m_scene = scene_;
	m_layer = layer_;
	m_isConstructing = construction_;
	m_mouseIsOver = false;
	m_address = 0;
	
	setPos(p1_);
}

ShapeItem::ShapeItem()
{
	setAcceptHoverEvents(true);
	setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);
}

Layer* ShapeItem::getLayer() const
{
	return m_layer;
}

Scene* ShapeItem::getScene() const
{
	return m_scene;
}

int ShapeItem::getType() const
{
	return m_type;
}

int ShapeItem::getAddress() const
{
	return m_address;
}

void ShapeItem::setAddress(int address_)
{
	m_address = address_;
}

QList<Node*> ShapeItem::getListNodes() const
{
	return m_listNodes;
}

void ShapeItem::addNode(int role_)
{
	Node *node = new Node(m_scene, this, role_, &m_listNodes);
	m_listNodes.append(node);
	m_scene->addItem(node);
}

void ShapeItem::setLayer(Layer *layer_)
{
	m_layer = layer_;
}

void ShapeItem::setName(QString name_)
{
	m_name = name_;
	
	if(m_type == 21 or m_type == 41)
	{
		setToolTip(m_name);
	}
}

QString ShapeItem::getName() const
{
	return m_name;
}

void ShapeItem::setListConditions(QList<Condition> listConditions_)
{
	m_listConditions = listConditions_;
}

QList<Condition> ShapeItem::getListConditions() const
{
	return m_listConditions;
}

QPointF ShapeItem::getClosestNodePos(QPointF pos_)
{
	qreal snappingDistance = 20/m_scene->views().at(0)->matrix().m11();
	
	QList<qreal> listDistances;
	
	foreach (Node *Node, m_listNodes)
	{
		// Compute all the distances in a list
		listDistances.append(MathCoord::distance(pos_, Node->pos()));
	}
	
	// Determine who is the smallest and get the index
	QList<qreal>::iterator it = std::min_element(listDistances.begin(), listDistances.end());
	int index = std::distance(listDistances.begin(), it);
	
	if(MathCoord::distance(pos_, m_listNodes.at(index)->pos()) < snappingDistance)
		return m_listNodes.at(index)->pos();

	return pos_;
}

QPointF ShapeItem::getNearestPos(QPointF, qreal)
{
	return QPointF(0,0);
}
		
QVariant ShapeItem::itemChange(GraphicsItemChange change_, const QVariant &data_ )
{
	if( change_ == QGraphicsItem::ItemSelectedChange )
	{
		if(data_ == true)
		{
			foreach (Node *node, m_listNodes)
				node->show();
		}
		else
		{
			foreach (Node *node, m_listNodes)
				node->hide();
		}
	}

	return QGraphicsItem::itemChange(change_, data_);
}

void ShapeItem::setIsConstructing(bool value_)
{
	m_isConstructing = value_;
}

bool ShapeItem::getIsConstructing() const
{
	return m_isConstructing;
}

QPen ShapeItem::getPen()
{
	QColor color = Settings::getInstance()->getColorTracks();
	setZValue(m_layer->getLevel());

	if(m_isConstructing == true)
        return QPen(color,0,Qt::SolidLine);
	
	if (isSelected() == false)
	{
		if (m_mouseIsOver == false)
            return QPen(color,0,Qt::SolidLine);
        else if(m_mouseIsOver==true and flags()&QGraphicsItem::ItemIsFocusable)
            return QPen(color,0,Qt::DotLine);
		else
            return QPen(color,0,Qt::SolidLine);
	}
    else if (isSelected() == true)
            return QPen(color,0,Qt::DashLine);
			
	return QPen();
}

void ShapeItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event_)
{
	m_mouseIsOver = true;
	update();

	QGraphicsItem::hoverEnterEvent(event_);
}

void ShapeItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event_)
{
	m_mouseIsOver = false;
	update();

	QGraphicsItem::hoverLeaveEvent(event_);
}

void ShapeItem::setMouseIsOver(bool mouse_)
{
	m_mouseIsOver = mouse_;
	
	// Update the childs if this ShapeItem is a BlockItem too
	foreach(QGraphicsItem* item, childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->setMouseIsOver(mouse_);
	}
	
	update();
}

void ShapeItem::removeFromScene()
{
	// Necessary to delete the nodes out of the destructors, otherwhise when closing the scene the nodes might be already destroyed automatically
}

void ShapeItem::translateItem(QPointF, bool)
{
	// move nodes (moving item is handled by QGraphicsItemGroup)
}

void ShapeItem::rotateItem(QPointF, qreal)
{
	// rotate item and nodes
}

void ShapeItem::scaleItem(QPointF, qreal)
{
	// Scale item and nodes
}

QGraphicsItem* ShapeItem::clone()
{
	// return a pointer toward a copy of the instance
	return 0;
}

bool ShapeItem::isOnItem(QPointF)
{
	// return a bool if the point is on the item
	return false;
}

void ShapeItem::connect()
{
	m_listConnected.clear();
	m_listPointsConnected.clear();
	
	// For each item in the scene
	foreach(QGraphicsItem* item, m_scene->items())
	{
		// If it's a ShapeItem
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			// If it's an arc or a line or points, i.e., a railway and not the actual one
			if(shape != this and (shape->getType() == 2 or shape->getType() == 21 or shape->getType() == 22 or shape->getType() == 4 or shape->getType() == 41 or shape->getType() == 42 or shape->getType() == 6))
			{
				// Test if there is nodes in common
				foreach(Node* myNode, m_listNodes)
				{
					foreach(Node* otherNode, shape->getListNodes())
					{
						if(myNode->pos() == otherNode->pos())
						{
							m_listConnected.append(shape);
							m_listPointsConnected.append(myNode->pos());
						}
					}
				}
			}
		}
	}
}

QList<ShapeItem*> ShapeItem::getListConnected() const
{
	return m_listConnected;
}

QList<QPointF> ShapeItem::getListConnectionPoints() const
{
	return m_listPointsConnected;
}

QPointF ShapeItem::getConnectionPoint(int index_) const
{
	return m_listPointsConnected.at(index_);
}
