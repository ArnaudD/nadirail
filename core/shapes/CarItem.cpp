#include "core/shapes/CarItem.h"
#include "core/support/Scene.h"

CarItem::CarItem(Scene *scene_, QString name_, int maxSpeed_, qreal length_, qreal width_, int type_, bool isLoaded_, QString filename_, QColor color_) : QGraphicsItem()
{
	setZValue(50);
	
	m_scene = scene_;
	m_layer = scene_->searchLayer("TRAINS");
	m_name = name_;
	m_maxSpeed = maxSpeed_;
	m_speed = 0;
	m_targetSpeed = 0;
	m_breaking = false;
	m_length = length_;
	m_width = width_;
	m_type = type_;
	m_isLoaded = isLoaded_;
	m_filename = filename_;
	m_speedLimiter = false;
	
	if(m_filename != "")
		m_pixmap = QPixmap(filename_);

	m_direction = true;
	m_color = color_;
	
	// Correspond to the car without the hooks, to test for collisions
	m_carShape = new QGraphicsRectItem(0, m_width, -m_length, -m_width*2, this);
	m_carShape->setVisible(false);
	
	// Define the hooks
	m_hookItem.append(new HookItem(m_scene, this, 0.25, -0.25, 0.1, 0.5));
	m_hookItem.append(new HookItem(m_scene, this, -m_length-0.35, -0.25, 0.1, 0.5));
	
	m_hookItem.at(0)->setOtherHook(m_hookItem.at(1));
	m_hookItem.at(1)->setOtherHook(m_hookItem.at(0));
	
	// Define shapeItem
	m_shapeItem.append(NULL);
	m_shapeItem.append(NULL);
	
	// Define position
	m_points.append(QPointF(0,0));
	m_points.append(QPointF(0,0));
	
	// settooltip
	setToolTip(m_name);
}

QRectF CarItem::boundingRect() const
{
	return QRectF(0, m_width, -m_length, -m_width*2);
}

QGraphicsRectItem* CarItem::getCarShape() const
{
	return m_carShape;
}

void CarItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	if (m_layer->isVisible() == true)
	{
		painter_->setPen(QPen(QPen(Qt::black, 0, Qt::SolidLine)));
		
		// Transparancy for tunnels
		if(m_shapeItem.at(0)->getType()==22 or m_shapeItem.at(0)->getType()==42 or m_shapeItem.at(1)->getType()==22 or m_shapeItem.at(1)->getType()==42)
		{
			painter_->setBrush(QColor(Qt::transparent));
		}
		else
			painter_->setBrush(m_color);

		painter_->drawRect(QRectF(0, m_width, -m_length, -m_width*2));
	
		// Draw the direction indicator if it's a locomotive
		if(m_type == -1)
		{
			if(m_direction == true)
			{
				painter_->drawLine(QLineF(0, 0, -1, m_width));
				painter_->drawLine(QLineF(0, 0, -1, -m_width));
			}
			else
			{
				painter_->drawLine(QLineF(-m_length, 0, -m_length+1, m_width));
				painter_->drawLine(QLineF(-m_length, 0, -m_length+1, -m_width));
			}
		}
		
		// Draw the loading indicator
		if(m_isLoaded == true)
		{
			painter_->drawLine(QLineF(-1, -m_width, -1, m_width));
			painter_->drawLine(QLineF(-m_length+1, -m_width, -m_length+1, m_width));
		}
	}
}

int CarItem::getType() const
{
	return m_type;
}

QString CarItem::getName() const
{
	return m_name;
}

QPixmap CarItem::getPixmap() const
{
	return m_pixmap;
}

int CarItem::getMaxSpeed() const
{
	return m_maxSpeed;
}

int CarItem::getTargetSpeed() const
{
	return m_targetSpeed;
}

qreal CarItem::getSpeed() const
{
	return m_speed;
}

qreal CarItem::getLength() const
{
	return m_length;
}

qreal CarItem::getWidth() const
{
	return m_width;
}

ShapeItem* CarItem::getShapeItem(int index_) const
{
	return m_shapeItem.at(index_);
}

QPointF CarItem::getPos(int index_) const
{
	return m_points.at(index_);
}

bool CarItem::getIsLoaded() const
{
	return m_isLoaded;
}

void CarItem::setBreaking(bool breaking_)
{
	m_breaking = breaking_;
}

void CarItem::setSpeedLimiter(bool speedLimiter_)
{
	m_speedLimiter = speedLimiter_;
}

bool CarItem::getSpeedLimiter() const
{
	return m_speedLimiter;
}

HookItem* CarItem::getOnlyHook() const
{
	// In case of a locomotive, only 1 hook should be hooked with a car. Return the free hook.
	if(m_hookItem.at(0)->getIsHooked() == true)
		return m_hookItem.at(1);
	
	if(m_hookItem.at(1)->getIsHooked() == true)
		return m_hookItem.at(0);
	
	// Return one anyway
	return m_hookItem.at(0);
}

void CarItem::setSpeed(qreal speed_, HookItem* hook_)
{
	// Used to pass speed to coupled cars
	m_speed = speed_;
	
	// Check from which hitch comes the traction and pass the speed to the other hitch
	if(hook_ == m_hookItem.at(0))
	{
		if(m_hookItem.at(1)->getIsHooked() == true)
			m_hookItem.at(1)->getCoupledHook()->getParent()->setSpeed(m_speed, m_hookItem.at(1)->getCoupledHook());
	}
	else if(hook_ == m_hookItem.at(1))
	{
		if(m_hookItem.at(0)->getIsHooked() == true)
			m_hookItem.at(0)->getCoupledHook()->getParent()->setSpeed(m_speed, m_hookItem.at(0)->getCoupledHook());
	}
}

void CarItem::bump(HookItem* hook_)
{
	// Used to pass speed to coupled cars
	m_speed = 0;
	m_targetSpeed = 0;
	
	// If loc, inform scene that we have to switch off the power
	if(m_type == -1)
	{
		m_scene->bump(this);
	}
	
	// Check from which hitch comes the bump and pass the speed to the other hitch
	if(hook_ == m_hookItem.at(0))
	{				
		if(m_hookItem.at(1)->getIsHooked() == true)
			m_hookItem.at(1)->getCoupledHook()->getParent()->bump(m_hookItem.at(1)->getCoupledHook());
	}
	else if(hook_ == m_hookItem.at(1))
	{
		if(m_hookItem.at(0)->getIsHooked() == true)
			m_hookItem.at(0)->getCoupledHook()->getParent()->bump(m_hookItem.at(0)->getCoupledHook());
	}
}

void CarItem::setLoaded(bool isLoaded_)
{
	m_isLoaded = isLoaded_;
}

void CarItem::setTargetSpeed(int targetSpeed_)
{
	m_targetSpeed = targetSpeed_;
}

QString CarItem::getFilename() const
{
	return m_filename;
}

QColor CarItem::getColor() const
{
	return m_color;
}

void CarItem::strongReverse()
{
	// When reversing using the panel, we have to transfer the reverse signal to both hooks
	reverse(m_hookItem.at(0));
	reverse(m_hookItem.at(1));

	// At this stage reverse has been called 2 times so it cancelled the m_direction. Enforce it.
	m_direction = !m_direction;
	
	prepareGeometryChange();
}

void CarItem::reverse(HookItem* hook_)
{
	m_direction = !m_direction;
	update();
	
	// Check from which hitch comes the reverse and pass the reverse to the other hitch
	if(hook_ == m_hookItem.at(0))
	{
		if(m_hookItem.at(1)->getIsHooked() == true)
			m_hookItem.at(1)->getCoupledHook()->getParent()->reverse(m_hookItem.at(1)->getCoupledHook());
	}
	else if(hook_ == m_hookItem.at(1))
	{
		if(m_hookItem.at(0)->getIsHooked() == true)
			m_hookItem.at(0)->getCoupledHook()->getParent()->reverse(m_hookItem.at(0)->getCoupledHook());
	}
	
	prepareGeometryChange();
}

void CarItem::advance(int)
{
	// Update speed of locomotives
	if(m_type == -1)
	{
		// If targetSpeed reached
		if(qFabs(m_targetSpeed-m_speed)<=0.02)
		{
			m_speed = m_targetSpeed;
		}
		// Acceleration
		if(m_speed < m_targetSpeed)
		{
			if(m_speedLimiter == false or (m_speedLimiter == true and m_speed <4.98))
				m_speed += 0.02;
		}
		// Deceleration
		else if(m_targetSpeed < m_speed and m_speed >= 0.005)
			m_speed -= 0.005;
		
		// Breaking
		if(m_breaking == true and m_speed >= 0.05)
			m_speed -=0.05;
		
		// Apply speed to coupled cars		
		foreach(HookItem* hook_, m_hookItem)
		{
			if(hook_->getIsHooked() == true)
				hook_->getCoupledHook()->getParent()->setSpeed(m_speed, hook_->getCoupledHook());
		}
	}

	// Calculate distance done in 30ms
	qreal distance = m_speed*1000*0.00000833;
	
	if(m_speed > 0)
	{
		// Project points
		m_points[0] = projectPoint(m_shapeItem.at(0), pos(), distance, 0);
		m_points[1] = projectPoint(m_shapeItem.at(1), m_points.at(1), distance, 1);

		setPos(m_points.at(0));
		setRotation(270-MathCoord::azimuth(m_points.at(0), m_points.at(1))*180/M_PI);
		
		// Check hooks collisions
		if(m_hookItem.at(0)->getIsHooked() == false)
			m_hookItem.at(0)->checkCollision();
		else
		{
		// Check the consistency of the hooking
			if(MathCoord::distance(m_hookItem.at(0)->getPos(), m_hookItem.at(0)->getCoupledHook()->getPos()) > 0.5)
				m_scene->crash(m_hookItem.at(0), m_hookItem.at(0)->getCoupledHook());
		}
						
		if(m_hookItem.at(1)->getIsHooked() == false)
			m_hookItem.at(1)->checkCollision();
		else
		{
		// Check the consistency of the hooking
			if(MathCoord::distance(m_hookItem.at(1)->getPos(), m_hookItem.at(1)->getCoupledHook()->getPos()) > 0.5)
				m_scene->crash(m_hookItem.at(1), m_hookItem.at(1)->getCoupledHook());
		}
	}
}

QPointF CarItem::projectPoint(ShapeItem* shapeItem_, QPointF pos_, qreal distance_, int index_)
{
	// Calculate new point	
	switch(shapeItem_->getType())
	{
		// case arc
		case 2:
		case 21:
		case 22:
		{
			// Get arc parameters
			ArcItem* arc = dynamic_cast<ArcItem*>(shapeItem_);
			qreal radius = arc->getRadius();
			QPointF center = arc->getCenter();
	
			// Calculate the center angle and after the azimuth toward the second point
			qreal angle = 2*qAsin(distance_/(2*radius));
			qreal az = MathCoord::azimuth(center, pos_);
			
			// Project the points in both direction	
			QPointF projectedPoint1 = MathCoord::projectPoint(center, az+angle, radius);
			QPointF projectedPoint2 = MathCoord::projectPoint(center, az-angle, radius);
		
			// Define azimuth of the car
			qreal azCar;
			if(m_direction == true)
				azCar = MathCoord::azimuth(m_points.at(1), m_points.at(0));
			else
				azCar = MathCoord::azimuth(m_points.at(0), m_points.at(1));
				
			// Choose the right azimuth for the segment fitting the direction of the car
			qreal az1 = MathCoord::azimuth(pos_, projectedPoint1);
			qreal az2 = MathCoord::azimuth(pos_, projectedPoint2);

			qreal mes1 = M_PI - abs(abs(az1 - azCar) - M_PI); 
			qreal mes2 = M_PI - abs(abs(az2 - azCar) - M_PI);
			
			// Compare which az is closer from azCar
			if(mes2 < mes1 and distance_ > 0)
				projectedPoint1 = projectedPoint2;
			
			// Attribute item if we reach the end of the arc
			if(arc->isOnArc(projectedPoint1) == false)
				projectedPoint1 = attributeItem(shapeItem_, pos_, distance_, index_);
			
			return projectedPoint1;		

		break;
		}
		
		// case line or points
		case 4:
		case 41:
		case 42:
		case 6:
		{
			QList<Node*> listNodes;
			
			// If points, we have to retrieve which line is activated
			if(shapeItem_->getType() == 6)
			{
				PointsItem* pointsItem_ = dynamic_cast<PointsItem*>(shapeItem_);
				if(pointsItem_->getRogueDirection() == true)
				{
					listNodes.push_back(pointsItem_->getListNodes().at(0));
					listNodes.push_back(pointsItem_->getListNodes().at(1));
				}
				else
				{
					listNodes.push_back(pointsItem_->getListNodes().at(0));
					listNodes.push_back(pointsItem_->getListNodes().at(2));
				}
			}
			// If straight line
			else
			{
				listNodes = shapeItem_->getListNodes();
			}
			
			// Normalize azimuth

			// Define azimuth of the car
			qreal azCar;
			if(m_direction == true)
				azCar = MathCoord::azimuth(m_points.at(1), m_points.at(0));
			else
				azCar = MathCoord::azimuth(m_points.at(0), m_points.at(1));
				
			// Choose the right azimuth for the segment fitting the direction of the car
			qreal az1 = MathCoord::azimuth(listNodes.at(0)->pos(), listNodes.at(1)->pos());
			qreal az2 = MathCoord::azimuth(listNodes.at(1)->pos(), listNodes.at(0)->pos());

			qreal mes1 = M_PI - abs(abs(az1 - azCar) - M_PI); 
			qreal mes2 = M_PI - abs(abs(az2 - azCar) - M_PI);
	
			if(mes2 < mes1)
				az1 = az2;

			// Project point
			QPointF posTemp = MathCoord::projectPoint(pos_, az1, distance_);
						
			// Return the pos only if we are on the line, otherwise attribute item
			if(MathCoord::isBetween(posTemp, listNodes.at(0)->pos(), listNodes.at(1)->pos()) == false)
				posTemp = attributeItem(shapeItem_, pos_, distance_, index_);

			return posTemp;

			break;
		}
	}

	// Exception (should not happen as long as shapeItem_ is a LineItem or an ArcItem
	m_speed = 0;
	m_targetSpeed = 0;
	return QPointF(0,0);
}

QPointF CarItem::attributeItem(ShapeItem* shapeItem_, QPointF pos_, qreal distance_, int index_)
{
	// Get the connection point where I am
	QPointF connectionPoint = shapeItem_->getClosestNodePos(m_points.at(index_));
	
	// Check if the connection point is connected with a shapeitem
	for(int i=0; i<shapeItem_->getListConnectionPoints().size(); i++)
	{
		if(connectionPoint == shapeItem_->getListConnectionPoints().at(i))
		{
			// Assign the shapeItem to the car
			m_shapeItem[index_] = shapeItem_->getListConnected().at(i);

			// Adjust points with the correct direction if relevant
			adjustPoints(index_, connectionPoint);
			
			// Project the remaining distance
			qreal remainDistance = distance_ - MathCoord::distance(connectionPoint, pos_);

			return projectPoint(m_shapeItem[index_], connectionPoint, remainDistance, index_);
		}
	}
	
	// If no item to attribute, check the speed to decide if it's a bump or an accident
	if(m_speed > 5)
		m_scene->crash(m_hookItem.at(index_), NULL); // Uncertainty on the index_
	else
		bump(m_hookItem.at(index_));

	return connectionPoint;
}

void CarItem::setPosInit(ShapeItem* shapeItem_)
{
	// This function is called when the user set the initial position of the car. We assume both points are on the same item.

	// Set the position of the loc at the middle of the platform
	QPointF posInit;
	
	if(shapeItem_->getType() == 41) // Straight platform
	{
		posInit = MathCoord::middlePoint(shapeItem_->getListNodes().at(0)->pos(), shapeItem_->getListNodes().at(1)->pos());
	}
	else if(shapeItem_->getType() == 21) // Arc platform
	{
		ArcItem* arc = dynamic_cast<ArcItem*>(shapeItem_);
		
		QPointF middle = MathCoord::middlePoint(shapeItem_->getListNodes().at(1)->pos(), shapeItem_->getListNodes().at(3)->pos());
		qreal az = MathCoord::azimuth(arc->getCenter(), middle);
		posInit = MathCoord::projectPoint(arc->getCenter(), az, arc->getRadius());
		
		// If outside of arc get the opposite direction
		if(arc->isOnArc(posInit) == false)
		{
			az+=M_PI;
			posInit = MathCoord::projectPoint(arc->getCenter(), az, arc->getRadius());
		}
		
	}
					
	m_shapeItem.clear();
	m_shapeItem.append(shapeItem_);
	m_shapeItem.append(shapeItem_);

	QGraphicsItem::setPos(posInit);

	// Calculate the second point and rotate
	m_points[0] = posInit;
	m_points[1] = projectPoint(m_shapeItem.at(0), posInit, m_length, 1);

	setRotation(270-MathCoord::azimuth(posInit, m_points.at(1))*180/M_PI);
}

// Useful when reading cars from file
bool CarItem::insert(QPointF pos_, QPointF secondPoint_, int address0_, int address1_)
{
	QGraphicsItem::setPos(pos_);
	m_points[0] = pos_;
	m_points[1] = secondPoint_;
	setRotation(270-MathCoord::azimuth(pos(), m_points.at(1))*180/M_PI);
	
	// Attribute Items to each points
	
	m_shapeItem.clear();
	m_shapeItem.append(NULL);
	m_shapeItem.append(NULL);
	
	foreach(QGraphicsItem* item, m_scene->items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
		
		// If this is a shape
		if(shape)
		{
			if(shape->getAddress() == address0_)
				m_shapeItem[0] = shape;
			if(shape->getAddress() == address1_)
				m_shapeItem[1] = shape;
		}
	}
	
	if(m_shapeItem.at(0) == NULL or m_shapeItem.at(1) == NULL)
	{
		return false;
	}
	else
	{
		// Streamline points position
		m_shapeItem.at(0)->isOnItem(m_points.at(0));
		m_shapeItem.at(1)->isOnItem(m_points.at(1));
		return true;
	}
	
	return false;
}

void CarItem::adjustPoints(int index_, QPointF entryPoint_)
{
	// Test if this is points, and in that case adjust the rogueDirection
	PointsItem* pointsItem_ = dynamic_cast<PointsItem*>(m_shapeItem.at(index_));
	if (pointsItem_)
	{	
		// If Car enters through P0
		
		if(entryPoint_ == pointsItem_->getListNodes().at(0)->pos())
		{
			// Change the direction if only the current side is on the point and only if none of the coupled cars are on the point. Otherwise it means the train is running on points and a change of direction (in case of a reverse) would result in each side on a different direction
			// It's not a problem and considered as a crash, but if there is not the code below, the direction might change ativated by another car, and result that projectPoints() call attributeItem() for no reason and provoque a bump(), blocking the train.
			// We can consider that the other cars on the points "block it" in the given direction while passing on it as a security.
			
			// Both sides on points
			if(m_shapeItem.at(1-index_) == m_shapeItem.at(index_))
				return;
				
			// Coupled cars on points
			if(m_hookItem.at(0)->getIsHooked() == true)
			{			
				if(m_hookItem.at(0)->getCoupledHook()->getParent()->getShapeItem(0) == m_shapeItem.at(index_) or m_hookItem.at(0)->getCoupledHook()->getParent()->getShapeItem(1) == m_shapeItem.at(index_))
					return;
			}
			
			if(m_hookItem.at(1)->getIsHooked() == true)
			{			
				if(m_hookItem.at(1)->getCoupledHook()->getParent()->getShapeItem(0) == m_shapeItem.at(index_) or m_hookItem.at(1)->getCoupledHook()->getParent()->getShapeItem(1) == m_shapeItem.at(index_))
					return;
			}

			pointsItem_->setRogueDirection(pointsItem_->getDirection());
		}
		// If Car enters through P1
		else if(entryPoint_ == pointsItem_->getListNodes().at(1)->pos())
			pointsItem_->setRogueDirection(true);
		// If Car enters through P2
		else if(entryPoint_ == pointsItem_->getListNodes().at(2)->pos())
			pointsItem_->setRogueDirection(false);
	}
}

bool CarItem::getDirection() const
{
	return m_direction;
}

void CarItem::mousePressEvent(QGraphicsSceneMouseEvent*)
{
	// If a loc, set it active
	if(m_type == -1)
	{
		m_scene->setActiveLoc(this);
	}
	// If we are on a platform, and we are not a locomotive, load/unload
	else if((m_shapeItem.at(0)->getType() == 41 or m_shapeItem.at(1)->getType() == 41) and m_speed==0)
	{
		m_isLoaded = !m_isLoaded;
		prepareGeometryChange();
	}
}
