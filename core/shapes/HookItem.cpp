#include "core/support/Scene.h"
#include "core/shapes/HookItem.h"
#include "core/shapes/CarItem.h"

HookItem::HookItem(Scene *scene_, CarItem *carItem_, qreal x_, qreal y_, qreal width_, qreal height_) : QGraphicsRectItem(x_, y_, width_, height_, carItem_)
{
	setZValue(50);

	m_carItem = carItem_;
	m_scene = scene_;
	
	setPen(QPen(QPen(Qt::black, 0, Qt::SolidLine)));
	
	isHooked = false;
	m_forceOpen = false;
	setBrush(QBrush(QColor(150,150,150)));
	m_coupledHook = NULL;
}

QPointF HookItem::getPos() const
{
	return mapToScene(rect().topLeft());
}

void HookItem::mousePressEvent(QGraphicsSceneMouseEvent* event_)
{
	// De-lock if locked and train is stopped
	if(isHooked == true and m_carItem->getSpeed() == 0)
	{
		// We will force it open for a small move otherwise the collision hook again
		m_forceOpen = true;
		m_initPos = m_carItem->pos();
		
		deHook();
	}

	QGraphicsItem::mousePressEvent(event_);
}

bool HookItem::getIsHooked() const
{
	return isHooked;
}

CarItem* HookItem::getParent() const
{
	return m_carItem;
}

void HookItem::hook(HookItem* hook_)
{
	isHooked = true;
	setBrush(QBrush(QColor(180,230,30)));
	m_coupledHook = hook_;
	m_coupledHook->hookCoupled(true, this);
	
	// Transfer the direction and also to all connected cars
	if(m_coupledHook->getParent()->getDirection() != m_carItem->getDirection())
		m_coupledHook->getParent()->reverse(m_coupledHook);
		
	m_scene->updateCompo();
}

void HookItem::deHook()
{
	// Don't forget to deHook m_hookedCar
	isHooked = false;
	setBrush(QBrush(QColor(150,150,150)));
	
	// We cannot call m_coupledHook->deHook() otherwise there is circular calls
	m_coupledHook->hookCoupled(false, NULL);
	m_coupledHook = NULL;
	
	m_scene->updateCompo();
}

void HookItem::hookCoupled(bool isHooked_, HookItem* hook_)
{
	// The difference with Hook or deHook is that hookCooupled doesn't call the other hook to avoid circular calls
	
	if(isHooked_ == false)
	{
		isHooked = false;
		setBrush(QBrush(QColor(150,150,150)));
		m_coupledHook = NULL;
		m_forceOpen = true;
		m_initPos = m_carItem->pos();
	}
	else
	{
		isHooked = true;
		setBrush(QBrush(QColor(180,230,30)));
		m_coupledHook = hook_;
	}
}

void HookItem::checkCollision()
{
		// if the hook is forced open and we moved more than 1 meter, close the hook, ready to hook again
		if(m_forceOpen == true and MathCoord::distance(m_initPos, m_carItem->pos()) > 1)
			m_forceOpen = false;
		
		// Check collision only if the hook is not forced open
		if(m_forceOpen == false)
		{
			foreach (QGraphicsItem *item_, m_scene->items())
			{
				// If item exists
				if(item_)
				{
					// If this is a hook
					HookItem* hook_ = dynamic_cast<HookItem*>(item_);
					
					// If the hook tested collides and it is not "this"
					if(collidesWithItem(hook_) and hook_ != this)
					{			
						// If the hooks are not actually already hooked
						if(hook_->getCoupledHook() != this)
						{
							// If the respective cars are not themselves colliding (overlapp on platforms)
							if(m_carItem->getCarShape()->collidesWithItem(hook_->getParent()->getCarShape()) == false)
							{
									// If the speed is < 5, we potentially accept hooking
									if(m_carItem->getSpeed() <= 5)
										hook(hook_);
									else
										m_scene->crash(this, hook_);
							}
						}
					}
				}
			}
		}
}

void HookItem::setOtherHook(HookItem* otherHook_)
{
	m_otherHook = otherHook_;
}


HookItem* HookItem::getOtherHook() const
{
	return m_otherHook;
}

HookItem* HookItem::getCoupledHook() const
{
	return m_coupledHook;
}
