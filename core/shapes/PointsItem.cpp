#include "core/shapes/PointsItem.h"
#include "core/support/Scene.h"

PointsItem::PointsItem(Scene *scene_, Layer *layer_, QPointF p1_, QPointF p2_, QPointF p3_, qreal gauge_, qreal crosstie_) : ShapeItem(scene_, layer_, p1_, false)
{
	addNode(5);
	m_listNodes.first()->setPos(p1_);
	addNode(5);
	m_listNodes.last()->setPos(p2_);
	addNode(5);
	m_listNodes.last()->setPos(p3_);
	
	setZValue(2);
	
	m_type = 6;
	m_gauge = gauge_;
	m_crosstie = crosstie_;
	
	// direction chosen by the user
	m_direction = true;
	// direction taken by the train in case it enters not through the main point. This one is set when the points is attributed to the car
	m_rogueDirection = true;
	
	// 4 = rail
	// 41 = platform passengers
	// 5 = rectangle
	// 6 = points
	
	m_path.lineTo(p2_);
	m_path.lineTo(p1_);
	m_path.lineTo(p3_);
	reconstructPathfromNodes();
	
	scene_->addItem(this);
}

QRectF PointsItem::boundingRect() const
{
	return 	m_path.boundingRect();
}

QPainterPath PointsItem::shape() const
{
	return m_pathShape;
}

qreal PointsItem::getGauge() const
{
	return m_gauge;
}

qreal PointsItem::getCrosstie() const
{
	return m_crosstie;
}

void PointsItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	if (m_layer->isVisible() == true)
	{
		// Ballast
		painter_->setPen(QPen(Settings::getInstance()->getColorRailway(), m_crosstie, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
		painter_->drawPath(m_path);

		// Track
		painter_->setPen(getPen());
		//painter_->drawPath(m_path);
		painter_->drawPath(m_pathWay1_1);
		painter_->drawPath(m_pathWay1_2);
		painter_->drawPath(m_pathWay2_1);
		painter_->drawPath(m_pathWay2_2);
		
		// Drawing the activated line ticker
		
		if(m_direction == true)
		{
			QPointF endPoint(m_listNodes.at(1)->pos().x()-m_listNodes.at(0)->pos().x(),m_listNodes.at(1)->pos().y()-m_listNodes.at(0)->pos().y());
			
			painter_->setPen(QPen(m_layer->getColor(),0.5,Qt::SolidLine));
			painter_->drawLine(QPointF(0,0),endPoint);
		}
		else
		{
			QPointF endPoint(m_listNodes.at(2)->pos().x()-m_listNodes.at(0)->pos().x(),m_listNodes.at(2)->pos().y()-m_listNodes.at(0)->pos().y());
			
			painter_->setPen(QPen(m_layer->getColor(),0.5,Qt::SolidLine));
			painter_->drawLine(QPointF(0,0),endPoint);
		}
		
	}
}

void PointsItem::lineTo(QPointF p1_)
{
	// Transfer point in item's coordinate system
	p1_ -= pos();
	m_path.lineTo(p1_);

	reconstructPathDraw();
	prepareGeometryChange();
}

void PointsItem::updateLastPos(QPointF p1_)
{
	// Transfer point in item's coordinate system
	p1_ -= pos();
	m_path.setElementPositionAt(m_path.elementCount()-1, p1_.x(), p1_.y());
	
	reconstructPathDraw();
	prepareGeometryChange();	
}

void PointsItem::updatePos(QPointF p1_, int i)
{
	// Transfer point in item's coordinate system
	p1_ -= pos();
	m_path.setElementPositionAt(i, p1_.x(), p1_.y());
	
	reconstructPathDraw();
	prepareGeometryChange();
}

// Useful when a node is moved by handling
void PointsItem::reconstructPathfromNodes()
	{
		setPos(m_listNodes.first()->pos());

		// Iteration through nodes, ignoring the 1st one
		
		// Start of the path is the Node 1
		m_path.setElementPositionAt(1, m_listNodes.at(1)->pos().x()-m_listNodes.first()->pos().x(), m_listNodes.at(1)->pos().y()-m_listNodes.first()->pos().y());
		
		// Second point of the path is the Node 0
		m_path.setElementPositionAt(2, 0, 0);
		
		// Third point of the path is the Node 2
		m_path.setElementPositionAt(3, m_listNodes.at(2)->pos().x()-m_listNodes.first()->pos().x(), m_listNodes.at(2)->pos().y()-m_listNodes.first()->pos().y());

		reconstructPathDraw();
		prepareGeometryChange();
	}

// Useful when copying the object
void PointsItem::reconstructNodesfromPath()
{	
	QPointF posNode;
	
	// Ignore the first one, it's a moveTo(0,0) and we already added the node above
	for(int i=1; i<m_path.elementCount(); i++)
	{	
		addNode(1);
		posNode.setX(pos().x() + m_path.elementAt(i).x);
		posNode.setY(pos().y() + m_path.elementAt(i).y);
		m_listNodes.last()->setPos(posNode);
	}
	
	// In the list, invert 0 and 1: the second point in the path is the origin
	m_listNodes.swap(0,1);
}


void PointsItem::reconstructPathDraw()
{
	QPainterPath newPathWay1_1;
	QPainterPath newPathWay1_2;
	QPainterPath newPathWay2_1;
	QPainterPath newPathWay2_2;
	
	QPainterPath::Element element;
	
	// Azimuth of the first track
	qreal azimuth = MathCoord::azimuth(QPointF(m_path.elementAt(2).x, m_path.elementAt(2).y), QPointF(m_path.elementAt(1).x, m_path.elementAt(1).y));

	for(int i=0; i<2; i++)
	{
		element = m_path.elementAt(i);
				
		QPointF p1 = MathCoord::projectPoint(QPointF(element.x, element.y), azimuth+M_PI/2, m_gauge/2);
		QPointF p2 = MathCoord::projectPoint(QPointF(element.x, element.y), azimuth-M_PI/2, m_gauge/2);
		
		if(element.isMoveTo() == true)
		{
			newPathWay1_1.moveTo(p1);
			newPathWay1_2.moveTo(p2);
		}
		else if(element.isLineTo() == true)
		{
			newPathWay1_1.lineTo(p1);
			newPathWay1_2.lineTo(p2);
		}
	}
			
	// Azimuth of the second track
	azimuth = MathCoord::azimuth(QPointF(m_path.elementAt(2).x, m_path.elementAt(2).y), QPointF(m_path.elementAt(3).x, m_path.elementAt(3).y));

	for(int i=2; i<4; i++)
	{
		element = m_path.elementAt(i);
				
		QPointF p1 = MathCoord::projectPoint(QPointF(element.x, element.y), azimuth+M_PI/2, m_gauge/2);
		QPointF p2 = MathCoord::projectPoint(QPointF(element.x, element.y), azimuth-M_PI/2, m_gauge/2);
		
		if(element.isMoveTo() == true)
		{
			newPathWay2_1.moveTo(p1);
			newPathWay2_2.moveTo(p2);
		}
		else if(element.isLineTo() == true)
		{
			newPathWay2_1.lineTo(p1);
			newPathWay2_2.lineTo(p2);
		}
	}
	
	m_pathWay1_1 = newPathWay1_1;
	m_pathWay1_2 = newPathWay1_2;
	m_pathWay2_1 = newPathWay2_1;
	m_pathWay2_2 = newPathWay2_2;
	
	// Reconstruct pathShape (for clic). We start at 2 since we just calculated the azimuth above for the second track
	element = m_path.elementAt(3);
		
	// Begin the path for the shape (clic)

	azimuth = MathCoord::azimuth(QPointF(m_path.elementAt(1).x, m_path.elementAt(1).y), QPointF(m_path.elementAt(3).x, m_path.elementAt(3).y));
	QPainterPath newPathShape(MathCoord::projectPoint(QPointF(element.x, element.y), azimuth, 2));
		
	// Continue to the second point	
	element = m_path.elementAt(2);
	
	newPathShape.lineTo(MathCoord::projectPoint(QPointF(element.x, element.y), azimuth, 2));
	newPathShape.lineTo(MathCoord::projectPoint(QPointF(element.x, element.y), azimuth+M_PI, 2));
	
	// Continue to the first point and close
	element = m_path.elementAt(1);
	newPathShape.lineTo(MathCoord::projectPoint(QPointF(element.x, element.y), azimuth+M_PI, 2));
	newPathShape.closeSubpath();
		
	m_pathShape = newPathShape;	
}

QPainterPath PointsItem::getPath()
{
	return m_path;
}

// Useful when copying the object. Assuming there is no other nodes than the first one
void PointsItem::setPath(QPainterPath path_)
{
	m_path = path_;
	reconstructNodesfromPath();
	
	reconstructPathDraw();
	prepareGeometryChange();
}

void PointsItem::translateItem(QPointF movement_, bool)
{
	// Move nodes
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
		(*iNode)->moveBy( movement_.x(), movement_.y() );
		
	reconstructPathDraw();
}

void PointsItem::rotateItem(QPointF origin_, qreal angle_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->rotateNode(origin_, angle_);	
	}
	
	reconstructPathfromNodes();
}

void PointsItem::scaleItem(QPointF origin_, qreal factor_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->scaleNode(origin_, factor_);	
	}
	
	reconstructPathfromNodes();
}

QPointF PointsItem::getNearestPos(QPointF mousePos_, qreal snappingDistance_)
{
	// For each segment
	for(int i=1; i<m_path.elementCount(); i++)
	{
		QPointF A = mapToScene(QPointF(m_path.elementAt(i-1).x, m_path.elementAt(i-1).y));
		QPointF B = mapToScene(QPointF(m_path.elementAt(i).x, m_path.elementAt(i).y));

		// Linear equation ax + by + c = 0 
		qreal a = B.y() - A.y();
		qreal b = A.x() - B.x();
		qreal c = B.x()*A.y() - A.x()*B.y();
		
		// Orthogonal projection
		QPointF H;
		qreal denom = a*a+b*b;
		
		// Particular case: vertical line
		if(b==0)
		{
			H.setX(A.x());
			H.setY(mousePos_.y());
		}
		// Particular case: horizontal line
		else if(a==0)
		{
			H.setX(mousePos_.x());
			H.setY(A.y());
		}
		else
		{
			H.setX( (b*(b*mousePos_.x() - a*mousePos_.y())-a*c)/denom);
			H.setY( (a*(-b*mousePos_.x() + a*mousePos_.y())-b*c)/denom);
		}
		
		// If the projected point is in the segment and distance to mousePos is below the snapping distance
		if(MathCoord::isBetween(H, A, B) and MathCoord::distance(mousePos_, H) < snappingDistance_)
			return H;
	}
	
	return mousePos_;
}

void PointsItem::removeFromScene()
{
	foreach (Node *item, m_listNodes)
	{
		delete item;
	}
	
	delete this;
}

bool PointsItem::getDirection() const
{
	return m_direction;
}

bool PointsItem::getRogueDirection() const
{
	return m_rogueDirection;
}

void PointsItem::setRogueDirection(bool rogueDirection_)
{
	m_rogueDirection = rogueDirection_;
}

void PointsItem::mousePressEvent (QGraphicsSceneMouseEvent*)
{
	// Get the colliding items
	QList<QGraphicsItem*> listItems = collidingItems();
	
	if(listItems.size() != 0)
	{
		foreach(QGraphicsItem* item, listItems)
		{
			CarItem* car = dynamic_cast<CarItem*>(item);
			
			if(car)
			{
				// Check if the car is actually on the points	
				if(car->getShapeItem(0) == this or car->getShapeItem(1) == this)
					return;
			}
		}
	}
	
	// If we aren't in edition mode, change the activated line
	if((flags()&QGraphicsItem::ItemIsSelectable) == false)
	{
		m_direction = !m_direction;
		m_rogueDirection = m_direction;
	}
}


bool PointsItem::isOnItem(QPointF pos_)
{
	if(MathCoord::isBetween(pos_, m_listNodes.at(0)->pos(), m_listNodes.at(1)->pos()) == true)
	{
		m_direction = true;
		m_rogueDirection = true;
		return true;
	}
	else if(MathCoord::isBetween(pos_, m_listNodes.at(0)->pos(), m_listNodes.at(2)->pos()) == true)
	{
		m_direction = false;
		m_rogueDirection = false;
		return true;
	}
	
	return false;			
}

QGraphicsItem* PointsItem::clone()
{
	PointsItem* shapeCloned = new PointsItem(m_scene, m_layer, m_listNodes.at(0)->pos(), m_listNodes.at(1)->pos(), m_listNodes.at(2)->pos(), m_gauge, m_crosstie);
	
	return dynamic_cast<QGraphicsItem*>(shapeCloned);
}
