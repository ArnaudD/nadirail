#ifndef SHAPEITEM_H_INCLUDED
#define SHAPEITEM_H_INCLUDED

#include <QtWidgets>
#include "core/support/Layer.h"
#include "core/support/Node.h"
#include "core/support/Settings.h"
#include "core/support/Definitions.h"

class Scene;
class BlockItem;

class ShapeItem : public QGraphicsItem
{

	public:
		ShapeItem(Scene*, Layer*, QPointF, bool);
		ShapeItem();

		// Handling
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();

		// Misc
		void setLayer(Layer*);
		Layer *getLayer() const;
		Scene *getScene() const;
		int getType() const;
		QString getName() const;
		void setName(QString);
		
		// Conditions
		void setListConditions(QList<Condition>);
		QList<Condition> getListConditions() const;
		
		// Snapping
		QPointF getClosestNodePos(QPointF);
		virtual QPointF getNearestPos(QPointF, qreal);
		void connect();
		QList<ShapeItem*> getListConnected() const;
		QPointF getConnectionPoint(int) const;
		QList<QPointF> getListConnectionPoints() const;
		virtual bool isOnItem(QPointF);
		void setAddress(int);
		int getAddress() const;
		
		// Construction
		void addNode(int);
		QList<Node*> getListNodes() const;
		
		// Drawing
		void setIsConstructing(bool);
		bool getIsConstructing() const;
		void setMouseIsOver(bool);
		QPen getPen();
		
	protected:
		void hoverEnterEvent(QGraphicsSceneHoverEvent*);
		void hoverLeaveEvent(QGraphicsSceneHoverEvent*);
		
		QVariant itemChange(GraphicsItemChange, const QVariant&);
		
		Layer *m_layer;
		Scene *m_scene;
		QList<Node*> m_listNodes;
		bool m_isConstructing;
		bool m_mouseIsOver;
		int m_type;
		int m_address;
		QString m_name;
		QList<Condition> m_listConditions;
		
		QList<ShapeItem*> m_listConnected;
		QList<QPointF> m_listPointsConnected;
};

#endif
