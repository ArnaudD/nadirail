#ifndef ARCITEM_H_INCLUDED
#define ARCITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"
#include "core/support/MathCoord.h"
#include "core/support/Settings.h"

class Scene;

class ArcItem : public ShapeItem
{
	public:
		ArcItem(Scene*, Layer*, QPointF, QPointF, QPointF, int, qreal, qreal, bool);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual bool isOnItem(QPointF);
		
		void setArc(QPointF, QPointF, QPointF);
		qreal getRadius() const;
		QPointF getCenter() const;
		bool isClockwise() const;
		bool isOnArc(QPointF) const;
		
		qreal getGauge() const;
		qreal getCrosstie() const;
		
	protected:
		QRectF boundingRect() const;
		QPainterPath shape() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		
	private:
		qreal m_radius;
		qreal m_spanAngle;
		qreal m_startAngle;
		QRectF m_rect;
		QRectF m_rectDraw1;
		QRectF m_rectDraw2;
		QRectF m_boundingRect;
		bool areAligned;
		qreal m_gauge;
		qreal m_crosstie;
        QPainterPath m_mainPath;
        QPainterPath m_mainPathTrack1;
        QPainterPath m_mainPathTrack2;
		QPainterPath m_pathShape;
};

#endif
