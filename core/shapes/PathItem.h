#ifndef PATHITEM_H_INCLUDED
#define PATHITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"
#include "core/support/Settings.h"

class Scene;

class PathItem : public ShapeItem
{
	public:
		PathItem(Scene*, Layer*, QPointF, bool, int, qreal, qreal);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual bool isOnItem(QPointF);
		
		void lineTo(QPointF);
		void updateLastPos(QPointF);
		void updatePos(QPointF, int);
		QPainterPath getPath();
		void removeLastElement();
		void reconstructPathfromNodes();
		void reconstructNodesfromPath();
		void setPath(QPainterPath);
		
		void initRectangle(QPointF);
		void updateRectangle(QPointF, QPointF);
		
		qreal getGauge() const;
		qreal getCrosstie() const;

	protected:
		QRectF boundingRect() const;
		QPainterPath shape() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		
	private:
		void reconstructPathDraw();
	
		QPainterPath m_path;
		QPainterPath m_pathDraw1;
		QPainterPath m_pathDraw2;
		QPainterPath m_pathShape;
		bool m_isRectangle;
		qreal m_gauge;
		qreal m_crosstie;
};

#endif
