NadiRail
========

[http://nadirail.frama.io/](http://nadirail.frama.io/)  
Version 1.1
License: GNU General Public License v3

## Introduction
NadiRail is a free and open railroad simulation game aiming to simulate operations in a real-world environment, using satellite or aerial imagery as a background.

## Installation
* Download the package from the website
### Windows 7, 8, 10
No installation required. Just launch nadirail.exe
### Linux x86_64 (Debian, Ubuntu)
No installation required. Just launch nadirail.

On a fresh install, you might have to install libxkbcommon-x11-0
```
sudo apt-get install libxkbcommon-x11-0
```

Alternative: the windows version works fine using Wine 3.

## Build from the sources
### Linux (Debian, Ubuntu)
* Install Qt5 (version >= 5.6) from the Qt website and git

* Clone project  
Latest release:
```
git clone -b v1.1 --depth 1 https://framagit.org/ArnaudD/nadirail.git
```
or version in development:
```
git clone --depth 1 https://framagit.org/ArnaudD/nadirail.git
```

* Move into the project's folder

```
cd nadirail
```

* Build

```
qmake
make
```

In case you accidentally rebuild the project file using `qmake-project`, add the following lines `QT += widgets`  `CONFIG += qt`  `TRANSLATIONS = languages/english.ts languages/french.ts` and `RC_ICONS = icons/main.ico` to nadirail.pro

* Run

```
./nadirail
```

* Extra steps for a deployment as a portable package

Preparing the folder to get a portable version for distribution needs a couple of additional steps. First, the project needs to be compiled with the lines about the RPATH in nadirail.pro un-commented. This will ensure that the executable will look for the librairies in its own folder.

Folder structure:
```
NadiRail-vx.x-Ubuntu_x86_64
|-- imageformats
|-- libs
|   |-- libicudata.so.56
|   |-- libicui18n.so.56
|   |-- libicuuc.so.56
|   |-- libQt5Core.so.5
|   |-- libQt5DBus.so.5
|   |-- libQt5Gui.so.5
|   |-- libQt5Widgets.so.5
|   |-- libQt5XcbQpa.so.5
|-- maps
|-- platforms
|   |-- libqxcb.so
|- main.ico
|- nadirail
|- README
```

Rename the files (the version number) so they match the ones hardcoded in the executable. Check using 'ldd nadirail'.

libqxcb.so doesn't have its RPATH updated at compile time. So we need to patch the file with the relative path to the libraries. This is done by the command:
```
patchelf --set-rpath '$ORIGIN/../libs' libqxcb.so
```

On a fresh install, it might be needed to install libxkbcommon-x11-0
```
sudo apt-get install libxkbcommon-x11-0
```

### Windows
* Download and install Qt
* Clone or download the project
* Open nadirail.pro with Qt Creator
* Build the project
* run windeployqt in the build folder

## Credits
NadiRail has been inspired by RailWorld,  developed by Steve Kollmansberger.
Maintained by Rick Rutt here: https://github.com/rrutt/RailWorld 

The NadiRail icon, and the car thumbnails provided with maps are from MLGTraffic.
Find more for your own maps at http://www.mlgtraffic.net/index.html

NadiRail is developed using the following librairies:  
The Qt5 Library <http://www.qt.io/>  
TinyXML-2 <http://www.grinninglizard.com/tinyxml2/> 

Some icons are taken or inspired from the Tangoified icons set for Inkscape and from the Tango Icon Theme:  
<http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons>  
<http://tango-project.org/>

