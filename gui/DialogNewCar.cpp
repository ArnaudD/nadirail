#include <gui/DialogNewCar.h>
#include <core/support/Scene.h>

DialogNewCar::DialogNewCar(Scene* scene_, QWidget *parent_, QString platform_) : QDialog(parent_)
{
	ui.setupUi(this);
	
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	
	m_scene = scene_;
	m_carItem = NULL;
	m_color = QColor(255, 250, 200);
	
	// Car widget color
	QPalette palette = ui.labelShape->palette();
	palette.setColor(QPalette::Window, m_color);
	ui.labelShape->setPalette(palette);
	
	QPalette palette2 = ui.labelShapeLoad->palette();
	palette2.setColor(QPalette::Window, m_color);
	ui.labelShapeLoad->setPalette(palette2);
	ui.labelShapeLoad->hide();
	
	// Fill in platforms
	
	// If DialogNewCar called from the main interface, fill with all the platforms
	if(platform_ == "")
	{
		foreach(QGraphicsItem* item, m_scene->items())
		{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

			if(shape)
			{
				if(shape->getType() == 41 or shape->getType() == 21)
					ui.comboBoxPlatform->addItem(shape->getName());
			}
		}
	}
	// If DialogNewCar called from DialogNewLoco, force the platform name
	else
	{
		ui.comboBoxPlatform->addItem(platform_);
		ui.comboBoxPlatform->setEnabled(false);
	}
	
	connect(ui.comboBoxType, SIGNAL(currentIndexChanged(QString)), this, SLOT(slot_changeType(QString)));
	connect(ui.comboBoxLoad, SIGNAL(currentIndexChanged(int)), this, SLOT(slot_changeLoad(int)));

	connect(ui.pushButtonColor, SIGNAL (clicked()),this, SLOT (slot_color()));
	connect(ui.doubleSpinBoxLength, SIGNAL (valueChanged(double)),this, SLOT (slot_valueChanged(double)));
	connect(ui.pushButtonImage, SIGNAL (clicked()),this, SLOT (slot_selectThumbnail()));
	connect(ui.pushButtonOK, SIGNAL (clicked()),this, SLOT (slot_OK()));
	connect(ui.pushButtonCancel, SIGNAL (clicked()),this, SLOT (slot_cancel()));
}

void DialogNewCar::closeEvent(QCloseEvent*)
{
	slot_cancel();
}

CarItem* DialogNewCar::getCarItem() const
{
	return m_carItem;
}

CarItem* DialogNewCar::newCar(Scene* scene_, QWidget *parent_, QString platform_)
{
	DialogNewCar* dialogNewCar = new DialogNewCar(scene_, parent_, platform_);
	dialogNewCar->exec();
	dialogNewCar->deleteLater();
	
	return dialogNewCar->getCarItem();
}

void DialogNewCar::slot_changeType(QString type_)
{
	if(type_ == tr("Box car"))
		m_color = QColor(128,0,0); // Maroon
	else if(type_ == tr("Gondola car"))
		m_color = QColor(128,128,0); // Olive
	else if(type_ == tr("Flat car"))
		m_color = QColor(0,128,128); // Teal
	else if(type_ == tr("Hopper car"))
		m_color = QColor(70, 240, 240); // Cyan
	else if(type_ == tr("Special type car"))
		m_color = QColor(0, 0, 128); // Navy
	else if(type_ == tr("Maintenance of way car"))
		m_color = QColor(250, 190, 190); // Pink
	else if(type_ == tr("Caboose car"))
		m_color = QColor(255, 215, 180); // Apricot
	else if(type_ == tr("Passenger car"))
		m_color = QColor(255, 250, 200); // Beige
	else if(type_ == tr("Refrigerator car"))
		m_color = QColor(170, 255, 195); // Mint
	else if(type_ == tr("Stock car"))
		m_color = QColor(128, 128, 128); // Grey
	else if(type_ == tr("Tank car"))
		m_color = QColor(230, 190, 255); // Lavender
	else if(type_ == tr("Containers car"))
		m_color = QColor(255, 255, 255); // White

	QPalette palette = ui.labelShape->palette();
	palette.setColor(QPalette::Window, m_color);
	ui.labelShape->setPalette(palette);
	
	QPalette palette2 = ui.labelShapeLoad->palette();
	palette2.setColor(QPalette::Window, m_color);
	ui.labelShapeLoad->setPalette(palette2);
}

void DialogNewCar::slot_changeLoad(int load_)
{
	if(load_ == 0)
		ui.labelShapeLoad->hide();
	else
		ui.labelShapeLoad->show();
}

void DialogNewCar::slot_color()
{
	QColor color = QColorDialog::getColor(m_color, this);
	
	if(color.isValid() == true)
	{
		m_color = color;
		QPalette palette = ui.labelShape->palette();
		palette.setColor(QPalette::Window, m_color);
		ui.labelShape->setPalette(palette);
		ui.labelShapeLoad->setPalette(palette);
	}
}

void DialogNewCar::slot_valueChanged(double value_)
{
	ui.labelShape->setFixedWidth(value_*3);
	ui.labelShapeLoad->setFixedWidth(value_*3);
}

void DialogNewCar::slot_selectThumbnail()
{
	m_filename = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.tif *.gif)"));

	if(m_filename!="")
	{
		ui.lineEditImagePath->setText(m_filename);
		m_pixmap = QPixmap(m_filename);
		ui.labelThumbnail->setPixmap(m_pixmap);
		ui.labelThumbnail->setText("");
	}
}

void DialogNewCar::slot_cancel()
{
	close();
}

void DialogNewCar::slot_OK()
{
	bool load = false;
	if (ui.comboBoxLoad->currentIndex() == 1)
		load = true;
		
	m_carItem = new CarItem(m_scene, ui.comboBoxType->currentText(), 0, ui.doubleSpinBoxLength->value(),ui.doubleSpinBoxWidth->value()/2, ui.comboBoxType->currentIndex(), load, m_filename, m_color);
	
	// Position the car on the platform
	foreach(QGraphicsItem* item, m_scene->items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			// Find the path with matching name
			if(shape->getName() == ui.comboBoxPlatform->currentText())
			{	
				// If we are calling from the main interface
				if(ui.comboBoxPlatform->isEnabled() == true)					
					m_carItem->setPosInit(shape);
			}
		}
	}
	
	close();
}
