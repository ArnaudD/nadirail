#include <gui/DialogAbout.h>

DialogAbout::DialogAbout(QWidget *parent_) : QDialog(parent_)
{
	ui.setupUi(this);
	
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
		
	connect(ui.pushButtonOK, SIGNAL (clicked()),this, SLOT (slot_OK()));
}

void DialogAbout::slot_OK()
{
	close();
}
