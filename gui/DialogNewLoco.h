#ifndef DIALOGNEWLOCO_H_INCLUDED
#define DIALOGNEWLOCO_H_INCLUDED

#include <QtWidgets>
#include "core/support/Scene.h"
#include "core/shapes/CarItem.h"
#include "gui/DialogNewCar.h"
#include "ui_DialogNewLoco.h"

class Scene;

class DialogNewLoco : public QDialog
{
	Q_OBJECT

	public:
		DialogNewLoco(Scene*, QWidget*);
		static CarItem* newLoco(Scene*, QWidget*);
		CarItem* getStatus() const;
	
	protected:
		virtual void closeEvent(QCloseEvent*);
	
	private slots:
		void slot_color();
		void slot_valueChanged(double);
		void slot_selectThumbnail();
		void slot_addCar();
		void slot_copyCar();
		void slot_deleteCar();
		void slot_OK();
		void slot_cancel();

	private:
		Scene* m_scene;
		CarItem* m_status;
		QPixmap m_pixmap;
		QString m_filename;
		QColor m_color;
		Ui::DialogNewLoco ui;
		QList<CarItem*> m_listCars;
};

#endif
