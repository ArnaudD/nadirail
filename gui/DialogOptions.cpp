#include <gui/DialogOptions.h>

DialogOptions::DialogOptions(QWidget *parent_) : QDialog(parent_)
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	
	ui.setupUi(this);

	m_langageIsChanged = false;
		
	if(Settings::getInstance()->getLanguage() == "french")
		ui.comboBoxLanguage->setCurrentIndex(1);
		
	connect(ui.comboBoxLanguage, SIGNAL(currentIndexChanged(int)), this, SLOT(slot_changeLanguage(int)));
	connect(ui.pushButtonOK, SIGNAL (clicked()),this, SLOT (slot_OK()));
	connect(ui.pushButtonCancel, SIGNAL (clicked()),this, SLOT (slot_cancel()));
	
	connect(ui.pushButtonTracksColor, SIGNAL (clicked()),this, SLOT (slot_colorTracks()));
	connect(ui.pushButtonRailwayColor, SIGNAL (clicked()),this, SLOT (slot_colorRailway()));
	connect(ui.pushButtonPlatformColor, SIGNAL (clicked()),this, SLOT (slot_colorPlatform()));
	
	ui.doubleSpinBoxGauge->setValue(Settings::getInstance()->getGauge());
	ui.doubleSpinBoxCrosstie->setValue(Settings::getInstance()->getCrosstie());
	
	m_colorTracks = Settings::getInstance()->getColorTracks();
	m_colorRailway = Settings::getInstance()->getColorRailway();
	m_colorPlatform = Settings::getInstance()->getColorPlatform();
		
	QString styleTracks("background: " + m_colorTracks.name() + ";");
	ui.pushButtonTracksColor->setStyleSheet(styleTracks);

	QString styleRailway("background: " + m_colorRailway.name() + ";");
	ui.pushButtonRailwayColor->setStyleSheet(styleRailway);
	
	QString stylePlatform("background: " + m_colorPlatform.name() + ";");
	ui.pushButtonPlatformColor->setStyleSheet(stylePlatform);

}

void DialogOptions::getOptions(QWidget *parent_)
{
	DialogOptions* dialogOptions = new DialogOptions(parent_);
	dialogOptions->exec();
	dialogOptions->deleteLater();
}

void DialogOptions::closeEvent(QCloseEvent*)
{
	slot_cancel();
}

void DialogOptions::slot_changeLanguage(int)
{
	m_langageIsChanged = true;
}

void DialogOptions::slot_cancel()
{
	close();
}

void DialogOptions::slot_OK()
{
	if(m_langageIsChanged == true)
	{	
		QTranslator translator;
		
		// Initialization translator for the message below
		
		if(ui.comboBoxLanguage->currentText() == tr("English"))
		{
			Settings::getInstance()->setLanguage("english");
		}
		else if(ui.comboBoxLanguage->currentText() == tr("French"))
		{
			Settings::getInstance()->setLanguage("french");
			translator.load(":/languages/french.qm");
		}

		qApp->installTranslator(&translator);
		
		if(Settings::getInstance()->getLanguage() == "english")
			QMessageBox::information(this, "Language change", "The application needs to be restarted to apply the language change.");
		else
			QMessageBox::information(this, tr("Language change"), tr("The application needs to be restarted to apply the language change."));
		
	}
	
	Settings::getInstance()->setGauge(ui.doubleSpinBoxGauge->value());
	Settings::getInstance()->setCrosstie(ui.doubleSpinBoxCrosstie->value());

	Settings::getInstance()->setColorTracks(m_colorTracks);
	Settings::getInstance()->setColorRailway(m_colorRailway);
	Settings::getInstance()->setColorPlatform(m_colorPlatform);

	Settings::getInstance()->save();

	close();
}

void DialogOptions::slot_colorTracks()
{
	QColor color_ = QColorDialog::getColor(m_colorTracks, this, "Select tracks color", QColorDialog::ShowAlphaChannel);
	
	if(color_.isValid()==false)
		return;
	else
	{
		m_colorTracks = color_;
		QString style("background: " + m_colorTracks.name() + ";");
		ui.pushButtonTracksColor->setStyleSheet(style);
	}
}

void DialogOptions::slot_colorRailway()
{
	QColor color_ = QColorDialog::getColor(m_colorRailway, this, "Select railway color", QColorDialog::ShowAlphaChannel);
	
	if(color_.isValid()==false)
		return;
	else
	{
		m_colorRailway = color_;
		QString style("background: " + m_colorRailway.name() + ";");
		ui.pushButtonRailwayColor->setStyleSheet(style);
	}
}

void DialogOptions::slot_colorPlatform()
{
	QColor color_ = QColorDialog::getColor(m_colorPlatform, this, "Select platform color", QColorDialog::ShowAlphaChannel);
	
	if(color_.isValid()==false)
		return;
	else
	{
		m_colorPlatform= color_;
		QString style("background: " + m_colorPlatform.name() + ";");
		ui.pushButtonPlatformColor->setStyleSheet(style);
	}
}



