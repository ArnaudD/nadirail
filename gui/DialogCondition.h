#ifndef DIALOGCONDITION_H_INCLUDED
#define DIALOGCONDITION_H_INCLUDED

#include <QtWidgets>
#include "core/support/Definitions.h"
#include "ui_DialogCondition.h"
#include "core/shapes/ShapeItem.h"

class DialogCondition : public QDialog
{
	Q_OBJECT

	public:
		DialogCondition(QWidget*, ShapeItem*);
		static void newCondition(QWidget*, ShapeItem*);
		
	protected:
		virtual void closeEvent(QCloseEvent*);
		
	private slots:
		void slot_addCondition();
		void slot_removeCondition();
		void slot_OK();
		void slot_cancel();
	
	private:
		void fillListWidgetConditions();
		
		QList<Condition> m_listConditions;
		Ui::DialogCondition ui;
		ShapeItem* m_shapeItem;
};

#endif
