#include <gui/DialogCondition.h>

DialogCondition::DialogCondition(QWidget *parent_, ShapeItem* shapeItem_) : QDialog(parent_)
{
	ui.setupUi(this);
	
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
		
	m_listConditions = shapeItem_->getListConditions();
	m_shapeItem = shapeItem_;
	
	ui.labelPlatform->setText(m_shapeItem->getName());
	
	// Fill the listWidget
	fillListWidgetConditions();

	connect(ui.pushButtonAddCondition, SIGNAL (clicked()),this, SLOT (slot_addCondition()));
	connect(ui.pushButtonRemoveCondition, SIGNAL (clicked()),this, SLOT (slot_removeCondition()));
	connect(ui.pushButtonOK, SIGNAL (clicked()),this, SLOT (slot_OK()));
	connect(ui.pushButtonCancel, SIGNAL (clicked()),this, SLOT (slot_cancel()));
}

void DialogCondition::fillListWidgetConditions()
{
	ui.listWidgetConditions->clear();
	
	QIcon icon(":/icons/trainsAddCar.png");
	QString textCondition;
	
	foreach(Condition cond, m_listConditions)
	{
		textCondition = QString::number(cond.nbCars);
		
		switch(cond.type)
		{
			case 0:
				textCondition += "x - Box car";
				break;
			case 1:
				textCondition += "x - Gondola car";
				break;
			case 2:
				textCondition += "x - Flat car";
				break;
			case 3:
				textCondition += "x - Hopper car";
				break;
			case 4:
				textCondition += "x - Special type car";
				break;
			case 5:
				textCondition += "x - Maintenance of way car";
				break;
			case 6:
				textCondition += "x - Caboose car";
				break;
			case 7:
				textCondition += "x - Passenger car";
				break;
			case 8:
				textCondition += "x - Refrigerator car";
				break;
			case 9:
				textCondition += "x - Stock car";
				break;
			case 10:
				textCondition += "x - Tank car";
				break;
			case 11:
				textCondition += "x - Containers car";
				break;			
			}

		if(cond.loaded == true)
			textCondition += " - Loaded";
		else
			textCondition += " - Unloaded";
		
		ui.listWidgetConditions->addItem(new QListWidgetItem(icon, textCondition));
	}
}

void DialogCondition::closeEvent(QCloseEvent*)
{
	slot_cancel();
}

void DialogCondition::newCondition(QWidget *parent_, ShapeItem* shapeItem_)
{
	DialogCondition* dialogCondition = new DialogCondition(parent_, shapeItem_);
	dialogCondition->exec();
	dialogCondition->deleteLater();
}

void DialogCondition::slot_addCondition()
{
	Condition newCond;
	
	newCond.nbCars = ui.spinBoxNbCars->value();
	newCond.type = ui.comboBoxType->currentIndex();
	newCond.loaded = ui.comboBoxLoad->currentIndex();
	
	m_listConditions.append(newCond);
	fillListWidgetConditions();
}

void DialogCondition::slot_removeCondition()
{
	if(ui.listWidgetConditions->currentRow()>=0)
	{
		// Delete from the list
		m_listConditions.removeAt(ui.listWidgetConditions->currentRow());
		
		// Refresh the widget
		fillListWidgetConditions();
	}
}

void DialogCondition::slot_cancel()
{
	close();
}

void DialogCondition::slot_OK()
{
	// Apply changes to item
	m_shapeItem->setListConditions(m_listConditions);
	
	close();
}
