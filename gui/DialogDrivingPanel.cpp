#include <gui/DialogDrivingPanel.h>
#include <core/support/Scene.h>

DialogDrivingPanel::DialogDrivingPanel(Window* window_) : QWidget(window_)
{
	ui.setupUi(this);
	m_window = window_;
	
	connect(ui.pushButtonBreak, SIGNAL(pressed()), this, SLOT(slot_breaking()));
	connect(ui.pushButtonBreak, SIGNAL(released()), this, SLOT(slot_breaking()));
	connect(ui.pushButtonReverse, SIGNAL(released()), this, SLOT(slot_reverse()));
	connect(ui.checkBoxSpeedLimiter, SIGNAL(stateChanged(int)), this, SLOT(slot_speedLimiter(int)));
	connect(ui.verticalSliderThrottle, SIGNAL(valueChanged(int)), this, SLOT(slot_sliderMoved(int)));

	m_activeLoc = NULL;
}

void DialogDrivingPanel::setScene(Scene* scene_)
{
	m_scene = scene_;
}

void DialogDrivingPanel::slot_sliderMoved(int value_)
{
	ui.lineEditThrottle->setText(QString::number(value_));
	
	if(m_activeLoc != NULL)
	{
		m_activeLoc->setTargetSpeed(value_);
	}
	else
	{
		ui.lineEditThrottle->setText("");
		ui.lineEditPower->setText("");
	}
}

void DialogDrivingPanel::updateSpeed()
{
	// Update driving panel at each cycle
	if(m_activeLoc != NULL)
	{
		ui.lineEditPower->setText(QString::number(m_activeLoc->getSpeed(),'f', 3));
		ui.verticalSliderPower->setSliderPosition(m_activeLoc->getSpeed());
		
		if(m_activeLoc->getSpeed() != 0)
			ui.pushButtonReverse->setEnabled(false);
		else
			ui.pushButtonReverse->setEnabled(true);
	} 
}


void DialogDrivingPanel::stopLoc(CarItem* loc_)
{
	if(m_activeLoc == loc_)
	{
		ui.lineEditThrottle->setText("");
		ui.lineEditPower->setText("");
		
		ui.verticalSliderThrottle->setSliderPosition(0);
		ui.verticalSliderPower->setSliderPosition(0);
		
		ui.pushButtonReverse->setEnabled(true);
	} 
}

void DialogDrivingPanel::setActiveLoc(CarItem* loc_)
{
	m_activeLoc = loc_;
		
	if(m_activeLoc != NULL)
	{
		// Update the thumbnail if a pixmap is stored within the CarItem
		if(m_activeLoc->getFilename() != "")
			ui.labelThumbnail->setPixmap(m_activeLoc->getPixmap().scaledToWidth(140));
		else
		{
			ui.labelThumbnail->clear();
			ui.labelThumbnail->setText(tr("no thumbnail"));
		}
		
		// Update max speed of vertical sliders
		ui.verticalSliderThrottle->setEnabled(true);
		ui.verticalSliderThrottle->setMaximum(m_activeLoc->getMaxSpeed());
		ui.verticalSliderPower->setMaximum(m_activeLoc->getMaxSpeed());
		
		// Update lineEdit sliders
		ui.lineEditThrottle->setText(QString::number(m_activeLoc->getTargetSpeed()));
		ui.lineEditPower->setText(QString::number(m_activeLoc->getSpeed()));
		
		ui.verticalSliderThrottle->setSliderPosition(m_activeLoc->getTargetSpeed());
		ui.verticalSliderPower->setSliderPosition(m_activeLoc->getSpeed());
		
		// Update speedLimiter
		ui.checkBoxSpeedLimiter->setChecked(m_activeLoc->getSpeedLimiter());
	}
	else
	{	
		ui.verticalSliderThrottle->setSliderPosition(0);
		ui.verticalSliderPower->setSliderPosition(0);
		ui.verticalSliderThrottle->setEnabled(false);
		ui.checkBoxFollow->setChecked(false);
		
		ui.lineEditThrottle->setText("");
		ui.lineEditPower->setText("");
		ui.labelThumbnail->clear();
		ui.labelThumbnail->setText(tr("no thumbnail"));
	}	
}

// Called by key
void DialogDrivingPanel::setBreaking(bool breaking_)
{
	ui.pushButtonBreak->setDown(breaking_);
	
	slot_breaking();
}

void DialogDrivingPanel::slot_breaking()
{
	if(m_activeLoc != NULL)
	{
		if(ui.pushButtonBreak->isDown())
			m_activeLoc->setBreaking(true);
		else
			m_activeLoc->setBreaking(false);
	}
}

void DialogDrivingPanel::slot_reverse()
{
	if(m_activeLoc != NULL and ui.pushButtonReverse->isEnabled() == true)
	{
		m_activeLoc->strongReverse();
	}
}

QPointF DialogDrivingPanel::getFollow()
{
	if(ui.checkBoxFollow->isChecked() == true)
		return m_activeLoc->pos();
	
	return QPointF(0,0);
}

CarItem* DialogDrivingPanel::getActiveLoc() const
{
	return m_activeLoc;
}

void DialogDrivingPanel::slot_speedLimiter(int checkState_)
{
	if(checkState_ == Qt::Unchecked)
		m_activeLoc->setSpeedLimiter(false);
	else if(checkState_ == Qt::Checked)
		m_activeLoc->setSpeedLimiter(true);
}

// Called by key
void DialogDrivingPanel::increaseSpeed()
{
	if(ui.verticalSliderThrottle->value() < ui.verticalSliderThrottle->maximum())
		ui.verticalSliderThrottle->setSliderPosition(ui.verticalSliderThrottle->value()+1);
}

// Called by key
void DialogDrivingPanel::decreaseSpeed()
{
	if(ui.verticalSliderThrottle->value() > 0)
		ui.verticalSliderThrottle->setSliderPosition(ui.verticalSliderThrottle->value()-1);
}

// Called by key
void DialogDrivingPanel::speedLimiter()
{
	if(ui.checkBoxSpeedLimiter->checkState() == Qt::Unchecked)
	{
		ui.checkBoxSpeedLimiter->setChecked(true);
	}
	else if(ui.checkBoxSpeedLimiter->checkState() == Qt::Checked)
	{
		ui.checkBoxSpeedLimiter->setChecked(false);
	}
}

// Called by key
void DialogDrivingPanel::follow()
{
	if(ui.checkBoxFollow->checkState() == Qt::Unchecked)
	{
		ui.checkBoxFollow->setChecked(true);
	}
	else if(ui.checkBoxFollow->checkState() == Qt::Checked)
	{
		ui.checkBoxFollow->setChecked(false);
	}
}
