#include <gui/DialogCompo.h>
#include <core/support/Scene.h>

DialogCompo::DialogCompo(Window* window_) : QWidget(window_)
{
	ui.setupUi(this);
	m_activeLoc = NULL;
	m_window = window_;
	
	ui.listWidgetCompo->setIconSize(QSize(2000, 43));
	
	connect(ui.listWidgetCompo, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(slot_changeLoc(QListWidgetItem*)));
}

void DialogCompo::slot_changeLoc(QListWidgetItem*)
{
	QList<CarItem*> listLoc = m_scene->getLocList();
	
	if(listLoc.size()>0)
	{
		m_window->centerOn(listLoc.at(ui.listWidgetCompo->currentRow())->pos());
		m_window->setDrivingPanelLoc(listLoc.at(ui.listWidgetCompo->currentRow()));
	}
}

void DialogCompo::updateList(CarItem* activeLoc_)
{
	// Get the list from Scene and clear the combo
	QList<CarItem*> listLoc = m_scene->getLocList();
	ui.listWidgetCompo->clear();
	
	if(listLoc.size() != 0)
	{
		CarItem* loc = NULL;
		
		// Fill the combo
		foreach(loc, listLoc)
		{
			QIcon icon;
			
			int y = 43; // default height
			if(loc->getFilename() != "")
				y=loc->getPixmap().height();

            QPixmap comboPixmap(2500, y);
			comboPixmap.fill(Qt::transparent);
			QPainter painter(&comboPixmap);
		
			if(loc->getFilename() != "")
			{
				int initWidth = 0;
				HookItem* hook = loc->getOnlyHook();
				
				do
				{	
					// Get the car parent of the hook
					CarItem* car = hook->getParent();
					
					if(car->getFilename() != "")
					{
						// Paint it
						painter.drawPixmap(initWidth, y-car->getPixmap().height(), car->getPixmap());
						// Move init position
						initWidth+=car->getPixmap().width();
					}
					
					// If the other hook on the car is coupled, get the hook coupled
					if(hook->getOtherHook()->getIsHooked() == true)
						hook = hook->getOtherHook()->getCoupledHook();
					else
						hook = NULL;
					
				}while(hook != NULL);
				
			}
			icon.addPixmap(comboPixmap, QIcon::Normal);
			icon.addPixmap(comboPixmap, QIcon::Selected);

            QListWidgetItem* listWidgetItem = new QListWidgetItem(icon, loc->getName());
            listWidgetItem->setTextAlignment(Qt::AlignLeft|Qt::AlignVCenter|Qt::AlignAbsolute);

            ui.listWidgetCompo->addItem(listWidgetItem);
		}
		
		// At the end, transmit the active loc to the driving panel. If none is given in argument, pick_up the last one
		if(activeLoc_ == NULL)
		{
			m_window->centerOn(listLoc.last()->pos());
			m_window->setDrivingPanelLoc(listLoc.last());
			// Select the last loc in the QListWidget
			ui.listWidgetCompo->setCurrentRow(listLoc.size()-1);
		}
		// If an active loc is given in argument, means the update is triggered by a hooking. In that case set the active loc but don't change the driving panel
		else
		{
			ui.listWidgetCompo->setCurrentRow(listLoc.indexOf(activeLoc_));
		}
	}
	else
	// If there is no locs within the map
	{
		m_window->setDrivingPanelLoc(NULL);
	}
}

void DialogCompo::selectLoc(CarItem* loc_)
{
	QList<CarItem*> listLoc = m_scene->getLocList();
	ui.listWidgetCompo->setCurrentRow(listLoc.indexOf(loc_));
	
	m_window->setDrivingPanelLoc(loc_);
}

void DialogCompo::setScene(Scene* scene_)
{
	m_scene = scene_;
}

// When press space, activate the next loc in the list
void DialogCompo::nextLoc()
{
	// If there is locs in the list
	if(ui.listWidgetCompo->count()>0)
	{
		// Get the index of the active loc
		int index = ui.listWidgetCompo->currentRow();
		
		// If this is the last index in the list, call the loc at index at 0. Otherwise increment
		if(index == ui.listWidgetCompo->count()-1)
			ui.listWidgetCompo->setCurrentRow(0);
		else
			ui.listWidgetCompo->setCurrentRow(++index);;
			
		slot_changeLoc(ui.listWidgetCompo->currentItem());
	}
}
