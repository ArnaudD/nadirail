#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <QtWidgets>
#include <QTranslator>
#include "gui/View.h"
#include "gui/DialogNewMap.h"
#include "gui/DialogNewLoco.h"
#include "gui/DialogNewCar.h"
#include "gui/DialogAbout.h"
#include "gui/DialogOptions.h"
#include "core/support/Layer.h"
#include "core/support/IO.h"

class Scene;
class DialogDrivingPanel;
class DialogCompo;

class Window : public QMainWindow
{
	Q_OBJECT

	public:
		Window();
		
		void setCoordStatusBar(qreal, qreal);
		
		void setSelectionButton(bool);
		void setLineButton(bool);
		void setPlatformLineButton(bool);
		void setTunnelLineButton(bool);
		void setPointsButton(bool);
		void setArcButton(bool);
		void setPlatformArcButton(bool);
		void setTunnelArcButton(bool);
		void setTextButton(bool);
		void setImageButton(bool);
		void setFontActions(bool);
		void setPlatformNameButton(bool);
		void setTranslateButton(bool);
		void setRotateButton(bool);
		void setScaleButton(bool);
		void setCopyButton(bool);
		
		QString openImageDialog();
		
		void setEditCursor(bool);
		void closedWindow();
		void updateCompo(bool);
		
		void stopLoc(CarItem*);
		void setActiveLoc(CarItem*);
		void setDrivingPanelLoc(CarItem*);
		
		void centerOn(QPointF);

		void keyPress(int);
		void keyRelease(int);
		
	public slots:
		void slot_ortho();
		void slot_snapping();
		
	protected:
		virtual void closeEvent(QCloseEvent*);
        bool eventFilter(QObject *obj, QEvent *event);

	private slots:
		void slot_newScene();
		void slot_fileOpen();
		void slot_fileSaveAs();
		void slot_fileOptions();
		
		void slot_addLocomotive();
		void slot_addCar();
		
		void slot_editMap();
		void slot_editScenario();
		
		void slot_drawLine();
		void slot_drawPlatformLine();
		void slot_drawTunnelLine();
		void slot_drawPoints();
		void slot_drawArc();
		void slot_drawPlatformArc();
		void slot_drawTunnelArc();
		void slot_drawText();
		void slot_drawImage();
		
		void slot_fontType();
		void slot_fontColor();
		
		void slot_selection();
		void slot_platformName();
		void slot_translate();
		void slot_rotate();
		void slot_scale();
		void slot_copy();
		
		void slot_doc();
		
		void slot_advance();
		
	private:
		void prepareScene();
		void closeMap();
		void activateEditor(bool);
		
		QMdiArea* m_centralArea;

		//Declaration menu Files
		QMenu* m_menuFile;
		QToolBar* m_toolbarFile;
		
		QAction* m_actionNew;
		QAction* m_actionOpen;
		QAction* m_actionSaveAs;
		QAction* m_actionOptions;
		QAction* m_actionExit;
		
		// Declaration menu Editor
		QMenu* m_menuTrains;
		QToolBar* m_toolbarTrains;
		QAction* m_actionAddLocomotive;
		QAction* m_actionAddCar;
		
		// Declaration menu Editor
		QMenu* m_menuEditor;
		QAction* m_actionEditMap;
		QAction* m_actionEditScenario;
		
		// Declaration menu Drawing
		QMenu* m_menuDrawing;
		QToolBar* m_toolbarDrawing;
		
		QAction* m_actionSelection;
		QAction* m_actionDrawLine;
		QAction* m_actionDrawPlatformLine;
		QAction* m_actionDrawTunnelLine;
		QAction* m_actionDrawPoints;
		QAction* m_actionDrawArc;
		QAction* m_actionDrawPlatformArc;
		QAction* m_actionDrawTunnelArc;
		QAction* m_actionDrawText;
		QAction* m_actionDrawImage;
		
		// Declaration menu Modify
		QMenu* m_menuModify;
		QToolBar* m_toolbarModify;
		
		QAction* m_actionPlatformName;
		QAction* m_actionTranslate;
		QAction* m_actionRotate;
		QAction* m_actionScale;
		QAction* m_actionCopy;
		QAction* m_actionFontType;
		QAction* m_actionFontColor;
	
		// Declaration menu Constraints
		QMenu* m_menuConstraints;
		QToolBar* m_toolbarConstraints;
		
		QAction* m_actionOrtho;
		QAction* m_actionSnapping;	
		
		// Declaration menu View
		QMenu* m_menuView;
	
		QAction* m_actionViewDrivingPanel;
		QAction* m_actionViewCompo;	
		
		// Declaration menu Help
		QMenu *m_menuHelp;
		
		QAction* m_actionDoc;
		QAction* m_actionAbout;
	
		// Status bar
		QStatusBar* m_statusBar;
		QLabel* m_labelX, *m_labelY;
		
		// Dock driving panel
		QDockWidget* m_dockDriving;
		DialogDrivingPanel* m_drivingPanel;
		
		// Dock compo
		QDockWidget* m_dockCompo;
		DialogCompo* m_dialogCompo;
		
		// DialogAbout
		DialogAbout* m_dialogAbout;
		
		// Scene
		Scene* m_scene;
		View* m_view;
		QList<Layer*> *m_layerList;
		
		// Timer
		QTimer m_timer;
};

#endif

