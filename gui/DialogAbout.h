#ifndef DIALOGABOUT_H_INCLUDED
#define DIALOGABOUT_H_INCLUDED

#include <QtWidgets>
#include "ui_DialogAbout.h"

class DialogAbout : public QDialog
{
	Q_OBJECT

	public:
		DialogAbout(QWidget*);
		
	private slots:
		void slot_OK();
	
	private:
		Ui::DialogAbout ui;
};

#endif
