#include "gui/Window.h"
#include "core/support/Scene.h"
#include "gui/DialogDrivingPanel.h"
#include "gui/DialogCompo.h"
#include "gui/DialogScenario.h"

Window::Window() : QMainWindow()
{
	setWindowTitle("NadiRail");
	setMouseTracking(true);
    qApp->installEventFilter(this);
	
	m_scene = NULL;
	m_layerList = NULL;

	// Initialization central area
	m_scene = new Scene(this);
	m_scene->setSceneRect(-10000,-10000,20000,20000);
	m_view = new View(this);
	m_view->setScene(m_scene);
	setCentralWidget(m_view);

	// Initialization dock driving panel
	m_dockDriving = new QDockWidget(tr("Driving panel"), this);
	m_drivingPanel = new DialogDrivingPanel(this);
	m_dockDriving->setWidget(m_drivingPanel);
	m_dockDriving->setFloating(false);
	addDockWidget (Qt::RightDockWidgetArea, m_dockDriving, Qt::Vertical);
	m_dockDriving->hide();
	
	// Initialization dock compo
	m_dockCompo = new QDockWidget(tr("Compositions"), this);
	m_dialogCompo = new DialogCompo(this);
	m_dockCompo->setWidget(m_dialogCompo);
	m_dockCompo->setFloating(false);
	addDockWidget (Qt::BottomDockWidgetArea, m_dockCompo, Qt::Horizontal);
	m_dockCompo->hide();
	
	// Initialization DialogAbout
	DialogAbout* m_dialogAbout = new DialogAbout(this);
	
	// Initialization menu File
	m_menuFile = menuBar()->addMenu(tr("Game"));
	m_toolbarFile = addToolBar(tr("Game"));

	m_actionNew = m_menuFile->addAction(tr("Create new map from scratch..."));
	m_toolbarFile->addAction(m_actionNew);
	m_actionNew->setIcon(QIcon(":/icons/fileNew.png"));
	connect(m_actionNew, SIGNAL(triggered()), this, SLOT(slot_newScene()));
	
	m_actionOpen = m_menuFile->addAction(tr("Open a map or a saved game..."));
	m_toolbarFile->addAction(m_actionOpen);
	m_actionOpen->setIcon(QIcon(":/icons/fileOpen.png"));
	connect(m_actionOpen, SIGNAL(triggered()), this, SLOT(slot_fileOpen()));
	
	m_menuFile->addSeparator();
	m_toolbarFile->addSeparator();
	
	m_actionSaveAs = m_menuFile->addAction(tr("Save..."));
	m_toolbarFile->addAction(m_actionSaveAs);
	m_actionSaveAs->setIcon(QIcon(":/icons/fileSaveAs.png"));
	m_actionSaveAs->setEnabled(false);
	connect(m_actionSaveAs, SIGNAL(triggered()), this, SLOT(slot_fileSaveAs()));

	m_menuFile->addSeparator();
	m_toolbarFile->addSeparator();
	
	m_actionOptions = m_menuFile->addAction(tr("Options..."));
	m_toolbarFile->addAction(m_actionOptions);
	m_actionOptions->setIcon(QIcon(":/icons/fileOptions.png"));
	connect(m_actionOptions, SIGNAL(triggered()), this, SLOT(slot_fileOptions()));
	
	m_menuFile->addSeparator();
	m_toolbarFile->addSeparator();
	
	m_actionExit = m_menuFile->addAction(tr("Exit"));
	m_toolbarFile->addAction(m_actionExit);
	m_actionExit->setIcon(QIcon(":/icons/fileExit.png"));
	connect(m_actionExit, &QAction::triggered, qApp, &QApplication::closeAllWindows);
	
	// Initialization menu Trains
	m_menuTrains = menuBar()->addMenu(tr("Trains"));
	m_menuTrains->setEnabled(false);
	
	m_toolbarTrains = addToolBar(tr("Train"));
	m_toolbarTrains->setEnabled(false);
	
	m_actionAddLocomotive = m_menuTrains->addAction(tr("Add locomotive..."));
	m_toolbarTrains->addAction(m_actionAddLocomotive);
	m_actionAddLocomotive->setIcon(QIcon(":/icons/trainsAddLocomotive.png"));
	connect(m_actionAddLocomotive, SIGNAL(triggered()), this, SLOT(slot_addLocomotive()));

	m_actionAddCar = m_menuTrains->addAction(tr("Add car..."));
	m_toolbarTrains->addAction(m_actionAddCar);
	m_actionAddCar->setIcon(QIcon(":/icons/trainsAddCar.png"));
	connect(m_actionAddCar, SIGNAL(triggered()), this, SLOT(slot_addCar()));
	
	// Initialization menu Editor
	m_menuEditor = menuBar()->addMenu(tr("Editor"));
	m_menuEditor->setEnabled(false);
	
	m_actionEditMap = m_menuEditor->addAction(tr("Edit map"));
	m_actionEditMap->setIcon(QIcon(":/icons/editorEditMap.png"));
	m_actionEditMap->setCheckable(true);
	connect(m_actionEditMap, SIGNAL(triggered()), this, SLOT(slot_editMap()));
	
	m_menuEditor->addSeparator();
	
	m_actionEditScenario = m_menuEditor->addAction(tr("Edit scenario..."));
	m_actionEditScenario->setIcon(QIcon(":/icons/editorScenario.png"));
	connect(m_actionEditScenario, SIGNAL(triggered()), this, SLOT(slot_editScenario()));

	m_menuEditor->addSeparator();

	// Initialization menu Drawing
	m_menuDrawing = m_menuEditor->addMenu(tr("Drawing"));
	m_toolbarDrawing = addToolBar(tr("Drawing"));
	m_toolbarDrawing->hide();
	m_menuDrawing->setEnabled(false);
	m_toolbarDrawing->setEnabled(false);

	m_actionDrawLine = m_menuDrawing->addAction(tr("Railway (straight)"));
	m_actionDrawLine->setIcon(QIcon(":/icons/line.png"));
	m_actionDrawLine->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawLine);
	connect(m_actionDrawLine, SIGNAL(triggered()), this, SLOT(slot_drawLine()));

	m_actionDrawPlatformLine = m_menuDrawing->addAction(tr("Platform (straight)"));
	m_actionDrawPlatformLine->setIcon(QIcon(":/icons/platformLine.png"));
	m_actionDrawPlatformLine->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawPlatformLine);
	connect(m_actionDrawPlatformLine, SIGNAL(triggered()), this, SLOT(slot_drawPlatformLine()));

	m_actionDrawTunnelLine = m_menuDrawing->addAction(tr("Tunnel (straigth)"));
	m_actionDrawTunnelLine->setIcon(QIcon(":/icons/tunnelLine.png"));
	m_actionDrawTunnelLine->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawTunnelLine);
	connect(m_actionDrawTunnelLine, SIGNAL(triggered()), this, SLOT(slot_drawTunnelLine()));

	m_menuDrawing->addSeparator();
	m_toolbarDrawing->addSeparator();

	m_actionDrawArc = m_menuDrawing->addAction(tr("Railway (arc)"));
	m_toolbarDrawing->addAction(m_actionDrawArc);
	m_actionDrawArc->setIcon(QIcon(":/icons/arc.png"));
	m_actionDrawArc->setCheckable(true);
	connect(m_actionDrawArc, SIGNAL(triggered()), this, SLOT(slot_drawArc()));

	m_actionDrawPlatformArc = m_menuDrawing->addAction(tr("Platform (arc)"));
	m_actionDrawPlatformArc->setIcon(QIcon(":/icons/platformArc.png"));
	m_actionDrawPlatformArc->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawPlatformArc);
	connect(m_actionDrawPlatformArc, SIGNAL(triggered()), this, SLOT(slot_drawPlatformArc()));
	
	m_actionDrawTunnelArc = m_menuDrawing->addAction(tr("Tunnel (arc)"));
	m_actionDrawTunnelArc->setIcon(QIcon(":/icons/tunnelArc.png"));
	m_actionDrawTunnelArc->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawTunnelArc);
	connect(m_actionDrawTunnelArc, SIGNAL(triggered()), this, SLOT(slot_drawTunnelArc()));
	
	m_menuDrawing->addSeparator();
	m_toolbarDrawing->addSeparator();
	
	m_actionDrawPoints = m_menuDrawing->addAction(tr("Points"));
	m_actionDrawPoints->setIcon(QIcon(":/icons/points.png"));
	m_actionDrawPoints->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawPoints);
	connect(m_actionDrawPoints, SIGNAL(triggered()), this, SLOT(slot_drawPoints()));
	
	m_menuDrawing->addSeparator();
	m_toolbarDrawing->addSeparator();
	
	m_actionDrawText = m_menuDrawing->addAction(tr("Text"));
	m_actionDrawText->setIcon(QIcon(":/icons/text.png"));
	m_actionDrawText->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawText);
	connect(m_actionDrawText, SIGNAL(triggered()), this, SLOT(slot_drawText()));
	
	m_actionDrawImage = m_menuDrawing->addAction(tr("Image"));
	m_actionDrawImage->setIcon(QIcon(":/icons/image.png"));
	m_actionDrawImage->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawImage);
	connect(m_actionDrawImage, SIGNAL(triggered()), this, SLOT(slot_drawImage()));
	
	// Initialization menu Modify
	m_menuModify = m_menuEditor->addMenu(tr("Modify"));
	m_toolbarModify = addToolBar(tr("Modify"));
	m_toolbarModify->hide();
	m_toolbarModify->setEnabled(false);
	m_menuModify->setEnabled(false);
	
	m_actionSelection = m_menuModify->addAction(tr("Selection"));
	m_actionSelection->setIcon(QIcon(":/icons/selection.png"));
	m_actionSelection->setCheckable(true);
	m_toolbarModify->addAction(m_actionSelection);
	connect(m_actionSelection, SIGNAL(triggered()), this, SLOT(slot_selection()));

	m_menuModify->addSeparator();
	m_toolbarModify->addSeparator();
	
	m_actionPlatformName = m_menuModify->addAction(tr("Change platform name"));
	m_actionPlatformName->setIcon(QIcon(":/icons/modifyPlatformName.png"));
	m_actionPlatformName->setCheckable(true);
	m_toolbarModify->addAction(m_actionPlatformName);
	connect(m_actionPlatformName, SIGNAL(triggered()), this, SLOT(slot_platformName()));
	
	m_menuModify->addSeparator();
	m_toolbarModify->addSeparator();

	m_actionTranslate = m_menuModify->addAction(tr("Translate"));
	m_actionTranslate->setIcon(QIcon(":/icons/modifyTranslate.png"));
	m_actionTranslate->setCheckable(true);
	m_toolbarModify->addAction(m_actionTranslate);
	connect(m_actionTranslate, SIGNAL(triggered()), this, SLOT(slot_translate()));

	m_actionRotate = m_menuModify->addAction(tr("Rotate"));
	m_actionRotate->setIcon(QIcon(":/icons/modifyRotate.png"));
	m_actionRotate->setCheckable(true);
	m_toolbarModify->addAction(m_actionRotate);
	connect(m_actionRotate, SIGNAL(triggered()), this, SLOT(slot_rotate()));
	
	m_actionScale = m_menuModify->addAction(tr("Scale"));
	m_actionScale->setIcon(QIcon(":/icons/modifyScale.png"));
	m_actionScale->setCheckable(true);
	m_toolbarModify->addAction(m_actionScale);
	connect(m_actionScale, SIGNAL(triggered()), this, SLOT(slot_scale()));

	m_actionCopy = m_menuModify->addAction(tr("Copy"));
	m_actionCopy->setIcon(QIcon(":/icons/modifyCopy.png"));
	m_actionCopy->setCheckable(true);
	m_toolbarModify->addAction(m_actionCopy);
	connect(m_actionCopy, SIGNAL(triggered()), this, SLOT(slot_copy()));
	
	m_menuModify->addSeparator();
	m_toolbarModify->addSeparator();
	
	m_actionFontType = m_menuModify->addAction(tr("Change font..."));
	m_actionFontType->setIcon(QIcon(":/icons/fontType.png"));
	m_toolbarModify->addAction(m_actionFontType);
	connect(m_actionFontType, SIGNAL(triggered()), this, SLOT(slot_fontType()));
	m_actionFontType->setEnabled(false);
	
	m_actionFontColor = m_menuModify->addAction(tr("Change text color..."));
	m_actionFontColor->setIcon(QIcon(":/icons/fontColor.png"));
	m_toolbarModify->addAction(m_actionFontColor);
	connect(m_actionFontColor, SIGNAL(triggered()), this, SLOT(slot_fontColor()));
	m_actionFontColor->setEnabled(false);

	// Initialization menu Constraints
	m_menuConstraints = m_menuEditor->addMenu(tr("Constraints"));
	m_toolbarConstraints = addToolBar(tr("Constraints"));
	m_toolbarConstraints->hide();
	m_toolbarConstraints->setEnabled(false);
	m_menuConstraints->setEnabled(false);
	
	m_actionOrtho = m_menuConstraints->addAction(tr("Ortho"));
	m_actionOrtho->setIcon(QIcon(":/icons/orthoDisengaged.png"));
	m_actionOrtho->setCheckable(true);
	m_toolbarConstraints->addAction(m_actionOrtho);
	connect(m_actionOrtho, SIGNAL(triggered()), this, SLOT(slot_ortho()));

	m_actionSnapping = m_menuConstraints->addAction(tr("Snapping"));
	m_actionSnapping->setIcon(QIcon(":/icons/snappingEngaged.png"));
	m_actionSnapping->setCheckable(true);
	m_actionSnapping->toggle();
	m_toolbarConstraints->addAction(m_actionSnapping);
	connect(m_actionSnapping, SIGNAL(triggered()), this, SLOT(slot_snapping()));	

	// Initialization menu View
	m_menuView= menuBar()->addMenu(tr("View"));
	m_menuView->setEnabled(false);
	
	m_actionViewDrivingPanel = m_menuView->addAction(tr("Driving Panel"));
	m_actionViewDrivingPanel->setIcon(QIcon(":/icons/viewDrivingPanel.png"));
	connect(m_actionViewDrivingPanel, SIGNAL(triggered()), m_dockDriving, SLOT(show()));
	m_actionViewDrivingPanel->setEnabled(false);
	
	m_actionViewCompo = m_menuView->addAction(tr("Compositions"));
	m_actionViewCompo->setIcon(QIcon(":/icons/viewCompo.png"));
	connect(m_actionViewCompo, SIGNAL(triggered()), m_dockCompo, SLOT(show()));
	m_actionViewCompo->setEnabled(false);
	
	// Initialization menu Help
	m_menuHelp= menuBar()->addMenu(tr("Help"));
	
	m_actionDoc = m_menuHelp->addAction(tr("Documentation"));
	m_actionDoc->setIcon(QIcon(":/icons/helpDoc.png"));
	connect(m_actionDoc, SIGNAL(triggered()), this, SLOT(slot_doc()));
	
	m_actionAbout = m_menuHelp->addAction(tr("About"));
	m_actionAbout->setIcon(QIcon(":/icons/helpAbout.png"));
	connect(m_actionAbout, SIGNAL(triggered()), m_dialogAbout, SLOT(show()));

	// Initialization status bar
	m_statusBar = statusBar();
	m_labelX = new QLabel("X = 0", this);
	m_labelY = new QLabel("Y = 0", this);
	m_statusBar->addPermanentWidget(m_labelX);
	m_statusBar->addPermanentWidget(m_labelY);
	
	// QTimer
    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(slot_advance()));
}

void Window::setCoordStatusBar(qreal x_, qreal y_)
{
	m_labelX->setText("X = " + QString::number(x_));
	m_labelY->setText("Y = " + QString::number(y_));
}

void Window::slot_newScene()
{
	DialogNewMap* dialogNewMap = new DialogNewMap(this);

	if(dialogNewMap->getStatus() == true)
	{
		// Show dock driving panel
		m_dockDriving->show();
		m_dockCompo->show();
		
		// Create new scene
		prepareScene();
		m_scene->setSceneRect(0, 0, dialogNewMap->getWidth()+1000, dialogNewMap->getHeight()+1000);
		
		// Insert background image
		QPixmap pixmap = QPixmap(dialogNewMap->getFilename());
		ImageItem* image = new ImageItem(m_scene, m_scene->searchLayer("BACKGROUND"), QPointF(500,500), pixmap.transformed(QTransform::fromScale(1, -1),Qt::SmoothTransformation), dialogNewMap->getFilename());
		
		// Scale background image
		image->scaleItem(QPointF(0,0), dialogNewMap->getResolution());
		image->ShapeItem::setZValue(0);
		
		// Make it not selectable
		image->ShapeItem::setFlag(QGraphicsItem::ItemIsSelectable, false);
		image->ShapeItem::setFlag(QGraphicsItem::ItemIsFocusable, false);
		
		m_view->fitInView(m_scene->sceneRect(), Qt::KeepAspectRatio);
	}
	
	delete dialogNewMap;
}

void Window::prepareScene()
{
	// If first start
	if(m_scene != NULL)
	{
		closeMap();
		
		delete m_scene;		
		m_scene = NULL;
	}
		
	// Create scene and view
	m_scene = new Scene(this);
	m_view->setScene(m_scene);
	
	// If we are in edition and open a new file
	activateEditor(false);
	
	// Activate menu
	m_actionSaveAs->setEnabled(true);
	
	m_actionSnapping->setChecked(true);
	m_menuTrains->setEnabled(true);
	m_menuEditor->setEnabled(true);
	m_toolbarTrains->setEnabled(true);
	
	m_menuView->setEnabled(true);
	m_actionViewDrivingPanel->setEnabled(true);
	m_actionViewCompo->setEnabled(true);
	
	// Set scene to driving panel
	m_drivingPanel->setScene(m_scene);
	m_dialogCompo->setScene(m_scene);
	
	m_dialogCompo->updateList(NULL);
	
	// Start timer
	m_timer.start(1000 / 33);
}

void Window::slot_fileOpen()
{
	// Get filename through GUI
	QString filename = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("NadiRail maps (*.tml)"));
    
	if(filename!="")
	{
		// Create a new empty scene
		prepareScene();
		
		// Wait cursor
		setCursor(Qt::WaitCursor);
		
		// Read the file
		if(IO::readTML(m_scene, m_view, filename) == false)
			QMessageBox::critical(this,tr("Critical Error"),tr("Failed to read file"));
		else
		{
			// If success, update the title bar with the filename
			QFileInfo fileInfo(filename);
			QString title = "NadiRail - " + fileInfo.fileName();
			setWindowTitle(title);
			
			// Desactivate editor: important consequence, all items are not selectable after being read
			activateEditor(false);
			
			// Show dock driving panel
			m_dialogCompo->updateList(NULL);
			m_dockDriving->show();
			m_dockCompo->show();
			
			// Show scenario message
			if(m_scene->getMessageScenario() != "")
				QMessageBox::information(this, fileInfo.fileName(), m_scene->getMessageScenario());
		}
	} 
}

void Window::slot_fileSaveAs()
{
	// Get a filename through GUI
	QString filename = QFileDialog::getSaveFileName(this, tr("Save as..."), "", tr("NadiRail maps (*.tml)"));
	
	if(filename!="")
	{
		// If filename not empty, attribute it to the scene
		m_scene->setFilename(filename);
		
		// Save the scene. If fail, return error message. If success, update the title bar with the filename
		if(IO::writeTML(m_scene, m_view, filename) == false)
			QMessageBox::critical(this,tr("Critical Error"),tr("Failed to save file"));
		else
		{
			QFileInfo fileInfo(filename);
			QString title = "NadiRail - " + fileInfo.fileName();
			setWindowTitle(title);
		}
	}
}

void Window::slot_fileOptions()
{
	DialogOptions::getOptions(this);
}

void Window::closeMap()
{
	// Check if modification and propose to save
	if(m_scene->items().size() > 1) // there is 1 invisible item which is the snapping item
	{
		QMessageBox::StandardButton button;

		button = QMessageBox::question(this, windowTitle(), tr("Would you like to save before leaving?"), QMessageBox::Yes|QMessageBox::No);
	
		if (button == QMessageBox::Yes)
			slot_fileSaveAs();
	}
}

void Window::closeEvent(QCloseEvent* event_)
{
	closeMap();
	event_->accept();
}

void Window::slot_editMap()
{
	if(m_actionEditMap->isChecked())
		activateEditor(true);
	else
		activateEditor(false);
}

void Window::slot_addLocomotive()
{
	// Check if there is any platform in the scene
	foreach(QGraphicsItem* item, m_scene->items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			// Check if the pathitem is a platform
			if(shape->getType() == 41 or shape->getType() == 21)
			{
				CarItem* car = DialogNewLoco::newLoco(m_scene, this);
				
				if(car != NULL)
				{
					m_scene->setSceneSelectItem(car);
				}
				
				return;
			}
		}
	}
	
	// If no platform found
	QMessageBox::information(this, tr("No platform"), tr("There is no platform in your map. Please create at least one platform using the editor."));
}

void Window::slot_addCar()
{
	// Position the car on the platform
	foreach(QGraphicsItem* item, m_scene->items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			// Find the path with matching name
			if(shape->getType() == 41 or shape->getType() == 21)
			{
				CarItem* car = DialogNewCar::newCar(m_scene, this, "");
	
				if(car != NULL)
				{
					m_scene->setSceneSelectItem(car);
					centerOn(car->pos());
				}
				
				return;
			}
			
		}
	}
	
	// If no platform found
	QMessageBox::information(this, tr("No platform"), tr("There is no platform in your map. Please create at least one platform using the editor."));
}

void Window::activateEditor(bool status_)
{
	if(status_==true)
	{
		// Stop timer
		m_timer.stop();
	
		// Activate menu
		m_actionEditScenario->setEnabled(true);
		m_menuDrawing->setEnabled(true);
		m_menuModify->setEnabled(true);
		m_menuConstraints->setEnabled(true);

		// Activate and show toolbars
		m_toolbarDrawing->setEnabled(true);
		m_toolbarModify->setEnabled(true);
		m_toolbarConstraints->setEnabled(true);
		
		m_toolbarDrawing->show();
		m_toolbarModify->show();
		m_toolbarConstraints->show();
		
		// Hide driving panel
		m_dockDriving->hide();
		m_dockCompo->hide();
		
		// Disable views
		m_actionViewDrivingPanel->setEnabled(false);
		m_actionViewCompo->setEnabled(false);	
		
		// Tell scene we are in edition mode
		m_scene->setStatusEditor(true);
		
		// Items selectable
		foreach(QGraphicsItem* item, m_scene->items())
		{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			if(shape)
			{
				// Do not make the background image selectable
				if(shape->getLayer()->getName() != "BACKGROUND")
				{
					shape->setFlag(QGraphicsItem::ItemIsSelectable, true);
					shape->setFlag(QGraphicsItem::ItemIsFocusable, true);
				}
			}
		}
	}
	else
	{
		m_scene->setStatusEditor(false);
		m_scene->disableDrawingAll();
		
		// Desactivate button editor
		m_actionEditMap->setChecked(false);
		m_actionEditScenario->setEnabled(false);
		
		// Desactivate menu
		m_menuDrawing->setEnabled(false);
		m_menuModify->setEnabled(false);
		m_menuConstraints->setEnabled(false);

		// Show toolbars
		m_toolbarDrawing->setEnabled(false);
		m_toolbarModify->setEnabled(false);
		m_toolbarConstraints->setEnabled(false);
		
		m_toolbarDrawing->hide();
		m_toolbarModify->hide();
		m_toolbarConstraints->hide();
		
		// Show driving panel
		m_dockDriving->show();
		m_dockCompo->show();
		
		// Enable views
		m_actionViewDrivingPanel->setEnabled(true);
		m_actionViewCompo->setEnabled(true);
		
		// Enable or disable add locs and add car
		if(m_scene->getLocsAllowed() == true)
			m_actionAddLocomotive->setEnabled(true);
		else
			m_actionAddLocomotive->setEnabled(false);
			
		// Enable or disable add locs and add car
		if(m_scene->getCarsAllowed() == true)
			m_actionAddCar->setEnabled(true);
		else
			m_actionAddCar->setEnabled(false);
		
		// Set wait cursor
		setCursor(Qt::WaitCursor);
		
		// Reconstruct topology
		m_scene->reconstructTopology();
		
		// Update list platforms scenario
		m_scene->updateListPlatformsScenario();
		
		// Items not selectable
		foreach(QGraphicsItem* item, m_scene->items())
		{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			if(shape)
			{
				shape->setFlag(QGraphicsItem::ItemIsSelectable, false);
				shape->setFlag(QGraphicsItem::ItemIsFocusable, false);
			}
		}
		
		// Set normal cursor
		setCursor(Qt::ArrowCursor);
		
		// Start timer again
		m_timer.start(1000 / 33);
	}
}

void Window::updateCompo(bool isFromHooking_)
{
	if(isFromHooking_ == false)
	{
		// If this is a complete regular update
		m_dialogCompo->updateList(NULL);
	}
	else
	{
		// This function is to be called by Scene on hook/dehook
		m_dialogCompo->updateList(m_drivingPanel->getActiveLoc());
	}
}

void Window::stopLoc(CarItem* loc_)
{
	// This function is to be called by Scene when a car bump
	m_drivingPanel->stopLoc(loc_);
}

void Window::slot_editScenario()
{
	DialogScenario::editScenario(m_scene, this);
	
	// Enable or disable add locs and add car
	if(m_scene->getLocsAllowed() == true)
		m_actionAddLocomotive->setEnabled(true);
	else
		m_actionAddLocomotive->setEnabled(false);
		
	// Enable or disable add locs and add car
	if(m_scene->getCarsAllowed() == true)
		m_actionAddCar->setEnabled(true);
	else
		m_actionAddCar->setEnabled(false);
}

void Window::slot_selection()
{
	if(m_actionSelection->isChecked())
		m_scene->setSceneSelection(true);
	else
		m_scene->setSceneSelection(false);
}

void Window::slot_drawLine()
{
	if(m_actionDrawLine->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setSceneLine(true);
		else
			m_scene->setSceneLine(false);
	}
	else
		m_scene->setSceneLine(false);
}

void Window::slot_drawPlatformLine()
{
	if(m_actionDrawPlatformLine->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setScenePlatformLine(true);
		else
			m_scene->setScenePlatformLine(false);
	}
	else
		m_scene->setScenePlatformLine(false);
}

void Window::slot_drawTunnelLine()
{
	if(m_actionDrawTunnelLine->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setSceneTunnelLine(true);
		else
			m_scene->setSceneTunnelLine(false);
	}
	else
		m_scene->setSceneTunnelLine(false);
}

void Window::slot_drawPoints()
{
	if(m_actionDrawPoints->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setScenePoints(true);
		else
			m_scene->setScenePoints(false);
	}
	else
		m_scene->setScenePoints(false);
}

void Window::slot_drawArc()
{
	if(m_actionDrawArc->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setSceneArc(true);
		else
			m_scene->setSceneArc(false);
	}
	else
		m_scene->setSceneArc(false);
}

void Window::slot_drawPlatformArc()
{
	if(m_actionDrawPlatformArc->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setScenePlatformArc(true);
		else
			m_scene->setScenePlatformArc(false);
	}
	else
		m_scene->setScenePlatformArc(false);
}

void Window::slot_drawTunnelArc()
{
	if(m_actionDrawTunnelArc->isChecked())
	{
		if(m_scene->searchLayer("RAILWAYS")->isVisible() == true)
			m_scene->setSceneTunnelArc(true);
		else
			m_scene->setSceneTunnelArc(false);
	}
	else
		m_scene->setSceneTunnelArc(false);
}
	
void Window::slot_drawText()
{
	if(m_actionDrawText->isChecked())
	{
		if(m_scene->searchLayer("TEXT")->isVisible() == true)
			m_scene->setSceneText(true);
		else
			m_scene->setSceneText(false);
	}
	else
		m_scene->setSceneText(false);
}

void Window::slot_drawImage()
{
	if(m_actionDrawImage->isChecked())
	{
		if(m_scene->searchLayer("IMAGES")->isVisible() == true)
			m_scene->setSceneImage(true);
		else
			m_scene->setSceneImage(false);
	}
	else
		m_scene->setSceneImage(false);
}

void Window::slot_fontType()
{
	QFont font_;
	foreach (QGraphicsItem *item, m_scene->items())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, get the font
				if (textItem_)
					font_ = textItem_->font();
			}
	
	bool ok;
	font_ = QFontDialog::getFont(&ok, font_, this);
	
	if(ok)
		m_scene->applyFontTexts(font_);	
}

void Window::slot_fontColor()
{
	QColor color_ ;
	foreach (QGraphicsItem *item, m_scene->items())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, get the color
				if (textItem_)
					color_ = textItem_->defaultTextColor();
			}
			
	color_ = QColorDialog::getColor(Qt::white, this);
	
	if(color_.isValid()==false)
		return;
	else
		m_scene->applyColorTexts(color_);	
}

void Window::slot_ortho()
{ 
	m_scene->toggleOrtho(); 
	
	if(m_scene->isOrthoEngaged() == true)
	{
		m_actionOrtho->setIcon(QIcon(":/icons/orthoEngaged.png"));
		m_actionOrtho->setChecked(true);
	}
	else
	{
		m_actionOrtho->setIcon(QIcon(":/icons/orthoDisengaged.png"));
		m_actionOrtho->setChecked(false);
	}
}

void Window::slot_snapping()
{
	m_scene->toggleSnapping(); 
	
	if(m_scene->isSnappingEngaged() == true)
	{
		m_actionSnapping->setIcon(QIcon(":/icons/snappingEngaged.png"));
		m_actionSnapping->setChecked(true);
	}
	else
	{
		m_actionSnapping->setIcon(QIcon(":/icons/snappingDisengaged.png"));
		m_actionSnapping->setChecked(false);
	}
	
}	

void Window::setEditCursor(bool status_)
{
	if(status_==true)
		m_view->setCursor(Qt::CrossCursor);
	else
		m_view->setCursor(Qt::ArrowCursor);
}

void Window::setSelectionButton(bool status_)
{
	m_actionSelection->setChecked(status_);
	setEditCursor(status_);
}

void Window::setLineButton(bool status_)
{
	m_actionDrawLine->setChecked(status_);
	setEditCursor(status_);
}

void Window::setPlatformLineButton(bool status_)
{
	m_actionDrawPlatformLine->setChecked(status_);
	setEditCursor(status_);
}

void Window::setTunnelLineButton(bool status_)
{
	m_actionDrawTunnelLine->setChecked(status_);
	setEditCursor(status_);
}

void Window::setPointsButton(bool status_)
{
	m_actionDrawPoints->setChecked(status_);
	setEditCursor(status_);
}

void Window::setArcButton(bool status_)
{
	m_actionDrawArc->setChecked(status_);
	setEditCursor(status_);
}

void Window::setPlatformArcButton(bool status_)
{
	m_actionDrawPlatformArc->setChecked(status_);
	setEditCursor(status_);
}

void Window::setTunnelArcButton(bool status_)
{
	m_actionDrawTunnelArc->setChecked(status_);
	setEditCursor(status_);
}

void Window::setTextButton(bool status_)
{
	m_actionDrawText->setChecked(status_);
	setEditCursor(status_);
}

void Window::setImageButton(bool status_)
{
	m_actionDrawImage->setChecked(status_);
	setEditCursor(status_);
}

void Window::setPlatformNameButton(bool status_)
{
	m_actionPlatformName->setChecked(status_);
	setEditCursor(status_);
}

void Window::setTranslateButton(bool status_)
{
	m_actionTranslate->setChecked(status_);
	setEditCursor(status_);
}

void Window::setRotateButton(bool status_)
{
	m_actionRotate->setChecked(status_);
	setEditCursor(status_);
}

void Window::setScaleButton(bool status_)
{
	m_actionScale->setChecked(status_);
	setEditCursor(status_);
}

void Window::setCopyButton(bool status_)
{
	m_actionCopy->setChecked(status_);
	setEditCursor(status_);
}

QString Window::openImageDialog()
{
	return QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.tif)"));
}

void Window::setFontActions(bool status_)
{
	m_actionFontType->setEnabled(status_);
	m_actionFontColor->setEnabled(status_);
}

void Window::keyRelease(int key_)
{
		switch (key_)
		{
			
			case Qt::Key_Space:
				m_drivingPanel->setBreaking(false);
				break;
		}
}

void Window::keyPress(int key_)
{		
	switch (key_)
		{
			
			case Qt::Key_Return:
				m_dialogCompo->nextLoc();
				break;
			case Qt::Key_Control:
				m_drivingPanel->slot_reverse();
				break;
			case Qt::Key_Space:
				m_drivingPanel->setBreaking(true);
				break;
			case Qt::Key_Up:
				m_drivingPanel->increaseSpeed();
				break;
			case Qt::Key_Down:
				m_drivingPanel->decreaseSpeed();
				break;
			case Qt::Key_Alt:
				m_drivingPanel->speedLimiter();
				break;
			case Qt::Key_AltGr:
				m_drivingPanel->speedLimiter();
				break;
			case Qt::Key_F:
				m_drivingPanel->follow();
				break;
		}
}

void Window::slot_platformName()
{
	if(m_actionPlatformName->isChecked())
		m_scene->setChangePlatformName(true);
	else
		m_scene->setChangePlatformName(false);
}

void Window::slot_translate()
{
	if(m_actionTranslate->isChecked())
		m_scene->setSceneTranslate(true);
	else
		m_scene->setSceneTranslate(false);
}

void Window::slot_rotate()
{
	if(m_actionRotate->isChecked())
		m_scene->setSceneRotate(true);
	else
		m_scene->setSceneRotate(false);
}

void Window::slot_scale()
{
	if(m_actionScale->isChecked())
		m_scene->setSceneScale(true);
	else
		m_scene->setSceneScale(false);
}

void Window::slot_copy()
{
	if(m_actionCopy->isChecked())
		m_scene->setSceneCopy(true);
	else
		m_scene->setSceneCopy(false);
}

void Window::slot_advance()
{
	// Center view on active loc
	QPointF center = m_drivingPanel->getFollow();
	
	if(center != QPointF(0,0))
		m_view->centerOn(center);
	
	// Update speed in driving panel
	m_drivingPanel->updateSpeed();
	
	// Move all items in scene (call QGraphicsItem::advance())
	m_scene->advance();
}

void Window::slot_doc()
{
	QDesktopServices::openUrl(QUrl("https://nadirail.frama.io/documentation.html", QUrl::TolerantMode));
}

void Window::centerOn(QPointF pos_)
{
//	m_view->fitInView (QRectF(pos_,QSize(100,100)), Qt::KeepAspectRatio);
	m_view->centerOn(pos_);
}

void Window::setActiveLoc(CarItem* loc_)
{
	m_dialogCompo->selectLoc(loc_);
}

void Window::setDrivingPanelLoc(CarItem* loc_)
{
	m_drivingPanel->setActiveLoc(loc_);
}

bool Window::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        // Intercept focus in compos
           if(obj == m_dialogCompo)
               keyPress(keyEvent->key());
    }
    return QObject::eventFilter(obj, event);
}
