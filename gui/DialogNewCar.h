#ifndef DIALOGNEWCAR_H_INCLUDED
#define DIALOGNEWCAR_H_INCLUDED

#include <QtWidgets>
#include "core/support/Scene.h"
#include "core/shapes/CarItem.h"

#include "ui_DialogNewCar.h"

class Scene;

class DialogNewCar : public QDialog
{
	Q_OBJECT

	public:
		DialogNewCar(Scene*, QWidget*, QString);
		static CarItem* newCar(Scene*, QWidget*, QString);
		CarItem* getCarItem() const;
		
	protected:
		virtual void closeEvent(QCloseEvent*);
		
	private slots:
		void slot_changeType(QString);
		void slot_changeLoad(int);
		void slot_color();
		void slot_valueChanged(double);
		void slot_selectThumbnail();
		void slot_OK();
		void slot_cancel();
	
	private:
		Scene* m_scene;
		QPixmap m_pixmap;
		QString m_filename;
		CarItem* m_carItem;
		QColor m_color;
		Ui::DialogNewCar ui;
};

#endif
