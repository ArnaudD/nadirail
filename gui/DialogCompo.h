#ifndef DIALOGCOMPO_H_INCLUDED
#define DIALOGCOMPO_H_INCLUDED

#include <QtWidgets>
#include "core/support/Scene.h"
#include "core/shapes/CarItem.h"
#include "gui/Window.h"

#include "ui_DialogCompo.h"

class Scene;

class DialogCompo : public QWidget
{
	Q_OBJECT

	public:
		DialogCompo(Window*);
		void setScene(Scene*);
		void updateList(CarItem*);
		void selectLoc(CarItem*);
		void nextLoc();
		
	private slots:
		void slot_changeLoc(QListWidgetItem*);
	
	private:
		Ui::DialogCompo ui;
		Scene* m_scene;
		CarItem* m_activeLoc;	
		Window* m_window;
};

#endif
