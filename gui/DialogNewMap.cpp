#include <gui/DialogNewMap.h>
#include <core/support/Scene.h>

DialogNewMap::DialogNewMap(QWidget *parent_ = 0) : QDialog(parent_)
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	
	ui.setupUi(this);
	
	connect(ui.pushButtonImage, SIGNAL (clicked()),this, SLOT (slot_selectImage()));
	connect(ui.pushButtonOK, SIGNAL (clicked()),this, SLOT (slot_OK()));
	connect(ui.doubleSpinBox, SIGNAL (valueChanged(double)),this, SLOT (slot_valueChanged(double)));
	
	m_status = false;
	
	exec();
}

bool DialogNewMap::getStatus() const
{
	return m_status;
}

void DialogNewMap::slot_selectImage()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.tif)"));
	
	if(filename!="")
	{
		ui.lineEditImagePath->setText(filename);
		
		m_pixmap = QPixmap(filename);
		
		ui.lineEditX->setText(QString::number(m_pixmap.width()*ui.doubleSpinBox->value()));
		ui.lineEditY->setText(QString::number(m_pixmap.height()*ui.doubleSpinBox->value()));
	}
}

void DialogNewMap::slot_valueChanged(double)
{
	if(ui.lineEditImagePath->text() != "")
	{	
		ui.lineEditX->setText(QString::number(m_pixmap.width()*ui.doubleSpinBox->value()));
		ui.lineEditY->setText(QString::number(m_pixmap.height()*ui.doubleSpinBox->value()));
	}
}

qreal DialogNewMap::getResolution() const
{
	return ui.doubleSpinBox->value();
}

qreal DialogNewMap::getWidth() const
{
	return m_width;
}

qreal DialogNewMap::getHeight() const
{
	return m_height;
}

QString DialogNewMap::getFilename() const
{
	return ui.lineEditImagePath->text();
}

void DialogNewMap::slot_OK()
{
	// If an image didn't have been specified, prevent the closure
	if(ui.lineEditImagePath->text() != "")
	{
		// Set scene dimension
		m_width = (ui.lineEditX->text()).toDouble();
		m_height = (ui.lineEditY->text()).toDouble();
	
		m_status = true;
		close();
	}
	else
	{
		QMessageBox::information(this,tr("Background image not selected"),tr("You must specify a background image."));
		return;
	}
}
