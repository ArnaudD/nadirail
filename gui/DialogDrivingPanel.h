#ifndef DIALOGDRIVINGPANEL_H_INCLUDED
#define DIALOGDRIVINGPANEL_H_INCLUDED

#include <QtWidgets>
#include "core/support/Scene.h"
#include "core/shapes/CarItem.h"
#include "gui/Window.h"

#include "ui_DialogDrivingPanel.h"

class Scene;

class DialogDrivingPanel : public QWidget
{
	Q_OBJECT

	public:
		DialogDrivingPanel(Window*);
		void setScene(Scene*);
		void updateSpeed();
		QPointF getFollow();
		void stopLoc(CarItem*);
		void setActiveLoc(CarItem*);
		CarItem* getActiveLoc() const;
		void increaseSpeed();
		void decreaseSpeed();
		void setBreaking(bool);
		void speedLimiter();
		void follow();
		
	public slots:
		void slot_sliderMoved(int);
		void slot_breaking();
		void slot_reverse();
		void slot_speedLimiter(int);
	
	private:
		Ui::DialogDrivingPanel ui;
		Scene* m_scene;
		Window* m_window;
		CarItem* m_activeLoc;
};

#endif
