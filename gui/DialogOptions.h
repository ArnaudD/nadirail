#ifndef DIALOGOPTIONS_H_INCLUDED
#define DIALOGOPTIONS_H_INCLUDED

#include <QtWidgets>
#include "core/support/Settings.h"

#include "ui_DialogOptions.h"

class Scene;

class DialogOptions : public QDialog
{
	Q_OBJECT

	public:
		DialogOptions(QWidget*);
		static void getOptions(QWidget*);
		
	protected:
		virtual void closeEvent(QCloseEvent*);
		
	private slots:
		void slot_OK();
		void slot_cancel();
		void slot_changeLanguage(int);
		void slot_colorTracks();
		void slot_colorRailway();
		void slot_colorPlatform();
	
	private:
		Ui::DialogOptions ui;
		bool m_langageIsChanged;
		
		QColor m_colorTracks;
		QColor m_colorRailway;
		QColor m_colorPlatform;
};

#endif
