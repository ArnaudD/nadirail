#ifndef DIALOGSCENARIO_H_INCLUDED
#define DIALOGSCENARIO_H_INCLUDED

#include <QtWidgets>
#include "core/support/Definitions.h"
#include "core/support/Scene.h"
#include "gui/DialogCondition.h"
#include "ui_DialogScenario.h"

class Scene;

class DialogScenario : public QDialog
{
	Q_OBJECT

	public:
		DialogScenario(Scene*, QWidget*);
		static void editScenario(Scene*, QWidget*);
		
	protected:
		virtual void closeEvent(QCloseEvent*);
		
	private slots:
		void slot_OK();
		void slot_editCondition();
	
	private:
		void refreshListWidgetPlatforms();
		
		Ui::DialogScenario ui;
		Scene* m_scene;
		QList<ShapeItem*> m_listPlatforms;
};

#endif
