#include <gui/DialogScenario.h>
#include <core/support/Scene.h>

DialogScenario::DialogScenario(Scene* scene_, QWidget *parent_) : QDialog(parent_)
{
	ui.setupUi(this);
	
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	
	m_scene = scene_;
	
	// Fill m_listPlatforms
	foreach(QGraphicsItem* item, m_scene->items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
		
		if(shape)
		{
			if(shape->getType() == 41 or shape->getType() == 21)
			{
				m_listPlatforms.append(shape);
			}
		}
	}
	
	// Enable or disable checkbox add locs and add cars
	if(m_scene->getLocsAllowed() == true)
		ui.checkBoxAddLocs->setCheckState(Qt::Checked);
	else
		ui.checkBoxAddLocs->setCheckState(Qt::Unchecked);
		
	if(m_scene->getCarsAllowed() == true)
		ui.checkBoxAddCars->setCheckState(Qt::Checked);
	else
		ui.checkBoxAddCars->setCheckState(Qt::Unchecked);
	
	// Fill ListWidget
	refreshListWidgetPlatforms();
	
	// Fill PlainTextEdit
	ui.textEditScenario->setPlainText(m_scene->getMessageScenario());
	ui.textEditVictory->setPlainText(m_scene->getMessageVictory());
	
	connect(ui.pushButtonOK, SIGNAL (clicked()), this, SLOT (slot_OK()));
	connect(ui.pushButtonEditCondition, SIGNAL (clicked()), this, SLOT (slot_editCondition()));
}

void DialogScenario::refreshListWidgetPlatforms()
{
	ui.listWidgetPlatforms->clear();
	
	QIcon iconLine(":/icons/platformLine.png");
	QIcon iconArc(":/icons/platformArc.png");
	QIcon iconLineCond(":/icons/platformLineCond.png");
	QIcon iconArcCond(":/icons/platformArcCond.png");
	
	// Iterate m_listPlatforms and fill listWidgetPlatform with the appropriate icon
	foreach(ShapeItem* shape, m_listPlatforms)
	{
		if(shape->getType() == 41)
		{
				if(shape->getListConditions().size() == 0)
					ui.listWidgetPlatforms->addItem(new QListWidgetItem(iconLine, shape->getName()));
				else
				{
					ui.listWidgetPlatforms->addItem(new QListWidgetItem(iconLineCond, shape->getName()));
				}
		}
		else if(shape->getType() == 21)	
		{
				if(shape->getListConditions().size() == 0)
					ui.listWidgetPlatforms->addItem(new QListWidgetItem(iconArc, shape->getName()));
				else
					ui.listWidgetPlatforms->addItem(new QListWidgetItem(iconArcCond, shape->getName()));
		}
	}
}

void DialogScenario::closeEvent(QCloseEvent*)
{
	slot_OK();
}

void DialogScenario::editScenario(Scene* scene_, QWidget *parent_)
{
	DialogScenario* dialogScenario = new DialogScenario(scene_, parent_);
	dialogScenario->exec();
	dialogScenario->deleteLater();
}

void DialogScenario::slot_OK()
{
	m_scene->setMessageScenario(ui.textEditScenario->toPlainText());
	m_scene->setMessageVictory(ui.textEditVictory->toPlainText());
	
	m_scene->setLocsAllowed(ui.checkBoxAddLocs->isChecked());
	m_scene->setCarsAllowed(ui.checkBoxAddCars->isChecked());
	
	close();
}

void DialogScenario::slot_editCondition()
{
	if(ui.listWidgetPlatforms->currentRow() >= 0)
		DialogCondition::newCondition(this, m_listPlatforms.at(ui.listWidgetPlatforms->currentRow()));
	else
		QMessageBox::information(this,"No condition selected","Please select first a condition in the list above.");
	
	refreshListWidgetPlatforms();
}
