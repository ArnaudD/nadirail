#ifndef DIALOGNEWMAP_H_INCLUDED
#define DIALOGNEWMAP_H_INCLUDED

#include <QtWidgets>
#include "core/support/Scene.h"

#include "ui_DialogNewMap.h"

class Scene;

class DialogNewMap : public QDialog
{
	Q_OBJECT

	public:
		DialogNewMap(QWidget*);
		bool getStatus() const;
		qreal getWidth() const;
		qreal getHeight() const;
		qreal getResolution() const;
		QString getFilename() const;
		
	private slots:
		void slot_selectImage();
		void slot_valueChanged(double);
		void slot_OK();
	
	private:
		bool m_status;
		qreal m_width, m_height;
		QPixmap m_pixmap;
		Ui::DialogNewMap ui;
};

#endif
