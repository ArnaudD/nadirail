#include <gui/DialogNewLoco.h>
#include <core/support/Scene.h>

DialogNewLoco::DialogNewLoco(Scene* scene_, QWidget *parent_ = 0) : QDialog(parent_)
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	
	ui.setupUi(this);
	
	m_scene = scene_;
	m_color = Qt::red;
	
	ui.listWidgetCars->setIconSize(QSize(64, 64));
	
	// Locomotive widget color
	QPalette palette = ui.labelShape->palette();
	palette.setColor(QPalette::Window, m_color);
	ui.labelShape->setPalette(palette);
	
	// Fill in platforms
	foreach(QGraphicsItem* item, m_scene->items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			if(shape->getType() == 41 or shape->getType() == 21)
				ui.comboBoxPlatform->addItem(shape->getName());
		}
	}

	connect(ui.pushButtonColor, SIGNAL (clicked()),this, SLOT (slot_color()));
	connect(ui.doubleSpinBoxLength, SIGNAL (valueChanged(double)),this, SLOT (slot_valueChanged(double)));
	connect(ui.pushButtonImage, SIGNAL (clicked()),this, SLOT (slot_selectThumbnail()));
	connect(ui.pushButtonAddCar, SIGNAL (clicked()),this, SLOT (slot_addCar()));
	connect(ui.pushButtonCopyCar, SIGNAL (clicked()),this, SLOT (slot_copyCar()));
	connect(ui.pushButtonDeleteCar, SIGNAL (clicked()),this, SLOT (slot_deleteCar()));
	connect(ui.pushButtonOK, SIGNAL (clicked()),this, SLOT (slot_OK()));
	connect(ui.pushButtonCancel, SIGNAL (clicked()),this, SLOT (slot_cancel()));
	
	m_status = NULL;
}

void DialogNewLoco::closeEvent(QCloseEvent*)
{
	// Warning, this is call by any button. Just remove the cross on top
//	slot_cancel();
}

CarItem* DialogNewLoco::getStatus() const
{
	return m_status;
}

CarItem* DialogNewLoco::newLoco(Scene* scene_, QWidget *parent_ = 0)
{
	DialogNewLoco* dialogNewLoco = new DialogNewLoco(scene_, parent_);
	dialogNewLoco->exec();
	dialogNewLoco->deleteLater();
	
	return dialogNewLoco->getStatus();
}

void DialogNewLoco::slot_color()
{
	QColor color = QColorDialog::getColor(m_color, this);
	
	if(color.isValid() == true)
	{
		m_color = color;
		QPalette palette = ui.labelShape->palette();
		palette.setColor(QPalette::Window, m_color);
		ui.labelShape->setPalette(palette);
	}
}

void DialogNewLoco::slot_valueChanged(double value_)
{
	ui.labelShape->setFixedWidth(value_*3);
}

void DialogNewLoco::slot_selectThumbnail()
{
	m_filename = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.tif *.gif)"));
	
	if(m_filename!="")
	{
		ui.lineEditImagePath->setText(m_filename);
		m_pixmap = QPixmap(m_filename);
		ui.labelThumbnail->setPixmap(m_pixmap);
		ui.labelThumbnail->setText("");
	}
}

void DialogNewLoco::slot_addCar()
{
	CarItem* car = DialogNewCar::newCar(m_scene, this, ui.comboBoxPlatform->currentText());
	
	if(car != NULL)
	{
		m_listCars.append(car);
		
		if(car->getFilename() == "")
		{
			ui.listWidgetCars->addItem(new QListWidgetItem(car->getName()));
		}
		else
		{
			QIcon icon = QIcon(car->getPixmap());
			ui.listWidgetCars->addItem(new QListWidgetItem(icon, car->getName()));
		}
	}
}

void DialogNewLoco::slot_copyCar()
{
	if(m_listCars.size() > 0)
	{
		CarItem* lastCar = m_listCars.last();
		
		CarItem* copiedCar = new CarItem(m_scene, lastCar->getName(), lastCar->getMaxSpeed(), lastCar->getLength(), lastCar->getWidth(), lastCar->getType(), lastCar->getIsLoaded(), lastCar->getFilename(), lastCar->getColor());
		m_listCars.append(copiedCar);
		
		if(copiedCar->getFilename() == "")
		{
			ui.listWidgetCars->addItem(new QListWidgetItem(copiedCar->getName()));
		}
		else
		{
			QIcon icon = QIcon(copiedCar->getPixmap());
			ui.listWidgetCars->addItem(new QListWidgetItem(icon, copiedCar->getName()));
		}
	}
}


void DialogNewLoco::slot_deleteCar()
{
	if(ui.listWidgetCars->currentRow()>=0)
	{
		// Delete from the list. Do it first otherwise currentRow not valid anymore
		delete m_listCars.takeAt(ui.listWidgetCars->currentRow());
		
		// Delete from the widget
		delete ui.listWidgetCars->takeItem(ui.listWidgetCars->currentRow());
	}
}

void DialogNewLoco::slot_cancel()
{
	foreach(CarItem* car, m_listCars)
	{
		delete car;
	}

	close();
}

void DialogNewLoco::slot_OK()
{
	// If a name didn't have been specified, prevent the closure
	if(ui.lineEditName->text() != "")
	{
		m_status = new CarItem(m_scene, ui.lineEditName->text(), ui.spinBoxSpeed->value(), ui.doubleSpinBoxLength->value(), ui.doubleSpinBoxWidth->value()/2, -1, false, ui.lineEditImagePath->text(), m_color);
		m_status->setSpeedLimiter(true);
		
		// Position the loc on the platform
		foreach(QGraphicsItem* item, m_scene->items())
		{
			ShapeItem* platform = dynamic_cast<ShapeItem*>(item);

			if(platform)
			{
				// Find the path with matching name
				if(platform->getName() == ui.comboBoxPlatform->currentText())
				{	
					// Affect all cars to the platform
					for(int i=0; i<m_listCars.size(); i++)
					{
						m_listCars.at(i)->setPosInit(platform);	
						m_scene->addItem(m_listCars.at(i));
					}

					// Affect the platform to the loc
					m_status->setPosInit(platform);
				}
			}
		}
		
		close();
	}
	else
	{
		QMessageBox::information(this,tr("Name missing"),tr("You must specify a name to identify this locomotive."));
		return;
	}
}
